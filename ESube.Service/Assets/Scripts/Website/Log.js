﻿webSiteModule.controller('LogController', ['$scope', '$rootScope', '$http', 'NgTableParams', '$filter', '$sce', function ($scope, $rootScope, $http, NgTableParams, $filter,$sce) {

    $scope.searchResultText = 'Please click search button...';

    $scope.logList = [];
    $scope.log= {};
    $scope.logItem = {};
    $scope.companyList = [];
    $scope.sourceTypeList = [
        'All',
        'WebService',
        'DbTransfer'
    ];



    $scope.filters = {
        SourceType : 'All',
        LogId: '',
        //Type: '',
        MainCompanyCode: '',
        SearchText: '',
        StatusCode:0
    }

    $scope.getListLog = function () {

        fireCustomLoading(true);
        $scope.logList =[];
        $http({
            method: "POST",
            url: "/Log/GetListLog",
            headers: { "Content-Type": "Application/json;charset=utf-8" },
            data: {
                pLogId: $scope.filters.LogId == '' ? 0 : $scope.filters.LogId,
                pCompanyCode: $scope.filters.MainCompanyCode,
                pSearchText: $scope.filters.SearchText,
                pStatusCode: $scope.filters.StatusCode,
                pSourceType: $scope.filters.SourceType === 'All' ? '' : $scope.filters.SourceType
            }
        }).then(function (response) {
            fireCustomLoading(false);
            $scope.logList = response.data.Data;
            $scope.tableParams = new NgTableParams({}, {
                filterDelay: 0,
                dataset: angular.copy($scope.logList)
            });
            $scope.tableParams.count(1000) // set


        });

    };



    $(document).ready(function () {
        //$('body').addClass('page-sidebar-closed');,
        //$rootScope.changeSideBar();
    });

    $scope.previewLog = function (row) {
        $scope.logItem = {};
        $scope.logItem = row;


        if ($rootScope.isValid($scope.logItem.RequestBody)) {

            try {
                $scope.logItem.RequestBody = JSON.parse($scope.logItem.RequestBody); 
            }
            catch (err) {
            }


        }

        if ($rootScope.isValid($scope.logItem.ResponseBody)) {
            try {
                $scope.logItem.ResponseBody = JSON.parse($scope.logItem.ResponseBody);
            }
            catch (err) {
            }
        }

        if ($scope.logItem.SourceType === 1) {
            $('#modal-webServiceLogPreview').modal('show');
        }
        else if ($scope.logItem.SourceType === 2) {
            $('#modal-dbTransferErrorLogPreview').modal('show');
        }

        
    };

    $(document).on('keypress', function (e) {
        if (e.which === 13) {
            $scope.getListLog();
        }
    });

    $scope.trustDangerousSnippet = function (snippet) {
        return $sce.trustAsHtml(snippet);
    };

}]).filter("dateFilter", function () {
    return function (item) {
        if (item != null) {
            return new Date(parseInt(item.substr(6)));
        }
        return "";
    }
});
