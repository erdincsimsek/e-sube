﻿webSiteModule.controller('Content_ReportController', ['$scope', '$rootScope', '$http', 'NgTableParams', '$filter', function ($scope, $rootScope, $http, NgTableParams, $filter) {


    $scope.searchResultText = 'Please click search button . . .';

    $scope.validateResult = false;
    $scope.validateMessage = '';

    $scope.reportList = [];
    $scope.filters = {
        MainCompanyCode: '',
        Name: ''
    }

    $scope.report = {};

    $scope.actionType = 'List';

    $scope.tableParamsReport = [];


    $scope.getListReport = function () {

        fireCustomLoading(true);
        $http({
            method: "POST",
            url: "/Report/GetListReport",
            headers: { "Content-Type": "Application/json;charset=utf-8" },
            data: {
                pMainCompanyCode: $scope.filters.MainCompanyCode
            }
        }).then(function (response) {

            fireCustomLoading(false);
            $scope.reportList = response.data;

            if ($scope.reportList.length === 0) {
                $scope.searchResultText = 'Sonuç Bulunamadı.';
                toastr.error('Sonuç Bulunamadı.');
            }
          
            $scope.tableParamsReport = new NgTableParams({}, {
                filterDelay: 0,
                dataset: angular.copy($scope.reportList)
            });
            $scope.tableParamsReport.count(1000) // set

        });

    };

    $scope.setActionType = function (pActionType, pRow) {
        $scope.actionType = pActionType;

        if (pActionType === 'Create') {
            $scope.report = angular.copy($scope.reportDefault);
        } else {
            $scope.report = angular.copy(pRow);
        }

    };


    $scope.validateInsertOrUpdate = function () {

        $scope.validateResult = false;
        $scope.validateMessage = '';

        if (!$rootScope.isValid($scope.report.MainCompanyCode)) {
            $scope.validateMessage = 'Select MainCompany';
        } else if (!$rootScope.isValid($scope.report.Name)) {
            $scope.validateMessage = 'Enter Report Name';
        } else if (!$rootScope.isValid($scope.report.URL)) {
            $scope.validateMessage = 'Enter POWER BI URL';
        }
        else {
            $scope.validateResult = true;
        }
    };

    $scope.insertOrUpdateReport = function () {


        $scope.validateInsertOrUpdate();


        if (!$scope.validateResult) {
            toastr.error($scope.validateMessage);
        } else {
            fireCustomLoading(true);
            $http({
                method: "POST",
                url: "/Content/InsertOrUpdateReport",
                headers: { "Content-Type": "Application/json;charset=utf-8" },
                data: {
                    pReport: $scope.report
                }
            }).then(function (response) {

                fireCustomLoading(false);
                $scope.retObj = response.data;

                if ($scope.retObj.Result === false) {
                    $scope.searchResultText = $scope.retObj.Result;
                    toastr.error($scope.retObj.Result);
                }
                else if ($scope.retObj.Result) {
                    $scope.searchResultText = '';
                    toastr.success('Success');
                }

                $scope.actionType = 'List';
                $scope.getListReport();

            });

        };


    };

    $scope.deleteReport = function (pRow) {
        $scope.report = pRow;
        $scope.report.IsDeleted = true;
        $scope.insertOrUpdateReport();
    };

    $scope.updateReportActive = function (pRow) {
        //pRow.IsActive = pRow.IsActive === true ? false : true;
        pRow.IsActive = !pRow.IsActive;
        $scope.report = pRow;
        $scope.insertOrUpdateReport();
    };



    

    $(document).ready(function () {
        $scope.reportDefault = angular.copy($scope.report);
        console.log($rootScope.globalMainCompanyList);
    });


    $(document).on('keypress', function (e) {
        if (e.which === 13) {
            $scope.getListReport();
        }
    });




}]).filter("dateFilter", function () {
    return function (item) {
        if (item != null) {
            return new Date(parseInt(item.substr(6)));
        }
        return "";
    }
});
