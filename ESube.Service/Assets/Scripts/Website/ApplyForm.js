﻿webSiteModule.controller('ApplyFormController', ['$scope', '$rootScope', '$http', 'NgTableParams', '$filter', '$sce', function ($scope, $rootScope, $http, NgTableParams, $filter, $sce) {


    $scope.getListQuestionAndAnswer = function () {

        if ($rootScope.isValid($scope.filters.MainCompanyCode)) {

            $scope.questionListWithAnswer = [];


            fireCustomLoading(true);
            $http({
                method: "POST",
                url: "/ApplyForm/GetListQuestionWithAnswer",
                headers: { "Content-Type": "Application/json;charset=utf-8" },
                data: {
                    pMainCompanyCode: $scope.filters.MainCompanyCode
                }
            }).then(function (response) {
                fireCustomLoading(false);
                $scope.questionListWithAnswer = response.data.Data;
            });
        };

    };

    $scope.insertOrUpdateForm = function () {

        fireCustomLoading(true);
        $http({
            method: "POST",
            url: "/ApplyForm/InsertOrUpdateForm",
            headers: { "Content-Type": "Application/json;charset=utf-8" },
            data: {
                pList: $scope.questionListWithAnswer,
                pMainCompanyCode: $scope.filters.MainCompanyCode

            }
        }).then(function (response) {
            fireCustomLoading(false);

            if (response.data.Result) {
                toastr.success('Success');
                $scope.getListQuestionAndAnswer();
            } else {
                toastr.error('Error : '+ response.data.ResulMessage);
            }

        });

    };





    $(document).ready(function () {
        $scope.getListQuestionAndAnswer();

        //$('body').addClass('page-sidebar-closed');,
        //$rootScope.changeSideBar();
    });


    $(document).on('keypress', function (e) {
        if (e.which === 13) {
            $scope.insertOrUpdateForm();
        }
    });

    $scope.trustDangerousSnippet = function (snippet) {
        return $sce.trustAsHtml(snippet);
    };

}]).filter("dateFilter", function () {
    return function (item) {
        if (item != null) {
            return new Date(parseInt(item.substr(6)));
        }
        return "";
    }
});
