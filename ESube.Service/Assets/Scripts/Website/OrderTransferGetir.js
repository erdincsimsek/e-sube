﻿webSiteModule.controller('OrderTransferGetirController', ['$scope', '$rootScope', '$http', 'NgTableParams', '$filter', function ($scope, $rootScope, $http, NgTableParams, $filter) {

    $scope.currentOrder = {};
    $scope.filters = {
        pOrder:''
    }

    $scope.postData = {
        CurrentOrderCode:'',
        NewOrderCode :'',
        NewOrderId :'',
        ItemCodeListStr :''

    }


    $scope.getIntOrder = function () {
        fireCustomLoading(true);
        $http({
            method: "POST",
            url: "/Operation/GetIntOrder",
            headers: { "Content-Type": "Application/json;charset=utf-8" },
            data: {
                pMainCompanyCode: 'GT',
                pOrderCode: $scope.filters.pOrder
            }
        }).then(function (response) {
            $scope.currentOrder = response.data;
            if (response.data === 'null') {
                toastr.error('Sipariş bulunamadı');
            } else {
                toastr.success('Sipariş bilgileri listelendi');
                $scope.postData.CurrentOrderCode = $scope.currentOrder.OrderCode;
            }

            fireCustomLoading(false);
        });
    };



    //Aktarılacak ürünlerin seçilmesi işlemini yapan fonksiyon
    $scope.setSelectedItemListStr = function () {
        var jItemCodes = '';
        var jOrderLines = $scope.currentOrder.Lines;
        jOrderLines = jOrderLines.filter(row => row.Item['IsSelected'] === true);
        angular.forEach(jOrderLines, function (pValue, pProp) {
            var item = pValue.Item
            jItemCodes += (item.ItemCode+',');
        });

        $scope.postData.ItemCodeListStr = angular.copy(jItemCodes);
    };


    $scope.setAllProductsSelectedOrNot = function () {
        var isSelected = $scope.isAllItemsSelected;

        angular.forEach($scope.currentOrder.Lines, function (pValue, pProp) {
            var item = pValue.Item
            item.IsSelected = isSelected;
            $scope.setSelectedItemListStr();

        });


    };

    $scope.setSelectedProduct = function (pIsSelected) {
        var isSelected = pIsSelected !== true ? false : true;

        if (!isSelected) {
            $scope.isAllItemsSelected = false;
        }

        $scope.setSelectedItemListStr();

        return isSelected;


    }


    $scope.transferItemList = function () {

        if (!$rootScope.isValid($scope.postData.ItemCodeListStr)) {
            toastr.error('Ürün seçiniz.');
        } else {
            console.log($scope.postData);
            var x = angular.copy($scope.postData);

            fireCustomLoading(true);
            $http({
                method: "POST",
                url: "/Order/TransferOrder",
                headers: { "Content-Type": "Application/json;charset=utf-8" },
                data: {
                    pCurrentOrderCode: x.CurrentOrderCode,
                    pNewOrderCode: x.NewOrderCode,
                    pNewOrderId: x.NewOrderId,
                    pItemCodeListStr: x.ItemCodeListStr
                }
            }).then(function (response) {
                console.log(response.data);
                if (response.data.Result !== true) {
                    toastr.error(response.data.ResultMessage);
                } else {
                    toastr.success(response.data.ResultMessage);
                }

                fireCustomLoading(false);
            });
        }


    };

    $scope.resetAll = function () {

        $scope.currentOrder = {};
        $scope.filters.pOrder = '';

    };


    $(document).on('keypress', function (e) {
        //if ($rootScope.isValid($scope.filters.pOrder)) {
            if (e.which === 13) {
                $scope.getIntOrder();
            }
        //}

    });

    $(document).ready(function () {
        //$scope.getOrder();
        //$rootScope.changeSideBar();
    });


    //$scope.getGetirGoodsOutDetail = function (row) {

    //    fireCustomLoading(true);
    //    $http({
    //        method: "POST",
    //        url: "/OrderGetir/GetListGoodsOutDetail",
    //        headers: { "Content-Type": "Application/json;charset=utf-8" },
    //        data: {
    //            pOrderCode: row.WaorCode
    //        }
    //    }).then(function (response) {
    //        fireCustomLoading(false);
    //        $scope.orderDetailList = response.data;

    //        $('#modal-getir-goodsOutList').modal('show');
    //    });
    //};




}]).filter("dateFilter", function () {
    return function (item) {
        if (item != null) {
            return new Date(parseInt(item.substr(6)));
        }
        return "";
    }
});
