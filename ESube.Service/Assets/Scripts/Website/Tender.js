﻿webSiteModule.controller('TenderController', ['$scope', '$rootScope', '$http', 'NgTableParams', '$filter', '$sce', function ($scope, $rootScope, $http, NgTableParams, $filter, $sce) {

    $scope.searchResultText = 'Ara Butonuna Tıklayınız...';
    $scope.actionType = 'list';
    $scope.tenderList = [];
    $scope.tender = {};


    $scope.filters = {
        TenderCode: '',
        SyncStatusCode: 0
    };

    $scope.getListTender = function () {

        fireCustomLoading(true);
        $scope.tenderList = [];
        $http({
            method: "POST",
            url: "/Tender/GetListTender",
            headers: { "Content-Type": "Application/json;charset=utf-8" },
            data: {
                //pTenderId: $scope.filters.TenderId == '' ? 0 : $scope.filters.TenderId,
                pCode: $scope.filters.TenderCode,
                pStatusCode: $scope.filters.SyncStatusCode
            }
        }).then(function (response) {
            fireCustomLoading(false);
            $scope.tenderList = response.data;
            $scope.tableParams = new NgTableParams({}, {
                filterDelay: 0,
                dataset: angular.copy($scope.tenderList)
            });
            $scope.tableParams.count(1000) // set


        });

    };


    $scope.getTender = function (row) {

        fireCustomLoading(true);
        $scope.tender = {};
        $http({
            method: "POST",
            url: "/Tender/GetTender",
            headers: { "Content-Type": "Application/json;charset=utf-8" },
            data: {
                pId: row.Id,
                pCode: row.Code,
            }
        }).then(function (response) {
            fireCustomLoading(false);
            $scope.tender = response.data;
            $scope.actionType = 'detail';


            $('#orderTab').click();

            //console.log($scope.tender.stops);
            //$('#modal-tender').modal('show');
        });

    };



    $scope.reSync = function () {
        fireCustomLoading(true);
        toastr.info('Tenderın tüm bilgilerinin statüsü  AKTARILMAMIŞ olarak güncelleniyor. Lütfen bekleyiniz... ');

        $http({
            method: "POST",
            url: "/Tender/UpdateSyncStatus",
            headers: { "Content-Type": "Application/json;charset=utf-8" },
            data: {
                pId: $scope.tender.Id,
                pCode: $scope.tender.Code
            }
        }).then(function (response) {
            toastr.remove();
            toastr.success('Tenderın tüm bilgileri AKTARILMAMIŞ olarak güncellendi');
            $scope.actionType = 'list';
            $scope.getListTender();

            fireCustomLoading(false);
        });

    };

    $(document).ready(function () {
        $scope.getListTender();

        //$('body').addClass('page-sidebar-closed');,
        //$rootScope.changeSideBar();
    });

    $scope.previewTender = function (row) {
        $scope.getTender(row);
    };

    $scope.previewStop = function (row) {
        $scope.stop = row;
        $('#modal-stop').modal('show');
    };

    $scope.previewAnswer = function (row) {
        $scope.answer = row;
        $('#modal-answer').modal('show');
    };

    $(document).on('keypress', function (e) {
        if (e.which === 13) {
            $scope.getListTender();
        }
    });

    $scope.trustDangerousSnippet = function (snippet) {
        return $sce.trustAsHtml(snippet);
    };

}]).filter("dateFilter", function () {
    return function (item) {
        if (item != null) {
            return new Date(parseInt(item.substr(6)));
        }
        return "";
    }
});
