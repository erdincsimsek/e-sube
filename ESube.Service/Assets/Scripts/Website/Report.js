﻿webSiteModule.controller('ReportController', ['$scope', '$rootScope', '$http', 'NgTableParams', '$filter', function ($scope, $rootScope, $http, NgTableParams, $filter) {


    $scope.reportList = [];

    //main variables
    $scope.sMainCompanyCode = '';
    $scope.sReportURL = '';
    $scope.sReportId = 0;

    $scope.warningMessage = "Please select maincompany and report , then click show button";


    $scope.getListReport = function () {

        fireCustomLoading(true);
        $http({
            method: "POST",
            url: "/Content/GetListReport",
            headers: { "Content-Type": "Application/json;charset=utf-8" },
            data: {
                pMainCompanyCode: $scope.sMainCompanyCode,
                pId: $scope.sReportName
            }
        }).then(function (response) {

            fireCustomLoading(false);
            $scope.reportList = response.data;
            $scope.reportListDefault = response.data;

            var tmpRecord = { Id: 0, Name: 'Please Select',URL:'' };
            $scope.reportList.unshift(tmpRecord);


        });

    };


    $scope.filterReportList = function () {
        var pMainCompanyCode = $scope.sMainCompanyCode;
        $scope.reportList = angular.copy($scope.reportListDefault);

        var reportListJs = $scope.reportList;

        reportListJs = reportListJs.filter(item => item['MainCompanyCode'] === pMainCompanyCode ||  item['Id'] === 0);

        $scope.reportList = angular.copy(reportListJs);

        $scope.sReportId = 0;
        $scope.sReportURL = "";


    };

    $scope.showReport = function () {

        //URL i bul
        var reportListJs = angular.copy($scope.reportList);
        var reportJs = reportListJs.filter(item => item['Id'] === $scope.sReportId);
        $scope.sReportURL = reportJs[0].URL;

        $("#iframeDiv").html("");
        $("#iframeDiv").append("<iframe src=" + $scope.sReportURL+ " frameborder='0' allowfullscreen style = 'position:absolute;top:0;left:0;width:100%;height:100%;'></iframe >");


        fireCustomLoading(true);

        setTimeout(function () {
            fireCustomLoading(false);
        }, 5000);

    };

    $scope.resetReport = function () {
        $scope.sReportURL = "";

        $("#iframeDiv").html("");
    };


    $(document).ready(function () {
        fireCustomLoading(true);
        $scope.getListReport();
    });


    $(document).on('keypress', function (e) {
        if (e.which === 13) {
            $scope.getReportList();
        }
    });




}]).filter("dateFilter", function () {
    return function (item) {
        if (item != null) {
            return new Date(parseInt(item.substr(6)));
        }
        return "";
    }
});
