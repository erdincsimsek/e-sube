﻿LoginModule.controller('AccountController', ['$scope', '$rootScope', '$http', function ($scope, $rootScope, $http) {

    $scope.actionType = 'login';
    $scope.validateResult = false;
    $scope.validateResultMessage = '';
    $scope.account = {};
    $scope.accountDefault = {};


    $scope.retObj = {
        Result: false
    };



    $scope.register = function () {
        fireCustomLoading(true);
            $http({
                method: "POST",
                url: "/Account/Register",
                headers: { "Content-Type": "Application/json;charset=utf-8" },
                data: { model: $scope.account }
            }).then(function (response) {
                $scope.retObj = response.data;
                if (!$scope.retObj.Result) {
                    fireCustomLoading(false);
                    toastr.error($scope.retObj.ResultMessage);
                } else {
                    toastr.success('Kaydınız Başarı İle Tamamlanmıştır');

                    setTimeout(
                        function () {
                            $(location).attr('href', '/Company');
                        }, 3000);
                }
            });
    };


    $scope.login = function () {
        fireCustomLoading(true);
            $http({
                method: "POST",
                url: "/Account/Login",
                headers: { "Content-Type": "Application/json;charset=utf-8" },
                data: { model: $scope.account }
            }).then(function (response) {
                $scope.retObj = response.data;
                if (!$scope.retObj.Result) {
                    fireCustomLoading(false);
                    toastr.error($scope.retObj.ResultMessage);
                } else {
                    $(location).attr('href', '/Help');
                }
            });
    };


    $scope.setActionType = function (pActionType) {
        $scope.actionType = pActionType;
        $scope.resetForm();
    };

    $scope.resetForm = function () {
        $scope.account = angular.copy($scope.accountDefault);

        //setTimeout(
        //    function () {
        //        $('#EmailID').val('');
        //        $('#Username').val('');
        //        $('#Password').val('');
        //    }, 500);

   
    };

    $(document).on('keypress', function (e) {
        if (e.which === 13) {
            if ($scope.actionType === 'register') {
                $scope.register();
            } else if ($scope.actionType === 'login') {
                $scope.login();
            }
        }

    });

    $(document).ready(function () {
        $scope.accountDefault = angular.copy($scope.account);
    });




}]);



