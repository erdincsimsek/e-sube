﻿webSiteModule.controller('ApiServicesController', ['$scope', '$rootScope', '$http', 'NgTableParams', '$filter', function ($scope, $rootScope, $http, NgTableParams, $filter) {


    $scope.companyList = [];
    $scope.company = {};



    $scope.getListCompany = function () {

        fireCustomLoading(true);
        $http({
            method: "POST",
            url: "/Company/GetList",
            headers: { "Content-Type": "Application/json;charset=utf-8" },
            data: {
            }
        }).then(function (response) {
            fireCustomLoading(false);
            $scope.companyList = response.data;
        });

    };

    $scope.getApiList = function () {
        fireCustomLoading(true);
        $http({
            method: "POST",
            url: "/Help/GetApiList",
            headers: { "Content-Type": "Application/json;charset=utf-8" },
            data: {}
        }).then(function (response) {

            console.log(response.data);

            $scope.apiList = response.data;


            fireCustomLoading(false);
        });
    };

    $scope.getApiHref = function (api) {



        var currentUrl = api.ID;

        var newUrl = "";

        if (currentUrl !== undefined) {

                
            currentUrl = currentUrl.replace('POST', 'POST-');
            currentUrl = currentUrl.replace('GET', 'GET-');
            currentUrl = currentUrl.replace('DELETE', 'DELETE-');

            currentUrl = currentUrl.replace('/', '-');
            currentUrl = currentUrl.replace('/', '-');
            currentUrl = currentUrl.replace('/', '-');
            currentUrl = currentUrl.replace('/', '-');

            currentUrl = currentUrl.replace('?', '_');
            currentUrl = currentUrl.replace('?', '_');
            currentUrl = currentUrl.replace('?', '_');
            currentUrl = currentUrl.replace('?', '_');
            currentUrl = currentUrl.replace('?', '_');
            currentUrl = currentUrl.replace('?', '_');


            newUrl = '/Help/Api/' + currentUrl;
        }

        return newUrl;
    };

    $(document).ready(function () {

        $('.help-page-table').find('.api-name').removeClass('api-name');
        $('.help-page-table').find('.api-documentation').removeClass('api-documentation');

        $('.help-page-table').addClass('table table-bordered table-hover');
        $('.help-page-table').removeClass('help-page-table');


        $('body').html().replace('#####', '<br>');

        //$scope.getApiList();

    });


}]).filter("dateFilter", function () {
    return function (item) {
        if (item != null) {
            return new Date(parseInt(item.substr(6)));
        }
        return "";
    }
});
