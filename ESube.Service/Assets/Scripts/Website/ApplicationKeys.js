﻿webSiteModule.controller('ApplicationKeysController', ['$scope', '$rootScope', '$http', function ($scope, $rootScope, $http) {
    $scope.token = '3C614100-7CDC-4BF5-98C2-FFF4420B8BD4';

    $scope.clientKeys = {};

    $scope.getApiKeys = function () {
        fireCustomLoading(true);
        $http({
            method: "POST",
            url: "/ApplicationKeys/GetApplicationKeys",
            headers: { "Content-Type": "Application/json;charset=utf-8" },
            data: { }
        }).then(function (response) {
            fireCustomLoading(false);
            $scope.clientKey = response.data;
        });
    };

    $scope.regenerateClientKeys = function () {
        $http({
            method: "POST",
            url: "/ApplicationKeys/ReGenerateClientKeys",
            headers: { "Content-Type": "Application/json;charset=utf-8" },
            data: { clientkeys: $scope.clientKey }
        }).then(function (response) {
            $scope.clientKey = response.data;
        });
    };


    $scope.regenerateToken = function () {
        $http({
            method: "POST",
            url: "/ApplicationKeys/ReGenerateToken",
            headers: { "Content-Type": "Application/json;charset=utf-8" },
            data: {  }
        }).then(function (response) {
            $scope.token = response.data;
        });
    };


    $(document).ready(function () {
        $scope.getApiKeys();
    });

}]);