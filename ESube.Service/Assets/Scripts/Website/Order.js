﻿webSiteModule.controller('OrderController', ['$scope', '$rootScope', '$http', 'NgTableParams', '$filter', function ($scope, $rootScope, $http, NgTableParams, $filter) {

    $scope.searchResultText = 'Click search button';
    $scope.actionType = 'list';


    $scope.orderList = [];
    $scope.order = {};
    $scope.orderDetailList = [];
    $scope.filters = {
        MainCompanyCode: '',
        ShipmentCode:'',
        OrderType: -10,
        OrderCode: '',
        SyncStatusCode: 0
    };

    $scope.setFilters = function (pOrderType, pMainCompany) {

        $scope.filters.OrderType = pOrderType === null ? $scope.filters.OrderType : pOrderType;
        $scope.filters.MainCompany = pMainCompany === null ? $scope.filters.MainCompany : pMainCompany;;

        $scope.order = {};
        $scope.orderList = [];
        $scope.orderDetailList = [];
    };

    $scope.getListOrder = function () {

        fireCustomLoading(true);
        $http({
            method: "POST",
            url: "/Order/GetListOrder",
            headers: { "Content-Type": "Application/json;charset=utf-8" },
            data: {
                pMainCompanyCode: $scope.filters.MainCompanyCode,
                pOrderCode: $scope.filters.OrderCode,
                //pShipmentCode: $scope.filters.ShipmentCode,
                pOrderType: $scope.filters.OrderType,
                pOrderStatusCode: $scope.filters.SyncStatusCode
            }
        }).then(function (response) {
            $scope.orderList = response.data;

            if ($scope.orderList.length === 0) {
                $scope.searchResultText = 'Sonuç Bulunamadı.';
                toastr.error('Sonuç Bulunamadı.');
            }
            else if ($scope.order.length) {
                $scope.searchResultText = '';
                toastr.success(' - Giriş Siparişleri Listelendi');
            }

            $scope.tableParamsOrder = new NgTableParams({}, {
                filterDelay: 0,
                dataset: angular.copy($scope.orderList)
            });
            $scope.tableParamsOrder.count(1000) // set


            setTimeout(function () {
                fireCustomLoading(false);
            }, 2000);


        });

    };

    $scope.searchOrderDetail = function (row) {

        $scope.order = angular.copy(row);
        $scope.getListOrderDetail(row);
    };

   

    $scope.getOrderWithDetail = function (row) {

        fireCustomLoading(true);
        $http({
            method: "POST",
            url: "/Order/GetOrderWithDetail",
            headers: { "Content-Type": "Application/json;charset=utf-8" },
            data: {
                pMainCompanyCode: row.MainCompanyCode,
                pId : row.Id,
                pOrderCode: row.OrderCode
            }
        }).then(function (response) {
            fireCustomLoading(false);
            $scope.order = response.data;
            $scope.actionType = 'detail';
            $('#orderTab').click();
            //$('#modal-orderDetail').modal('show');
        });
    };

    $scope.reSync = function () {
        fireCustomLoading(true);
        toastr.info('Siparişin tüm bilgilerinin statüsü  AKTARILMAMIŞ olarak güncelleniyor. Lütfen bekleyiniz... ');

        $http({
            method: "POST",
            url: "/Order/UpdateSyncStatus",
            headers: { "Content-Type": "Application/json;charset=utf-8" },
            data: {
                pMainCompanyCode: $scope.filters.MainCompanyCode,
                pOrderId: $scope.order.Id,
                pOrderCode: $scope.order.OrderCode,
                pStatusCode: 100
            }
        }).then(function (response) {
            toastr.remove();
            toastr.success('Siparişin tüm bilgileri AKTARILMAMIŞ olarak güncellendi');
            $scope.actionType = 'list';
            $scope.getListOrder();

            fireCustomLoading(false);
        });

    };

    $scope.getListOrderCompanies = function () {

        fireCustomLoading(true);
        $http({
            method: "POST",
            url: "/Order/GetListByOrder",
            headers: { "Content-Type": "Application/json;charset=utf-8" },
            data: {
                pOrderId: $scope.order.Id,
                pTableName: 'Company'
            }
        }).then(function (response) {
            $scope.order.CompanyList = response.data;
            fireCustomLoading(false);
        });
    };


    $scope.getListOrderLines = function () {

        fireCustomLoading(true);
        $http({
            method: "POST",
            url: "/Order/GetListByOrder",
            headers: { "Content-Type": "Application/json;charset=utf-8" },
            data: {
                pOrderId: $scope.order.Id,
                pTableName: 'Status'
            }
        }).then(function (response) {
            $scope.orderLines = response.data;
            fireCustomLoading(false);
        });
    };
    ////////////////////////////////////////////////////////////////

    $scope.getListEntStatus = function () {

        fireCustomLoading(true);
        $http({
            method: "POST",
            url: "/Order/GetListByOrder",
            headers: { "Content-Type": "Application/json;charset=utf-8" },
            data: {
                pOrderId: $scope.order.Id,
                pTableName: 'Status'
            }
        }).then(function (response) {
            $scope.order.EntStatus = response.data;
            fireCustomLoading(false);
        });
    };


    $scope.getListEntWaybillStatus = function () {

        fireCustomLoading(true);
        $http({
            method: "POST",
            url: "/Order/GetListByOrder",
            headers: { "Content-Type": "Application/json;charset=utf-8" },
            data: {
                pOrderId: $scope.order.Id,
                pTableName: 'Waybill'
            }
        }).then(function (response) {
            $scope.order.EntWaybillStatus = response.data;
            fireCustomLoading(false);
        });
    };


    $scope.reTransfer = function () {
        fireCustomLoading(true);
        toastr.info('Siparişin tüm bilgilerinin statüsü  AKTARILMAMIŞ olarak güncelleniyor. Lütfen bekleyiniz... ');

        $http({
            method: "POST",
            url: "/Order/UpdateTransferStatus",
            headers: { "Content-Type": "Application/json;charset=utf-8" },
            data: {
                pMainCompanyCode: $scope.order.MainCompanyCode,
                pOrderId: $scope.order.Id,
                pOrderCode: $scope.order.OrderCode,
                pOrderType: $scope.order.OrderType
            }
        }).then(function (response) {
            toastr.remove();
            toastr.success('Siparişin tüm bilgileri AKTARILMAMIŞ olarak güncellendi');
            $scope.actionType = 'list';
            $scope.getListOrder();

            fireCustomLoading(false);
        });

    };
   /////////////////////////////////////////////////////////////////



    $scope.previewOrderLineAndItemDetails = function (selectedOrderLine) {
        $scope.getOrderLineItem(selectedOrderLine); 
    };

    $scope.getOrderLineItem = function (selectedOrderLine) {
        $scope.selectedOrderLine = angular.copy(selectedOrderLine);

        fireCustomLoading(true);
        $http({
            method: "POST",
            url: "/Order/GetListByOrder",
            headers: { "Content-Type": "Application/json;charset=utf-8" },
            data: {
                pOrderId: selectedOrderLine.Id,
                pTableName: 'Item'
            }
        }).then(function (response) {
            $scope.selectedOrderLineItem = angular.copy(response.data[0]);
            fireCustomLoading(false);
            $scope.getOrderLineItemBarcodeList();
        });

    };

    $scope.getOrderLineItemBarcodeList = function () {

        fireCustomLoading(true);
        $http({
            method: "POST",
            url: "/Order/GetListByOrder",
            headers: { "Content-Type": "Application/json;charset=utf-8" },
            data: {
                pOrderId: $scope.selectedOrderLine.Id,
                pTableName: 'ItemBarcode'
            }
        }).then(function (response) {
            $scope.selectedOrderLineItemBarcodeList = angular.copy(response.data);
            fireCustomLoading(false);
            $scope.getOrderLineItemEachCustList();
        });


    };

    $scope.getOrderLineItemEachCustList = function () {

        fireCustomLoading(true);
        $http({
            method: "POST",
            url: "/Order/GetListByOrder",
            headers: { "Content-Type": "Application/json;charset=utf-8" },
            data: {
                pOrderId: $scope.selectedOrderLine.Id,
                pTableName: 'ItemEachCustomer'
            }
        }).then(function (response) {
            $scope.selectedOrderLineItemEachCustList = angular.copy(response.data);
            console.log($scope.selectedOrderLineItemEachCustList);
            fireCustomLoading(false);
        });

        $('#modal-orderLine-detail').modal('show');

    };

    $scope.previewCompany = function (row) {
        $scope.selectedCompany = row;
        $('#modal-company-detail').modal('show');
    };

    $(document).ready(function () {
        $scope.getListOrder();
    });



    $(document).on('keypress', function (e) {
        if (e.which === 13) {
            $scope.getListOrder();
        }
    });

    $scope.resetSearchResult = function () {

        $scope.orderList = [];
        $scope.tableParamsOrder = [];
    };


}]).filter("dateFilter", function () {
    return function (item) {
        if (item != null) {
            return new Date(parseInt(item.substr(6)));
        }
        return "";
    }
});
