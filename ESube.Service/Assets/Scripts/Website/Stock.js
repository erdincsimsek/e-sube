﻿webSiteModule.controller('StockController', ['$scope', '$rootScope', '$http', 'NgTableParams', '$filter', function ($scope, $rootScope, $http, NgTableParams, $filter) {

    $scope.searchResultText = 'Please select mainCompany and enter stock date, then click search button';
    $scope.actionType = 'list';


    $scope.stockList = [];
    $scope.filters = {
        MainCompanyCode: '',
        StockDate: '',
        WareHouseCode: ''
    };


    $scope.getListStock = function () {

        fireCustomLoading(true);
        $http({
            method: "POST",
            url: "/Stock/GetListStock",
            headers: { "Content-Type": "Application/json;charset=utf-8" },
            data: {
                pMainCompanyCode: $scope.filters.MainCompanyCode,
                pStockDate: $scope.filters.StockDate,
                pWareHouseCode: $scope.filters.WareHouseCode
            }
        }).then(function (response) {
            fireCustomLoading(false);
            $scope.stockList = response.data;
            console.log(response.data);

            if ($scope.stockList.length === 0) {
                $scope.searchResultText = 'No result ! ';
                toastr.error('No result ! ');
            }

            $scope.tableParamsStock = new NgTableParams({}, {
                filterDelay: 0,
                dataset: angular.copy($scope.stockList)
            });
            $scope.tableParamsStock.count(1000) // set

        });

    };



    //////////////////////////////////////////////////////////////



    $scope.getExportToExcel = function () {

        fireCustomLoading(true);
        $http({
            method: "POST",
            url: "/Stock/GridExportToExcel",
            headers: { "Content-Type": "Application/json;charset=utf-8" },
            data: {
               
                pStockDate: $scope.filters.StockDate,
                pWareHouseCode: $scope.filters.WareHouseCode,
                pMainCompanyCode: $scope.filters.MainCompanyCode
            }
        }).then(function (response) {
            fireCustomLoading(false);
            //$scope.stockList = response.data;
            console.log(response.data);

            //if ($scope.stockList.length === 0) {
            //    $scope.searchResultText = 'No result ! ';
            //    toastr.error('No result ! ');
            //}

            //$scope.tableParamsStock = new NgTableParams({}, {
            //    filterDelay: 0,
            //    dataset: angular.copy($scope.stockList)
            //});
            //$scope.tableParamsStock.count(1000) // set

        });

    };


    //////////////////////////////////////////////////////////////

    $(document).ready(function () {
        //$rootScope.changeSideBar();
    });



    $(document).on('keypress', function (e) {
        //Ana firma seçili ise ve stok tarihi girilmiş ise
        if ($rootScope.isValid($scope.filters.MainCompanyCode) && $rootScope.isValid($scope.filters.StockDate)) {
            if (e.which === 13) {
                $scope.getListStock();
            }
        }

    });



}]).filter("dateFilter", function () {
    return function (item) {
        if (item != null) {
            return new Date(parseInt(item.substr(6)));
        }
        return "";
    }
});


