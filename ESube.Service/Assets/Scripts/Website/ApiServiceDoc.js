﻿webSiteModule.controller('ApiServiceDocController', ['$scope', '$rootScope', '$http', 'NgTableParams', '$filter', '$sce', function ($scope, $rootScope, $http, NgTableParams, $filter, $sce) {


    $(document).ready(function () {

        $('.help-page-table').find('.api-name').removeClass('api-name');
        $('.help-page-table').find('.api-documentation').removeClass('api-documentation');

        $('.help-page-table').addClass('table table-bordered table-hover');
        $('.help-page-table').removeClass('help-page-table');

        //$('a').attr("target", "_blank");

        var URL = $(location).attr("href").split('/').pop();
        $scope.apiName = URL;
    });


    $scope.trustDangerousSnippet = function (snippet) {
        return $sce.trustAsHtml(snippet);
    };

}]).filter("dateFilter", function () {
    return function (item) {
        if (item != null) {
            return new Date(parseInt(item.substr(6)));
        }
        return "";
    }
});
