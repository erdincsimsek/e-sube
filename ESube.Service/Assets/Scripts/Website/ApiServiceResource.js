﻿webSiteModule.controller('ApiServiceResourceController', ['$scope', '$rootScope', '$http', 'NgTableParams', '$filter', function ($scope, $rootScope, $http, NgTableParams, $filter) {


    $(document).ready(function () {

        $('.help-page-table').find('.api-name').removeClass('api-name');
        $('.help-page-table').find('.api-documentation').removeClass('api-documentation');

        $('.help-page-table').addClass('table table-bordered table-hover');
        $('.help-page-table').removeClass('help-page-table');


    });


}]).filter("dateFilter", function () {
    return function (item) {
        if (item != null) {
            return new Date(parseInt(item.substr(6)));
        }
        return "";
    }
});
