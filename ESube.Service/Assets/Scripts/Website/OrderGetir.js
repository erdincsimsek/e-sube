﻿webSiteModule.controller('OrderGetirController', ['$scope', '$rootScope', '$http', 'NgTableParams', '$filter', function ($scope, $rootScope, $http, NgTableParams, $filter) {
    $scope.searchResultText = 'Ara Butonuna Tıklayınız...';

    $scope.orderList = [];
    $scope.order = {};
    $scope.orderDetailList = [];
    $scope.filters = {
        OrderType: 1,
        OrderCode:'',
        SyncStatus :0
    };

    $scope.setFilters = function (pOrderType, pCompanyCode) {

        $scope.filters.OrderType = pOrderType === null ? $scope.filters.OrderType : pOrderType;
        $scope.filters.CompanyCode = pCompanyCode === null ? $scope.filters.CompanyCode : pCompanyCode;;

        $scope.order = {};
        $scope.orderList = [];
        $scope.orderDetailList = [];
    };

    $scope.getGetirGoodsIn = function () {

        fireCustomLoading(true);
        $http({
            method: "POST",
            url: "/OrderGetir/GetListGoodsIn",
            headers: { "Content-Type": "Application/json;charset=utf-8" },
            data: {
                pOrderCode: $scope.filters.OrderCode,
                pOrderStatus: $scope.filters.SyncStatus
            }
        }).then(function (response) {
            console.log(response.data);
            fireCustomLoading(false);
            $scope.orderList = response.data;

            if ($scope.orderList.length === 0)
            {
                $scope.searchResultText = 'Sonuç Bulunamadı.';
                toastr.error('Sonuç Bulunamadı.');
            }
            else if ($scope.order.length) {
                $scope.searchResultText = '';
                toastr.success('GETİR - Giriş Siparişleri Listelendi');
            }

            $scope.tableParamsGetirGoodsIn = new NgTableParams({}, {
                filterDelay: 0,
                dataset: angular.copy($scope.orderList)
            });
            $scope.tableParamsGetirGoodsIn.count(1000) // set



        });

    };

    $scope.getGetirGoodsOut = function () {
        $scope.orderList = [];
        fireCustomLoading(true);
        $http({
            method: "POST",
            url: "/OrderGetir/GetListGoodsOut",
            headers: { "Content-Type": "Application/json;charset=utf-8" },
            data: {
                pOrderCode: $scope.filters.OrderCode,
                pOrderStatus: $scope.filters.SyncStatus
            }
        }).then(function (response) {
            fireCustomLoading(false);
            toastr.success('GETİR - Çıkış  Siparişleri Listelendi');
            $scope.orderList = response.data;
            $scope.tableParamsGetirGoodsOut = new NgTableParams({}, {
                filterDelay: 0,
                dataset: angular.copy($scope.orderList)
            });
            $scope.tableParamsGetirGoodsOut.count(1000) // set
        });

    };

    $scope.searchOrderDetail = function (row) {

        $scope.order = angular.copy(row);

        if ($scope.filters.OrderType === 1) {
                $scope.getGetirGoodsInDetail(row);
        }
        else if ($scope.filters.OrderType === 2)
        {
                $scope.getGetirGoodsOutDetail(row);
        }


    };

    $(document).ready(function () {
        //$rootScope.changeSideBar();
    });

    $scope.searchOrder = function () {
        if ($scope.filters.OrderType === 1) {
                $scope.getGetirGoodsIn();
        } else if ($scope.filters.OrderType === 2) {
                $scope.getGetirGoodsOut();
        }
    };


    $scope.getGetirGoodsInDetail = function (row) {

        fireCustomLoading(true);
        $http({
            method: "POST",
            url: "/OrderGetir/GetListGoodsInDetail",
            headers: { "Content-Type": "Application/json;charset=utf-8" },
            data: {
                pOrderCode: row.WareCode
            }
        }).then(function (response) {
            fireCustomLoading(false);
            $scope.orderDetailList = response.data;

            $('#modal-orderDetail').modal('show');
        });
    };



    $scope.getGetirGoodsOutDetail = function (row) {

        fireCustomLoading(true);
        $http({
            method: "POST",
            url: "/OrderGetir/GetListGoodsOutDetail",
            headers: { "Content-Type": "Application/json;charset=utf-8" },
            data: {
                pOrderCode: row.WaorCode
            }
        }).then(function (response) {
            fireCustomLoading(false);
            $scope.orderDetailList = response.data;

            $('#modal-getir-goodsOutList').modal('show');
        });
    };


    $(document).on('keypress', function (e) {
        if (e.which === 13) {
            $scope.searchOrder();
        }
    });

    $scope.resetSearchResult = function () {

        $scope.orderList = [];
        $scope.tableParamsGetirGoodsIn = [];

        $scope.tableParamsGetirGoodsOut = [];
    };

}]).filter("dateFilter", function () {
    return function (item) {
        if (item != null) {
            return new Date(parseInt(item.substr(6)));
        }
        return "";
    }
});
