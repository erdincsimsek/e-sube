﻿function fireCustomLoading(pActionType) {

    if (pActionType) {
        $('body').loadingModal(
            {
                position: 'auto',
                text: 'Please Wait...',
                color: '#fff',
                opacity: '0.8',
                backgroundColor: 'rgb(0,0,0)',
                animation: 'wave'
            });
    } else if (pActionType === false) {
        $('body').loadingModal('destroy');
    }

}