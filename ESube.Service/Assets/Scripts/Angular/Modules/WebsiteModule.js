﻿var webSiteModule = angular.module('webSiteModule', ['ngTable']);

webSiteModule.run(function ($rootScope,$http) {

    //Global İşlemler ( değişkenler, fonksiyonlar, array ler )
    $rootScope.sessionUser = {
        Id: 0
        //,
        //RoleListStr: '',
        //AdminLoginType: '',
        //A: {
            //B: '',
            //C: 0
        //}
    };

  
    $rootScope.isValid = function (data) {

        if (data === undefined || data === '' || data === null) {
            return false;
        } else {
            return true;
        }
    };

    $rootScope.getListMainCompany = function () {

        fireCustomLoading(true);
        $http({
            method: "POST",
            url: "/MainCompany/GetList",
            headers: { "Content-Type": "Application/json;charset=utf-8" },
            data: {
            }
        }).then(function (response) {
            fireCustomLoading(false);
            $rootScope.globalMainCompanyList = response.data;

            var tmpRecord = { Id: 0, Name: '',Code:'' };
            $rootScope.globalMainCompanyList.unshift(tmpRecord);

        });

    };

    $rootScope.changeSessionGlobalMainCompany = function (row) {


        fireCustomLoading(true);
        $http({
            method: "POST",
            url: "/Setting/ChangeSessionGlobalMainCompany",
            headers: { "Content-Type": "Application/json;charset=utf-8" },
            data: {
                pMainCompanyCode: row.Code
            }
        }).then(function (response) {
            toastr.remove();
            toastr.success('MainCompany has been changed to  ' + row.Name);

      

            setTimeout(function () {
                if (row.Code === 'SMP')
                {
                    location.href="/Tender";
                } else
                {
                    location.href = "/Order";
                }

                fireCustomLoading(false);
            }, 2000);

          
        });



    };

    $rootScope.showMainCompanySelectModal = function () {
        $('#modal_main_company_select').modal('show');
    };


    $(document).ready(function () {
        $rootScope.getListMainCompany();


        if ($rootScope.sessionUser.RoleListStr.includes('ADM') ) {
            if ($rootScope.IsMainCompanySelected !== 'True') {
                $rootScope.showMainCompanySelectModal();
            }
        }


    });

   


    //$rootScope.setSalesman = function (pSsalesman) {
    //$rootScope.salesman = angular.fromJson(pSsalesman);
    //};

    //$rootScope.setSideBarPageList = function (list) {
    //$rootScope.sideBarPageList = angular.fromJson(list);

    //var pageList = angular.copy($rootScope.sideBarPageList);
    //$rootScope.sideBarMainPageList = pageList.filter(item => item['ParentId'] === 0);
    //$rootScope.sideBarSubPageList = pageList.filter(item => item['ParentId'] !== 0);

    //};

    //$rootScope.hasSub = function (parentId) {
    //    var pageList = angular.copy($rootScope.sideBarPageList);
    //    var tempList = pageList.filter(item => item['ParentId'] === parentId);

    //    return tempList.length > 0 ? true : false;
    //};

    $rootScope.changeSideBar = function () {
        $('body').toggleClass('page-sidebar-closed');
        $('.page-sidebar-menu').toggleClass('page-sidebar-menu-closed');
    };

});

//webSiteModule.filter("jsonDate", function () {

//    return function (x) {
//        return new Date(parseInt(x.substr(6)));
//    };
//});
