namespace ESube.Service.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class x22SSSsssss : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Item", "MainCompanyCode", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Item", "MainCompanyCode", c => c.String(nullable: false));
        }
    }
}
