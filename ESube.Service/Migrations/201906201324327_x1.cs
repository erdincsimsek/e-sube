namespace ESube.Service.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class x1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PreOrderOffer",
                c => new
                    {
                        id = c.Long(nullable: false, identity: true),
                        AnswerById = c.String(),
                        AnswerDate = c.String(),
                        StatusCode = c.String(),
                        CurrencyCode = c.String(),
                        Comments = c.String(),
                        Notes = c.String(),
                        TenderPriceType1 = c.String(),
                        TenderPrice1 = c.String(),
                        TenderPriceType2 = c.String(),
                        TenderPrice2 = c.String(),
                        TenderPriceType3 = c.String(),
                        TenderPrice3 = c.String(),
                        TenderPriceType4 = c.String(),
                        TenderPrice4 = c.String(),
                        TenderPriceType5 = c.String(),
                        TenderPrice5 = c.String(),
                        TenderPriceType6 = c.String(),
                        TenderPrice6 = c.String(),
                        TenderPriceType7 = c.String(),
                        TenderPrice7 = c.String(),
                        TenderPriceType8 = c.String(),
                        TenderPrice8 = c.String(),
                        Commission = c.String(),
                        DriverName = c.String(),
                        DriverExternalSystemId = c.String(),
                        DriverCharachterName = c.String(),
                        DriverMobileNR = c.String(),
                        CarrierName = c.String(),
                        CarrierExternalSystemId = c.String(),
                        status = c.String(),
                        statusDescription = c.String(),
                        createDate = c.DateTime(),
                        integrationCode = c.String(),
                        freeText = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
            AddColumn("dbo.PreOrder", "ExternalSystemId", c => c.String());
            AddColumn("dbo.PreOrder", "CompanyExternalSystemId", c => c.String());
            AddColumn("dbo.PreOrder", "ShipmentCode", c => c.String());
            AddColumn("dbo.PreOrder", "TransporterCode", c => c.String());
            AddColumn("dbo.PreOrder", "CargoType", c => c.String());
            AddColumn("dbo.PreOrder", "TruckType", c => c.String());
            AddColumn("dbo.PreOrder", "ConfirmedKM", c => c.String());
            AddColumn("dbo.PreOrder", "RequestSentYN", c => c.String());
            AddColumn("dbo.PreOrder", "CarrierName_1", c => c.String());
            AddColumn("dbo.PreOrder", "CarrierName_2", c => c.String());
            AddColumn("dbo.PreOrder", "CarrierName_3", c => c.String());
            AddColumn("dbo.PreOrder", "CarrierName_4", c => c.String());
            AddColumn("dbo.PreOrder", "CarrierName_5", c => c.String());
            AddColumn("dbo.PreOrder", "CarrierNameContact_1", c => c.String());
            AddColumn("dbo.PreOrder", "CarrierNameContact_2", c => c.String());
            AddColumn("dbo.PreOrder", "CarrierNameContact_3", c => c.String());
            AddColumn("dbo.PreOrder", "CarrierNameContact_4", c => c.String());
            AddColumn("dbo.PreOrder", "CarrierNameContact_5", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.PreOrder", "CarrierNameContact_5");
            DropColumn("dbo.PreOrder", "CarrierNameContact_4");
            DropColumn("dbo.PreOrder", "CarrierNameContact_3");
            DropColumn("dbo.PreOrder", "CarrierNameContact_2");
            DropColumn("dbo.PreOrder", "CarrierNameContact_1");
            DropColumn("dbo.PreOrder", "CarrierName_5");
            DropColumn("dbo.PreOrder", "CarrierName_4");
            DropColumn("dbo.PreOrder", "CarrierName_3");
            DropColumn("dbo.PreOrder", "CarrierName_2");
            DropColumn("dbo.PreOrder", "CarrierName_1");
            DropColumn("dbo.PreOrder", "RequestSentYN");
            DropColumn("dbo.PreOrder", "ConfirmedKM");
            DropColumn("dbo.PreOrder", "TruckType");
            DropColumn("dbo.PreOrder", "CargoType");
            DropColumn("dbo.PreOrder", "TransporterCode");
            DropColumn("dbo.PreOrder", "ShipmentCode");
            DropColumn("dbo.PreOrder", "CompanyExternalSystemId");
            DropColumn("dbo.PreOrder", "ExternalSystemId");
            DropTable("dbo.PreOrderOffer");
        }
    }
}
