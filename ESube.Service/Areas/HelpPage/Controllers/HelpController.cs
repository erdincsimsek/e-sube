using System;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Description;
using System.Web.Mvc;
using ESube.Service.Areas.HelpPage.ModelDescriptions;
using ESube.Service.Areas.HelpPage.Models;
using ESube.Service.Models;
using Newtonsoft.Json;

namespace ESube.Service.Areas.HelpPage.Controllers
{
    /// <summary>
    /// The controller that will handle requests for the help page.
    /// </summary>
    public class HelpController : Controller
    {
        private const string ErrorViewName = "Error";

        public HelpController()
            : this(GlobalConfiguration.Configuration)
        {
        }

        public HelpController(HttpConfiguration config)
        {
            Configuration = config;
        }

        public HttpConfiguration Configuration { get; private set; }

        public ActionResult Index()
        {
            ViewBag.DocumentationProvider = Configuration.Services.GetDocumentationProvider();

            var x = Configuration.Services.GetApiExplorer().ApiDescriptions;

            foreach (var item in x)
            {
                if (item.Documentation != null)
                {
                  item.Documentation = item.Documentation.Replace("#####", "</br>");
                    //    x[0].Documentation = "<span>A</span>< br ><span> B </span>";
                    //    x[1].Documentation = "A  <br>  B";
                    //    x[2].Documentation = "A  \n\n B";
                    //    x[3].Documentation = "A  /n B";
                }

            }


            string SessionJsonApiList = Session["SessionJsonApiList"] != null ? Session["SessionJsonApiList"].ToString() : null;
            MainCompany SessionMainCompany = Session["MainCompany"] != null ? (MainCompany)Session["MainCompany"] : new MainCompany();
            string MainCompanyCode = SessionMainCompany.Code;


            //Firma girmi� ise
            //if (CompanyCode == "GT")
            //{
            //    x = x.Where(a => a.RelativePath.Contains("getir") || a.RelativePath.Contains("login"));
            //}
            //else if (CompanyCode == "SMP")
            //{
            //    x =x.Where(a => a.RelativePath.Contains("simply") || a.RelativePath.Contains("login")).ToList();

            //}
            ////Di�er firmalar ise
            //else if (!string.IsNullOrEmpty(CompanyCode))
            //{
            //    x = Configuration.Services.GetApiExplorer().ApiDescriptions.Where(a => !a.RelativePath.Contains("simply") && !a.RelativePath.Contains("simply")).ToList();
            //}


            return View(x);
        }

        //[System.Web.Http.HttpPost]
        //public string GetApiList()
        //{
       

        //    var x = Configuration.Services.GetApiExplorer().ApiDescriptions.ToList();

         

        //    var _jsonSettings = new JsonSerializerSettings()
        //    {
        //        TypeNameHandling = TypeNameHandling.Auto,
        //        ConstructorHandling = ConstructorHandling.AllowNonPublicDefaultConstructor,
        //        PreserveReferencesHandling = PreserveReferencesHandling.Objects,
        //        ObjectCreationHandling = ObjectCreationHandling.Auto
        //        //ReferenceResolver = new JsonConvertReferenceSolver();
        //};


        //    var jsonApiList = JsonConvert.SerializeObject(x, _jsonSettings);


        //    #region AP�L�STES� SADECE 1 KEZ �EK�LS�N

        //    if (string.IsNullOrEmpty(SessionJsonApiList))
        //    {
        //        Session["SessionJsonApiList"] = jsonApiList;
        //        SessionJsonApiList = jsonApiList;
        //    }
        //    #endregion

        //    return SessionJsonApiList;
        //}

        public ActionResult Api(string apiId)
        {
            if (!String.IsNullOrEmpty(apiId))
            {
                HelpPageApiModel apiModel = Configuration.GetHelpPageApiModel(apiId);
                if (apiModel != null)
                {
                    return View(apiModel);
                }
            }

            return View(ErrorViewName);
        }

        public ActionResult ResourceModel(string modelName)
        {
            if (!String.IsNullOrEmpty(modelName))
            {
                ModelDescriptionGenerator modelDescriptionGenerator = Configuration.GetModelDescriptionGenerator();
                ModelDescription modelDescription;
                if (modelDescriptionGenerator.GeneratedModels.TryGetValue(modelName, out modelDescription))
                {
                    return View(modelDescription);
                }
            }

            return View(ErrorViewName);
        }
    }
}