﻿using ESube.Service.Context;
using ESube.Service.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;

namespace ESube.Service.Controllers
{
    public class IntegrationDetailsController : Controller
    {
        private ESubeContext db = new ESubeContext();

        // GET: IntegrationDetails
        public ActionResult Index(string sortOrder, string searchString, string currentFilter, int? page)
        {
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;
            ViewBag.DateSortParm = sortOrder == "Date" ? "date_desc" : "Date";

            var getirIntegrationDetails = from s in db.GetirIntegrationDetails
                                          select s;
            if (!String.IsNullOrEmpty(searchString))
            {
                getirIntegrationDetails = getirIntegrationDetails.Where(s => s.DocumentId.Contains(searchString)
                                       || s.orderCode.Contains(searchString));
            }
            switch (sortOrder)
            { 
                case "Date":
                    getirIntegrationDetails = getirIntegrationDetails.OrderBy(s => s.WaybillDate);
                    break;
                case "date_desc":
                    getirIntegrationDetails = getirIntegrationDetails.OrderByDescending(s => s.WaybillDate);
                    break;
                default:
                    getirIntegrationDetails = getirIntegrationDetails.OrderBy(s => s.DocumentId);
                    break;
            }
            int pageSize = 20;
            int pageNumber = (page ?? 1);
            // IQueryable<IntegrationDetails> obj = db.IntegrationDetails;
            return View(getirIntegrationDetails.ToPagedList(pageNumber, pageSize));
        }

        //// POST: IntegrationDetails/Resend/1
        //[HttpPost]
        //public ActionResult Resend(int id, FormCollection collection)
        //{
        //    try
        //    {
        //        // TODO: Add delete logic here

        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}
        // GET: IntegrationDetails/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: IntegrationDetails/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: IntegrationDetails/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: IntegrationDetails/Edit/5
        public ActionResult Resend(string id)
        {
            var context = new ESubeContext();
            
            context.Database.ExecuteSqlCommand("pr_ResendIntegrationData @p0, @p1", parameters: new[] { "Bill", "Gates" });
            return View();
        }

        // POST: IntegrationDetails/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: IntegrationDetails/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: IntegrationDetails/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

       
    }
}
