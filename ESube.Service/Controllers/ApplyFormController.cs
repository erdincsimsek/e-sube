﻿using ESube.Service.Context;
using ESube.Service.Helpers;
using ESube.Service.Models;
using ESube.Service.Models.Helpers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ESube.Service.Controllers
{
    public class ApplyFormController : Controller
    {
        QuestionHeader _QuestionHeader = new QuestionHeader();
        //QuestionAnswer _Answer = new QuestionAnswer();

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public string GetListQuestionWithAnswer(string pMainCompanyCode)
        {

            var listQuestionGroup = _QuestionHeader.GetList(pMainCompanyCode);

            //Ignore Propertylerin Ignore larını temizlemek için
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.ContractResolver = new IgnoreJsonAttributesResolver();

            var c = JsonConvert.SerializeObject(listQuestionGroup, Formatting.Indented,
            new JsonSerializerSettings
            {
                PreserveReferencesHandling = PreserveReferencesHandling.Objects
            });

            //var jsonList = JsonConvert.SerializeObject(listQuestionGroup, settings);

            return c;
        }

        [HttpPost]
        public string InsertOrUpdateForm(string pMainCompanyCode, List<QuestionHeader> pList)
        {
            ReturnObject retObj = new ReturnObject();
            string sqlQuery = "";
            int affactedRowsCount = 0;

            try
            {
                //Cevapları yeni bir list e aktardım.
                List<QuestionAnswer> _AnswerList = new List<QuestionAnswer>();
                QuestionAnswer _Answer = new QuestionAnswer();

                foreach (var itemA in pList)
                {

                    foreach (var itemB in itemA.QuestionList)
                    {
                        if (itemB != null)
                        {
                            foreach (var itemC in itemB._QuestionAnswer)
                            {
                                if (itemC != null && !string.IsNullOrEmpty(itemC.Answer))
                                {
                                   _Answer = new QuestionAnswer();
                                   _Answer._MainCompany.Code = pMainCompanyCode;
                                   _Answer._MainCompany.Id = 1021;
                                    _Answer._Question.Id = itemB.Id;
                                   _Answer.Answer =itemC.Answer;
                                   _AnswerList.Add(_Answer);
                                }
                            }
                        }


                    }
                }

                using (ESubeContext db = new ESubeContext())
                {

                    //Önceki kayıtları silinmişe çek
                    List<QuestionAnswer> _OldAnswerList = db.QuestionAnswer.Where(b => b._MainCompany.Code == pMainCompanyCode).ToList();
                    sqlQuery = "";
                    sqlQuery = "UPDATE [dbo].[QuestionAnswer] SET[IsDeleted] = 1,[UpdateDate] ='"+DateTime.Now.ToString()+"' WHERE _MainCompany_Code = '" + pMainCompanyCode+"'";
                    affactedRowsCount = db.Database.ExecuteSqlCommand(sqlQuery);
                    //if (_OldAnswerList.Count >0){
                    //    foreach (var item in _OldAnswerList){
                    //        item.IsDeleted = true;
                    //        item.UpdateDate = DateTime.Now;
                    //    }
                    //    db.SaveChanges();
                    //}



                    //Gelen kayıtları kaydet
                    if (_AnswerList.Count >0)
                    {
                       foreach (var itemX in _AnswerList)
                       {
                            //db.QuestionAnswer.Add(itemX);
                            sqlQuery = "";
                            sqlQuery = "INSERT INTO[dbo].[QuestionAnswer] ([Answer],[CreateId],[UpdateId],[CreateDate],[IsDeleted],[IsActive],[_MainCompany_Code],[_Question_Id]) VALUES(";


                            string sqlParamaters = string.Format("{0},{1},{2},{3},{4},{5},{6},{7}", 
                                "'"+itemX.Answer+"'",
                                0,
                                0, 
                                "'"+DateTime.Now.ToString()+ "'", 
                                0,
                                1, 
                                "'"+pMainCompanyCode+ "'", 
                                itemX._Question.Id+")");

                            sqlQuery += sqlParamaters;

                            affactedRowsCount = 0;
                            affactedRowsCount = db.Database.ExecuteSqlCommand(sqlQuery);

                        }
                       //db.SaveChanges();
                    }
                }

                retObj.Result = true;
                retObj.ResultMessage = "";
            }
            catch (Exception x)
            {
                retObj.Result = false;
                retObj.ResultMessage = x.Message;
            }

            var retObjJson = JsonConvert.SerializeObject(retObj);

            return retObjJson;
        }

        

    }
}