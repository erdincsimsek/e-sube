﻿using ESube.Service.Helpers;
using ESube.Service.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ESube.Service.Controllers
{
    public class ReportController : Controller
    {

        public Report _Report = new Report();



        // GET: Report
        public ActionResult Index()
        {
            return View();
        }


        [HttpPost]
        public string GetListReport(string pMainCompanyCode)
        {

            var list = _Report.GetList(pMainCompanyCode, "",true);

            //Ignore Propertylerin Ignore larını temizlemek için
            JsonSerializerSettings settingsXX = new JsonSerializerSettings();
            settingsXX.ContractResolver = new IgnoreJsonAttributesResolver();

            var json = JsonConvert.SerializeObject(list, settingsXX);

            return json;
        }
    }
}