﻿using ESube.Service.Models;
using ESube.Service.Models.Enums;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using ESube.Service.Helpers;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;
using System.Data;
using Microsoft.Office.Interop.Excel;

namespace ESube.Service.Controllers
{
    public class StockController : Controller
    {
        // GET: DailyStock
        public DailyStock _Stock = new DailyStock();
        public GridView grid;

        public ActionResult Index()
        {
            return View();
        }


        [HttpPost]
        public string GetListStock(string pMainCompanyCode, string pStockDate,string pWareHouseCode)
        {

            var list = _Stock.GetList(pStockDate, pWareHouseCode, pMainCompanyCode);

            //Ignore Propertylerin Ignore larını temizlemek için
            JsonSerializerSettings settingsXX = new JsonSerializerSettings();
            settingsXX.ContractResolver = new IgnoreJsonAttributesResolver();

            var json = JsonConvert.SerializeObject(list, settingsXX);

            return json;
        }


        [HttpPost]
        public void GridExportToExcel(string pStockDate, string pWareHouseCode, string pMainCompanyCode)
        {
            Microsoft.Office.Interop.Excel.Application excel = new Microsoft.Office.Interop.Excel.Application();

            excel.Visible = true;
            Microsoft.Office.Interop.Excel.Workbook workbook = excel.Workbooks.Add(System.Reflection.Missing.Value);

            Microsoft.Office.Interop.Excel.Worksheet sheet1 = (Microsoft.Office.Interop.Excel.Worksheet)workbook.Sheets[1];

            int StartCol = 1;

            int StartRow = 1;
            string[] ColumnDesc = { "Firma Kodu", "Depo Kodu", "Stok Tarihi", "Ürün Kodu", "Ürün Adı", "Adet", "Küçük Birim", "Son Kullanım Tarihi" };
           // int[] ColumnRemove = { 7, 8, 9, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21,22 };
            string dosyaAdi = "DailyStock";
           //var table = _Stock.GetList(pRequestdate, pWareHouseCode, pMainCompanyCode);// Buraya veritabanınından gelen herhangi bir dataSource gelebilir.( DataTable, DataSet, kendi oluşturduğunuz, herhangi bir ICollection tipinde entitiy model)
            System.Data.DataTable dt = new System.Data.DataTable();
            dt = _Stock.ExcelExport(pStockDate, pWareHouseCode, pMainCompanyCode);
            //for (int i = ColumnRemove.Length-1; i >= 0; i--)
            //{
            //    dt.Columns.RemoveAt(ColumnRemove[i]);
            //} 
            for (int i = 0; i < ColumnDesc.Length; i++)
            {
                Microsoft.Office.Interop.Excel.Range rng = (Microsoft.Office.Interop.Excel.Range)excel.Cells[StartRow, StartCol + i];
                rng.Font.Bold = true;
                Microsoft.Office.Interop.Excel.Range myRange = (Microsoft.Office.Interop.Excel.Range)sheet1.Cells[StartRow, StartCol + i];

                myRange.Value2 = ColumnDesc[i].ToString();
                // myRange.Interior.Color = XlRgbColor.rgbBlue;
               // rng.Style.Font.Bold = true;
                //myRange.Style.Font.Color = "Blue";
                //myRange.Columns.AutoFit();
                //myRange.Borders.LineStyle = XlLineStyle.xlContinuous;
                //myRange.Borders.Weight = XlBorderWeight.xlThin;


            }
            StartRow++;

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                for (int j = 0; j < dt.Columns.Count; j++)
                {
                    Microsoft.Office.Interop.Excel.Range myRange = (Microsoft.Office.Interop.Excel.Range)sheet1.Cells[StartRow + i, StartCol + j];

                    myRange.Value2 = dt.Rows[i][j].ToString() == null ? "" : dt.Rows[i][j].ToString();
                   
                   // myRange.Style.Font.Bold = false;
                    //myRange.Columns.AutoFit();
                    //myRange.Borders.LineStyle = XlLineStyle.xlContinuous;
                    //myRange.Borders.Weight = XlBorderWeight.xlThin;
                }

               
            }

            Microsoft.Office.Interop.Excel.Range trange = sheet1.UsedRange;
            trange.Borders.LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
            trange.Borders.Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;
            trange.Columns.AutoFit();
            Response.ClearContent();
            Response.AddHeader("content-disposition", "attachment; filename=" + dosyaAdi + ".xls");

            Response.ContentType = "application/excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);

            


            Response.Write(sw.ToString());
            Response.End();

            excel.Quit();
            //return RedirectToAction("");
        }

    }
}