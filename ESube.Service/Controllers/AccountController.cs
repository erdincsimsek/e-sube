﻿using ESube.Service.Models.Helpers;
using ESube.Service.Models;
using ESube.Service.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ESube.Service.Context;
using System.Web.Security;
using System.Xml.Linq;
using System.Xml;
using Newtonsoft.Json;
using System.IO;
using System.Xml.Serialization;

namespace ESube.Service.Controllers
{
    public class AccountController : Controller
    {
        public ESubeContext _db = new ESubeContext();


        [AllowAnonymous]
        public ActionResult Login()
        {
            return View();
        }


        [AllowAnonymous]
        [HttpPost]
        public JsonResult Register(UserForm model)
        {
            ReturnObject retObj = new ReturnObject();

            //Validate RegisterForm
            retObj = model.ValidateRegister();

            //Sorun yoksa
            if (retObj.Result)
            {
                retObj = model.UserRegister();
            }


            //Sorun yoksa- kullanıcıyı session a yaz...
            if (retObj.Result)
            {
                User _User = _db.User.Where(u => u.EmailAddress == model.EmailAddress || u.Username == model.Username).FirstOrDefault();
                MainCompany _MainCompany = _db.MainCompany.Where(x => x.Id == _User.CompanyId).FirstOrDefault();

                UserRole _UserRole = new UserRole();
                string sessionUserRoleListStr = _UserRole.GetRoleListStrByUserId(_User.Id);

                Session["User"] = _User;
                Session["UserRoleListStr"] = sessionUserRoleListStr;
                Session["MainCompany"] = _MainCompany;
                FormsAuthentication.SetAuthCookie(model.EmailAddress, false);
            }

            return Json(retObj);
        }
        [AllowAnonymous]
        [HttpPost]
        public JsonResult Login(UserForm model)
        {
            ReturnObject retObj = new ReturnObject();

            //Validate RegisterForm
            retObj = model.ValidateLogin();


            //Sorun yoksa- kullanıcıyı session a yaz...
            if (retObj.Result)
            {
                User _User = _db.User.Where(u => u.EmailAddress == model.EmailAddressOrUsername || u.Username == model.EmailAddressOrUsername).FirstOrDefault();
                MainCompany _MainCompany = _db.MainCompany.Where(x => x.Id == _User.CompanyId).FirstOrDefault();

                UserRole _UserRole = new UserRole();
                string sessionUserRoleListStr = _UserRole.GetRoleListStrByUserId(_User.Id);

                Session["User"] = _User;
                Session["UserRoleListStr"] = sessionUserRoleListStr;
                Session["MainCompany"] = _MainCompany;
                FormsAuthentication.SetAuthCookie(_User.EmailAddress, false);
            }

            return Json(retObj);
        }




        public ActionResult Logout()
        {
            Session.Abandon();
            FormsAuthentication.SignOut();
            return RedirectToAction("Login","Account");
        }
    }
}