﻿using ESube.Service.Models;
using ESube.Service.Models.Enums;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using ESube.Service.Helpers;
using ESube.Service.Models.Helpers;

namespace ESube.Service.Controllers
{
    public class OrderController : Controller
    {
        public OrderNew _OrderNew = new OrderNew();
        public OrderLineNew _OrderLineNew = new OrderLineNew();

        # region HEMEL

        #endregion
        public ActionResult Index()
        {
            return View();
        }


        [HttpPost]
        public string GetListOrder(string pMainCompanyCode, string pOrderCode,EnumOrderType pOrderType, EnumSyncStatus pOrderStatusCode)
        {

            var list = _OrderNew.GetListAll(pMainCompanyCode, pOrderCode,pOrderType, pOrderStatusCode);

            //Ignore Propertylerin Ignore larını temizlemek için
            JsonSerializerSettings settingsXX = new JsonSerializerSettings();
            settingsXX.ContractResolver = new IgnoreJsonAttributesResolver();

            var json = JsonConvert.SerializeObject(list, settingsXX);

            return json;
        }

        [HttpPost]
        public string GetOrderWithDetail(string pMainCompanyCode, long pId, string pOrderCode)
        {
            var list = _OrderNew.GetOrderWithDetail(pMainCompanyCode, pId,pOrderCode);

            //Ignore Propertylerin Ignore larını temizlemek için
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.ContractResolver = new IgnoreJsonAttributesResolver();
            settings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;

            var json = JsonConvert.SerializeObject(list,settings);

            return json;
        }

        [HttpPost]
        public string GetOrderLine(int pOrderLineId)
        {
            var orderLine = _OrderLineNew.GetOrderLine(pOrderLineId);

            //Ignore Propertylerin Ignore larını temizlemek için
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.ContractResolver = new IgnoreJsonAttributesResolver();
            settings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;

            var json = JsonConvert.SerializeObject(orderLine, settings);

            return json;
        }

        [HttpPost]
        public string UpdateSyncStatus(string pMainCompanyCode, int pOrderId, string pOrderCode, int pStatusCode)
        {
            _OrderNew.UpdateSyncStatus(pMainCompanyCode, pOrderId, pOrderCode,"","", (EnumSyncStatus)pStatusCode);

            return "";
        }


        [HttpPost]
        public string UpdateTransferStatus(string pMainCompanyCode, int pOrderId, string pOrderCode, string pOrderType)
        {
            _OrderNew.UpdateTransferStatus(pMainCompanyCode, pOrderId, pOrderCode, pOrderType);

            return "";
        }


        [HttpPost]
        public string TransferOrder(string pCurrentOrderCode, string pNewOrderCode, string pNewOrderId,string pItemCodeListStr)
        {
            ReturnObject  _RetObj = _OrderNew.TransferOrder(pCurrentOrderCode, pNewOrderCode, pNewOrderId, pItemCodeListStr);

            //Ignore Propertylerin Ignore larını temizlemek için
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.ContractResolver = new IgnoreJsonAttributesResolver();

            var json = JsonConvert.SerializeObject(_RetObj, settings);

            return json;
        }

        //[HttpPost]
        //public string GetListGoodsOutDetail(string pOrderCode)
        //{
        //    var list = cGetirGoodsOutOrderDetail.GetListByOrderCode(pOrderCode);

        //    var json = JsonConvert.SerializeObject(list);
        //    return json;
        //}

        [HttpPost]
        public string GetListByOrder(int pOrderId,string pTableName)
        {
            return _OrderNew.GetListByOrder(pOrderId, pTableName); ;
        }





    }
}