﻿using ESube.Service.Models;
using ESube.Service.Models.Enums;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using ESube.Service.Helpers;

namespace ESube.Service.Controllers
{
    [Authorize]
    public class OrderGetirController : Controller
    {
        public GetirGoodsInOrder cGetirGoodsInOrder = new GetirGoodsInOrder();
        public GetirGoodsInOrderLine cGetirGoodsInOrderDetail = new GetirGoodsInOrderLine();

        public GetirGoodsOutOrder cGetirGoodsOutOrder = new GetirGoodsOutOrder();
        public GetirGoodsOutOrderLine cGetirGoodsOutOrderDetail = new GetirGoodsOutOrderLine();


        public ActionResult Index()
        {
            return View();
        }


        [HttpPost]
        public string GetListGoodsIn(string pOrderCode, EnumSyncStatus pOrderStatus)
        {

            var list = cGetirGoodsInOrder.GetListAll(pOrderCode, pOrderStatus);

            //Ignore Propertylerin Ignore larını temizlemek için
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.ContractResolver = new IgnoreJsonAttributesResolver();

            var json = JsonConvert.SerializeObject(list, settings);
    
            return json;
        }

        [HttpPost]
        public string GetListGoodsInDetail(string pOrderCode)
        {
            var list = cGetirGoodsInOrderDetail.GetListByOrderCode(pOrderCode);

         

            var json = JsonConvert.SerializeObject(list);

            return json;
        }

        [HttpPost]
        public string GetListGoodsOut(string pOrderCode, EnumSyncStatus pOrderStatus)
        {
            var list = cGetirGoodsOutOrder.GetListAll(pOrderCode, pOrderStatus);

            //Ignore Propertylerin Ignore larını temizlemek için
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.ContractResolver = new IgnoreJsonAttributesResolver();

            var json = JsonConvert.SerializeObject(list, settings);

            return json;
        }

        [HttpPost]
        public string GetListGoodsOutDetail(string pOrderCode)
        {
            var list = cGetirGoodsOutOrderDetail.GetListByOrderCode(pOrderCode);

            var json = JsonConvert.SerializeObject(list);
            return json;
        }


    }
}