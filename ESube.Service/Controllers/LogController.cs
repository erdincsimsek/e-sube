﻿using ESube.Service.Models;
using ESube.Service.Models.Enums;
using ESube.Service.Models.Helpers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ESube.Service.Controllers
{
    [Authorize]
    public class LogController : Controller
    {

        Log _Log = new Log();

        public ActionResult Index()
        {
            return View();
        }


        [HttpPost]
        public string GetListLog(string pSourceType,int pLogId, string pCompanyCode,string pSearchText,EnumSyncStatus pStatusCode)
        {
            ReturnObject retObj = _Log.GetListLog(pSourceType,pLogId, pCompanyCode, pSearchText,pStatusCode);

            var retObjJson = JsonConvert.SerializeObject(retObj);

            //json = JsonConvert.SerializeObject(json, new JsonSerializerSettings
            //{
            //    ContractResolver = new AllPropertiesResolver()
            //});

            return retObjJson;
        }
    }
}