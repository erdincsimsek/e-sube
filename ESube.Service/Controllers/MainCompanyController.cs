﻿using ESube.Service.Models.Helpers;
using ESube.Service.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ESube.Service.Models;
using Newtonsoft.Json;

namespace ESube.Service.Controllers
{
    public class MainCompanyController : Controller
    {
        public MainCompany _MainCompany = new MainCompany();

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public string GetList()
        {
            var list = _MainCompany.GetListAll();
            var json = JsonConvert.SerializeObject(list);
            return json;
        }

    }
}