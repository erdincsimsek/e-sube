﻿using ESube.Service.Filters;
using ESube.Service.Models;
using ESube.Service.Repository;
using System;
using System.Web.Mvc;
using ESube.Service.Models.Helpers;

namespace ESube.Service.Controllers
{
    //[ValidateSessionAttribute]
    [Authorize]
    public class ApplicationKeysController : BaseController
    {
        IClientKeys _IClientKeys;
        //IRegisterCompany _IRegisterCompany;  

        //public ApplicationKeysController()
        //{
        //    _IClientKeys = new ClientKeysConcrete();
        //    _IRegisterCompany = new RegisterCompanyConcrete();

        //}

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult GetApplicationKeys()
        {
            ClientKey clientkeys = new ClientKey();

            int SessionUserId = Convert.ToInt32(SessionUser.Id);

            try
            {
                // Validating ClientID and ClientSecert already Exists
                var keyExists = _IClientKeys.IsUniqueKeyAlreadyGenerate(SessionUserId);

                if (keyExists)
                {
                    // Getting Generate ClientID and ClientSecert Key By UserID
                    clientkeys = _IClientKeys.GetGenerateUniqueKeyByUserID(SessionUserId);
                }
                else
                {
                    string clientID = string.Empty;
                    string clientSecert = string.Empty;
                    long companyId = 0;

                    //var company = _IRegisterCompany.FindCompanyByUserId(SessionUserId);
                    var company = 0;
                    //companyId = company.Id;
                    companyId = 1;

                    //Generate Keys
                    _IClientKeys.GenerateUniqueKey(out clientID, out clientSecert);

                    //Saving Keys Details in Database
                    clientkeys.ClientKeyID = 0;
                    clientkeys.CompanyID = companyId;
                    clientkeys.CreateOn = DateTime.Now;
                    clientkeys.ClientID = clientID;
                    clientkeys.ClientSecret = clientSecert;
                    clientkeys.UserID = SessionUser.Id;
                    _IClientKeys.SaveClientIDandClientSecert(clientkeys);

                }

            }
            catch (Exception)
            {
                throw;
            }

            return Json(clientkeys);
        }

        [HttpPost]
        public JsonResult ReGenerateClientKeys(ClientKey clientkeys)
        {
            try
            {
                string clientID = string.Empty;
                string clientSecert = string.Empty;
                int SessionUserId = Convert.ToInt32(SessionUser.Id);


                //Generate Keys
                _IClientKeys.GenerateUniqueKey(out clientID, out clientSecert);

                //Updating ClientID and ClientSecert 
                //var company = _IRegisterCompany.FindCompanyByUserId(SessionUserId);
                var company = "";
                //clientkeys.CompanyID = company.Id;
                clientkeys.CreateOn = DateTime.Now;
                clientkeys.ClientID = clientID;
                clientkeys.ClientSecret = clientSecert;
                clientkeys.UserID = SessionUserId;
                _IClientKeys.UpdateClientIDandClientSecert(clientkeys);

                //return RedirectToAction("GenerateKeys");
            }
            catch (Exception ex)
            {
            }

            return Json(clientkeys);

        }

    }
}
