﻿using ESube.Service.Context;
using ESube.Service.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ESube.Service.AES256Encryption;
using Newtonsoft.Json;
using ESube.Service.Models.WebApi;
using System.Web.Http.Description;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace ESube.Service.Controllers.Api
{
    /// <summary>
    /// TR : Kullanıcı İşlemleri Apileri - 
    /// EN : User webservices
    /// </summary>
    public class AuthenticationController : ApiController
    {
        public Token cToken = new Token();
        bool Result = false;
        string Token = "";

        /// <summary>
        /// TR : Yeni bir token üretir - 
        /// EN : Creates a new token
        /// </summary>
        [Route("api/login")]
        [ResponseType(typeof(ApiResponseLogin))]
        [HttpPost]
        public async Task<IHttpActionResult> Login(UserForm pUserForm)
        {
            bool IsValidLogin = false;
            ApiResponseLogin apiResponse = new ApiResponseLogin();
            Token _Token = new Token();


            IsValidLogin = ValidateLogin(pUserForm.EmailAddressOrUsername, pUserForm.Password);

            //Login Bilgileri Doğru
            if (IsValidLogin)
            {
                _Token = cToken.InsertOrUpdateToken(pUserForm.EmailAddressOrUsername, pUserForm.Password);

                apiResponse.StatusCode = EnumApiHttpStatus.OK;
                apiResponse.Message = "Login Başarılı";
                apiResponse.Token = _Token.TokenKey;
                apiResponse.TokenExpireDate = _Token.ExpireDate.ToString("dd'.'MM'.'yyyy HH:mm:ss"); ;
            }
            else
            {
                apiResponse.StatusCode = EnumApiHttpStatus.Unauthorized;
                apiResponse.Message = "Giriş Bilgileri Hatalı";
            }


            return Ok(apiResponse);
        }


    
    

        //private bool InsertToken(ApiLogin pApiRequest, int pCompanyId){
        //    Result = false;
        //    _tokensManager = new TokensManager();
        //    Token = GenerateToken();
        //    try{
        //        using (ESubeContext db = new ESubeContext())
        //        {
        //            _tokensManager.TokenKey = Token;
        //            _tokensManager.CreatedOn = DateTime.Now;
        //            _tokensManager.ExpiresOn = DateTime.Now;
        //            _tokensManager.CompanyID = pCompanyId;
        //            db.TokensManager.Add(_tokensManager);
        //            db.SaveChanges();
        //            Result = true;
        //        }              
        //    }
        //    catch (Exception x){
        //        Result = false;
        //    }

        //    return Result;
        //}

        //private bool UpdateToken(ApiLogin pApiRequest, int pCompanyId){
        //    Result = false;
        //    _tokensManager = new TokensManager();
        //    Token = GenerateToken();
        //    try
        //    {
        //        using (ESubeContext db = new ESubeContext())
        //        {
        //            _tokensManager = (from p in db.TokensManager where p.CompanyID == pCompanyId  select p).FirstOrDefault();
        //            _tokensManager.TokenKey = Token;
        //            _tokensManager.CreatedOn = DateTime.Now;
        //            _tokensManager.ExpiresOn = DateTime.Now;
        //            db.SaveChanges();
        //            Result = true;
        //        }
        //        Result = true;
        //    }
        //    catch (Exception x)
        //    {
        //        Result = false;
        //    }

        //    return Result;
        //}

        //private string GenerateToken(){
        //    return  Guid.NewGuid().ToString().ToUpper();
        //}

        private bool ValidateLogin(string pEmailAddressOrUsername, string pPassword)
        {
            ESubeContext _db = new ESubeContext();
            string pass = EncryptionLibrary.EncryptText(pPassword);
            int count = _db.User.Where(u => (u.EmailAddress == pEmailAddressOrUsername || u.Username == pEmailAddressOrUsername) && u.Password == pass).Count();

            return count == 1 ? true : false; ;
        }
    }
    public class UserForm
    {
        public string EmailAddressOrUsername { get; set; }
        public string Password { get; set; }

    }
}
