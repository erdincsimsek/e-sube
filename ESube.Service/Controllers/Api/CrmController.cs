﻿    using ESube.Service.Context;
using ESube.Service.Filters;
using ESube.Service.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;
using ESube.Service.Models.Helpers;
using ESube.Service.Models.WebApi;
using Newtonsoft.Json;
using System.IO;
using System.Text;
using System.Runtime.Serialization.Json;
using ESube.Service.Models.Enums;
using System.Web;
using System.Threading.Tasks;
using ESube.Service.Models.Crm;

namespace ESube.Service.Controllers.Api
{
    public class CrmController : ApiController
    {
        ApiSession _ApiSession = new ApiSession();
        ApiResponse _ApiResponse = new ApiResponse();
        Token _Token = new Token();
        Log _Log = new Log();

        Tender _Tender = new Tender();

        TripAndOrder _TripAndOrder = new TripAndOrder();

        string ExceptionMessage = "";
        string InnerExceptionMessage = "";


        [Route("api/crm/orderAndTripInfo")]
        [ResponseType(typeof(ApiResponse))]
        [HttpGet]
        public async Task<IHttpActionResult> GetCrmOrderAndTripInfo(string pOrderCode,string pTripCode)
        {
            string CompanyCode = "";

            try
            {
                //TOKEN KONTROLÜ ( Doğru mu, Yanlış Mı)
                ReturnObject _RetObj = _Token.ValidateWebApiToken(Request);
                if (!_RetObj.Result)
                {
                    _ApiResponse.StatusCode = EnumApiHttpStatus.Unauthorized;
                    _ApiResponse.Message = _RetObj.ResultMessage;
                }
                else
                {
                    //TOKEN ÜZERİNDEN KULLANICI BİLGİLERİNİ BUL VE APİ SESSİON A YAZ
                    _ApiSession = _ApiSession.GetByToken(Request);
                    CompanyCode = _ApiSession.MainCompany.Code;

                    //INSERT LOG
                    _Log = _Log.InsertWebApiLog(Request, CompanyCode, String.Concat("pOrderCode : ",pOrderCode, "pTripCode : ",pTripCode), HttpContext.Current);


                    //DATAYI ÇEK
                    _ApiResponse.Data = _TripAndOrder.GetList(pOrderCode, pTripCode);


                }

            }
            catch (Exception e)
            {
                _ApiResponse.StatusCode = EnumApiHttpStatus.MethodFailure;
                _ApiResponse.Message = "Error Code : " + _Log.Id.ToString();

                _Log.StatusCode = EnumLogStatus.Error;
                _Log.StatusMessage = _ApiResponse.Message;
                _Log.ErrorMessage = (e.InnerException != null) ? e.InnerException.InnerException.Message : "";
                _Log.ErrorMessage = string.IsNullOrEmpty(_Log.ErrorMessage) ? e.Message : _Log.ErrorMessage;

            }

            //LOG Update   
            if (_Log != null)
            {
                _Log.ResponseBody = JsonConvert.SerializeObject(_ApiResponse).ToString();
                _Log.UpdateWebApiLog(_Log);
            }


            return Ok(_ApiResponse);
        }
    }
}
