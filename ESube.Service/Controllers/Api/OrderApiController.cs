﻿using ESube.Service.Context;
using ESube.Service.Filters;
using ESube.Service.Helpers;
using ESube.Service.Models;
using ESube.Service.Models.Enums;
using ESube.Service.Models.Helpers;
using ESube.Service.Models.OrderReturn;
using ESube.Service.Models.WebApi;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;

namespace ESube.Service.Controllers
{

    /// <summary>
    /// TR:  Sipariş methodlarını içerir - 
    /// EN:  Order webserivces
    /// </summary>
    //[APIAuthorizeAttribute]
    public class OrderApiController : ApiController
    {

        ApiSession _ApiSession = new ApiSession();
        ApiResponse _ApiResponse = new ApiResponse();
        Token _Token = new Token();
        Log _Log = new Log();

        OrderNew _Order = new OrderNew();
        //DailyStokcVW _DailyStokcVW = new DailyStokcVW();
        DailyStock _DailyStock = new DailyStock();
        OrderReturn _OrderReturn = new OrderReturn();
        ReturnObject _RetObj = new ReturnObject();




        /// <summary>
        /// TR : Siparişleri ve siparişlerdeki firma ve ürünleri oluşturur
        /// EN : Creates orders with companies and products
        /// </summary>
        /// <param name="pOrder"></param>
        /// <returns></returns>
        [ResponseType(typeof(ApiResponse))]
        [Route("api/order")]
        [HttpPost]
        public async Task<IHttpActionResult> PostOrder(OrderNew pOrder)
        {
            try
            {
                //TOKEN KONTROLÜ ( Doğru mu, Yanlış Mı)
                ReturnObject _RetObj = _Token.ValidateWebApiToken(Request);

                //TOKEN İLE İLGİLİ BİR HATA VARSA
                if (!_RetObj.Result){
                    _ApiResponse.StatusCode = EnumApiHttpStatus.Unauthorized;
                    _ApiResponse.Message = _RetObj.ResultMessage;
                }
                else{

                    //TOKEN ÜZERİNDEN KULLANICI BİLGİLERİNİ BUL VE APİ SESSİON A YAZ
                    _ApiSession = _ApiSession.GetByToken(Request);
                    pOrder.MainCompanyCode = _ApiSession.MainCompany.Code;

                    //SİPARİŞİ KAYDETMEDEN ÖNCE KONTROLLERDEN GEÇİR
                    _RetObj = _Order.InsertOrderBefore(pOrder);

                    //INSERT LOG
                    _Log = _Log.InsertWebApiLog(Request, pOrder.MainCompanyCode, pOrder, HttpContext.Current);

                    //MODEL VALIDATON
                    if (!ModelState.IsValid){
                        var messages = string.Join(" | ", ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage));
                        _ApiResponse.StatusCode = EnumApiHttpStatus.BadRequest;
                        _ApiResponse.Message = messages;
                    }
                    else
                    {                       
                        //SORUN VARSA
                        if (!_RetObj.Result){
                            _ApiResponse.StatusCode = EnumApiHttpStatus.Error;
                            _ApiResponse.Message = _RetObj.ResultMessage;
                        }
                        else{
                            _Order.InsertOrder(pOrder);

                            _ApiResponse.StatusCode = EnumApiHttpStatus.OK;
                            _ApiResponse.Message = _ApiResponse.Status;
                            _ApiResponse.Data = null;
                        }
                    }

                }
            }
            catch (Exception e){
                string errorMsg = e.InnerException != null && e.InnerException.InnerException != null ? e.InnerException.InnerException.Message : e.InnerException.Message;


                _ApiResponse.StatusCode = EnumApiHttpStatus.MethodFailure;
                _ApiResponse.Message = "Error Code : " + _Log.Id.ToString();

                _Log.StatusCode = EnumLogStatus.Error;
                _Log.StatusMessage = errorMsg;
            }

            //LOG Update   
            if (_Log != null){
                _Log.ResponseBody = JsonConvert.SerializeObject(_ApiResponse).ToString();
                _Log.UpdateWebApiLog(_Log);
            }

            return Ok(_ApiResponse);
        }



        /// <summary>
        /// Firmanın Dinçer'de bulunan stoğunu verir
        /// </summary>
        /// <param name="pRequestDate"></param>
        /// <param name="pWareHouseCode"></param>
        /// <returns></returns>
        [Route("api/order/dailyStock")]
        [ResponseType(typeof(ApiResponse))]
        [HttpGet]
        public IHttpActionResult GetDailyStock(string pRequestDate, string pWareHouseCode)
        {
            string CompanyCode = "";
            try
            {
                //TOKEN KONTROLÜ ( Doğru mu, Yanlış Mı)
                ReturnObject _RetObj = _Token.ValidateWebApiToken(Request);
                if (!_RetObj.Result)
                {
                    _ApiResponse.StatusCode = EnumApiHttpStatus.Unauthorized;
                    _ApiResponse.Message = _RetObj.ResultMessage;
                }
                else
                {
                    //TOKEN ÜZERİNDEN KULLANICI BİLGİLERİNİ BUL VE APİ SESSİON A YAZ
                    _ApiSession = _ApiSession.GetByToken(Request);
                    CompanyCode = _ApiSession.MainCompany.Code;

                    //INSERT LOG
                    _Log = _Log.InsertWebApiLog(Request, CompanyCode, "pRequestDate : " + pRequestDate + "  " + " pWareHouseCode : " + pWareHouseCode, HttpContext.Current);

                    //MODEL VALIDATION
                    if (!ModelState.IsValid)
                    {
                        var messages = string.Join(" | ", ModelState.Values
                                            .SelectMany(v => v.Errors)
                                            .Select(e => e.ErrorMessage));

                        _ApiResponse.StatusCode = EnumApiHttpStatus.BadRequest;
                        _ApiResponse.Message = messages;
                    }
                    else
                    {
                        _ApiResponse.Data = _DailyStock.GetList(pRequestDate, pWareHouseCode, CompanyCode);
                        _ApiResponse.StatusCode = EnumApiHttpStatus.OK;
                        _ApiResponse.Message = _ApiResponse.Status;
                    }

                }

            }
            catch (Exception e)
            {
                _ApiResponse.StatusCode = EnumApiHttpStatus.MethodFailure;
                _ApiResponse.Message = "Error Code : " + _Log.Id.ToString();

                _Log.StatusCode = EnumLogStatus.Error;
                _Log.StatusMessage = _ApiResponse.Message;
            }

            //LOG Update   
            if (_Log != null)
            {
                _Log.ResponseBody = JsonConvert.SerializeObject(_ApiResponse).ToString();
                _Log.UpdateWebApiLog(_Log);
            }

            return Ok(_ApiResponse);
        }



        /// <summary>
        /// Siparişin statüsünü alır.
        /// </summary>
        /// <param name="pOrderCode"></param>
        /// <returns></returns>
        [Route("api/order/status")]
        [ResponseType(typeof(ApiResponse))]
        [HttpGet]
        public IHttpActionResult GetOrderStatus(int pOrderType, string pOrderCode)
        {
            string CompanyCode = "";
            try
            {
                //TOKEN KONTROLÜ ( Doğru mu, Yanlış Mı)
                ReturnObject _RetObj = _Token.ValidateWebApiToken(Request);
                if (!_RetObj.Result)
                {
                    _ApiResponse.StatusCode = EnumApiHttpStatus.Unauthorized;
                    _ApiResponse.Message = _RetObj.ResultMessage;
                }
                else
                {
                    //TOKEN ÜZERİNDEN KULLANICI BİLGİLERİNİ BUL VE APİ SESSİON A YAZ
                    _ApiSession = _ApiSession.GetByToken(Request);
                    CompanyCode = _ApiSession.MainCompany.Code;

                    //INSERT LOG
                    _Log = _Log.InsertWebApiLog(Request, CompanyCode, "pOrderCode : " + pOrderCode, HttpContext.Current);

                    //MODEL VALIDATION
                    if (!ModelState.IsValid)
                    {
                        var messages = string.Join(" | ", ModelState.Values
                                            .SelectMany(v => v.Errors)
                                            .Select(e => e.ErrorMessage));

                        _ApiResponse.StatusCode = EnumApiHttpStatus.BadRequest;
                        _ApiResponse.Message = messages;
                    }
                    else
                    {
                        //Gelen Koda Ait Sipariş Türünü Bul
                        if (CompanyCode == "HEMEL")
                        {


                            //EnumOrderType orderType = _Order.GetOrderType(CompanyCode, pOrderCode);
                            EnumOrderType orderType = (EnumOrderType)pOrderType;

                            if (orderType == EnumOrderType.None)
                            {
                                _ApiResponse.StatusCode = EnumApiHttpStatus.OK;
                                _ApiResponse.Message = "Sipariş Bulunamadı";
                            }
                            else
                            {

                                var list = _OrderReturn.GetList(CompanyCode, orderType, pOrderCode);

                                _ApiResponse.Data = list;
                                _ApiResponse.StatusCode = EnumApiHttpStatus.OK;
                                _ApiResponse.Message = _ApiResponse.Status;
                            }
                        }
                        else
                        {
                            _ApiResponse.StatusCode = EnumApiHttpStatus.OK;
                            _ApiResponse.Message = _ApiResponse.Status;
                        }
                    }

                }

            }
            catch (Exception e)
            {
                _ApiResponse.StatusCode = EnumApiHttpStatus.MethodFailure;
                _ApiResponse.Message = "Error Code : " + _Log.Id.ToString() +" - "+ e.Message;

                _Log.StatusCode = EnumLogStatus.Error;
                _Log.StatusMessage = _ApiResponse.Message;
            }

            //LOG Update   
            if (_Log != null)
            {
                _Log.ResponseBody = JsonConvert.SerializeObject(_ApiResponse).ToString();
                _Log.UpdateWebApiLog(_Log);
            }

            return Ok(_ApiResponse);
        }

        /// <summary>
        /// Müşteri kendi stok verilerini Dincer tarfaına gönderir.
        /// </summary>
        /// <param name="pStockObject"></param>
        /// <returns></returns>
        [ResponseType(typeof(ApiResponse))]
        [Route("api/order/stockSnapshot")]
        [HttpPost]
        public IHttpActionResult sendStockSnapshot(StockObject pStockObject)
        {

            string CompanyCode = "";
            try
            {
                //TOKEN KONTROLÜ ( Doğru mu, Yanlış Mı)
                ReturnObject _RetObj = _Token.ValidateWebApiToken(Request);
                if (!_RetObj.Result)
                {
                    _ApiResponse.StatusCode = EnumApiHttpStatus.Unauthorized;
                    _ApiResponse.Message = _RetObj.ResultMessage;
                }
                else
                {
                    //TOKEN ÜZERİNDEN KULLANICI BİLGİLERİNİ BUL VE APİ SESSİON A YAZ
                    _ApiSession = _ApiSession.GetByToken(Request);
                    CompanyCode = _ApiSession.MainCompany.Code;

                    //INSERT LOG
                    _Log = _Log.InsertWebApiLog(Request, CompanyCode, pStockObject, HttpContext.Current);

                    //MODEL VALIDATION
                    if (string.IsNullOrEmpty(pStockObject.companyBranchCode))
                    {
                        _ApiResponse.StatusCode = EnumApiHttpStatus.BadRequest;
                        _ApiResponse.Message = "Error Code : " + _Log.Id.ToString() + "| " + "Depo bilgisi boş geçilemez.";

                    }
                    else if (string.IsNullOrEmpty(pStockObject.stockDate))
                    {
                        _ApiResponse.StatusCode = EnumApiHttpStatus.BadRequest;
                        _ApiResponse.Message = "Error Code : " + _Log.Id.ToString() + "| " + "Stok tarihi bilgisi boş geçilemez.";
                    }
                    else
                    {

                        _DailyStock.AddStockObjectList(pStockObject, _ApiSession.MainCompany.Code);

                        _ApiResponse.StatusCode = EnumApiHttpStatus.OK;
                        _ApiResponse.Message = _ApiResponse.Status;
                    }

                }

            }
            catch (Exception e)
            {
                _ApiResponse.StatusCode = EnumApiHttpStatus.MethodFailure;
                _ApiResponse.Message = "Error Code : " + _Log.Id.ToString();

                _Log.StatusCode = EnumLogStatus.Error;
                _Log.StatusMessage = _ApiResponse.Message;
            }

            //LOG Update   
            if (_Log != null)
            {
                _Log.ResponseBody = JsonConvert.SerializeObject(_ApiResponse).ToString();
                _Log.UpdateWebApiLog(_Log);
            }

            return Ok(_ApiResponse);
        }





        /// <summary>
        /// Siparişi siler
        /// </summary>
        /// <param name="sorder"></param>
        /// <returns></returns>
        [ResponseType(typeof(ApiResponse))]
        [Route("api/order")]
        [HttpDelete]
        public IHttpActionResult DeleteOrder(string pOrderCode, string pCompanyBranchCode)
        {
            string CompanyCode = "";
            try
            {
                //TOKEN KONTROLÜ ( Doğru mu, Yanlış Mı)
                ReturnObject _RetObj = _Token.ValidateWebApiToken(Request);
                if (!_RetObj.Result)
                {
                    _ApiResponse.StatusCode = EnumApiHttpStatus.Unauthorized;
                    _ApiResponse.Message = _RetObj.ResultMessage;
                }
                else
                {
                    //TOKEN ÜZERİNDEN KULLANICI BİLGİLERİNİ BUL VE APİ SESSİON A YAZ
                    _ApiSession = _ApiSession.GetByToken(Request);
                    CompanyCode = _ApiSession.MainCompany.Code;

                    //INSERT LOG
                    _Log = _Log.InsertWebApiLog(Request, CompanyCode, "pOrderCode : " + pOrderCode + " | pCompanyBranchCode : " + pCompanyBranchCode, HttpContext.Current);

                    //MODEL VALIDATION
                    if (!ModelState.IsValid)
                    {
                        var messages = string.Join(" | ", ModelState.Values
                                            .SelectMany(v => v.Errors)
                                            .Select(e => e.ErrorMessage));

                        _ApiResponse.StatusCode = EnumApiHttpStatus.BadRequest;
                        _ApiResponse.Message = messages;
                    }
                    else
                    {

                        //_Order.DeleteOrder(pOrderCode, pCompanyBranchCode);

                        _ApiResponse.StatusCode = EnumApiHttpStatus.OK;
                        _ApiResponse.Message = _ApiResponse.Status;
                        _ApiResponse.Data = null;
                    }

                }

            }
            catch (Exception e)
            {
                _ApiResponse.StatusCode = EnumApiHttpStatus.MethodFailure;
                _ApiResponse.Message = "Error Code : " + _Log.Id.ToString();

                _Log.StatusCode = EnumLogStatus.Error;
                _Log.StatusMessage = _ApiResponse.Message;
            }

            //LOG Update   
            if (_Log != null)
            {
                _Log.ResponseBody = JsonConvert.SerializeObject(_ApiResponse).ToString();
                _Log.UpdateWebApiLog(_Log);
            }

            return Ok(_ApiResponse);
        }


    }





    public class singleOrderRequest
    {
        /// <summary>
        /// Firma kodu
        /// </summary>
        //[Required]
        public string mainCompanyCode { get; set; }
        /// <summary>
        /// Depo kodu
        /// </summary>
        [Required]
        public string companyBranchCode { get; set; }
        /// <summary>
        /// Sipariş numarası
        /// </summary>
        [Required]
        public string orderCode { get; set; }

        /// <summary>
        /// Sipariş Referans numarası
        /// </summary>
        [Required]
        public string documentID { get; set; }

        ///// <summary>
        ///// Sipariş tipi
        ///// </summary>
        //[Required]
        //public EnumOrderType orderType { get; set; }
    }
    public class singleOrderDocumentStatus
    {
        public string orderCode { get; set; }
        public string documentId { get; set; }

        public string reason { get; set; }
        public string integrationId { get; set; }
        public EnumDocumentStatus status { get; set; }
        public string[] items { get; set; }
    }

    public class dailyStockRequest
    {
        //string _apikey;
        //public string apikey
        //{
        //    get { return _apikey; }
        //    set { _apikey = value; }
        //}

        string _requestDate;
        public string requestDate
        {
            get { return _requestDate; }
            set { _requestDate = value; }
        }

        string _itemCode;
        public string itemCode
        {
            get { return _itemCode; }
            set { _itemCode = value; }
        }

        string _warehouseCode;
        public string warehouseCode
        {
            get { return _warehouseCode; }
            set { _warehouseCode = value; }
        }

        string _location;
        public string location
        {
            get { return _location; }
            set { _location = value; }
        }




    }


}