﻿using ESube.Service.Context;
using ESube.Service.Filters;
using ESube.Service.Models;
using ESube.Service.Models.Enums;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

namespace ESube.Service.Controllers
{
    /// <summary>
    /// Getir entegrasyon apileri
    /// </summary>
    [RoutePrefix("api/getirIntegration")]
    // [APIAuthorizeAttribute]
    [ApiExplorerSettings(IgnoreApi = false)]

    public class GetirIntegrationController : ApiController
    {

        Log _Log = new Log();

        // private ESubeContext db = new ESubeContext();

        /// <summary>
        /// Malzeme insert eder
        /// </summary>
        [HttpPost]
        [Route("postCreateItem")]
        [ResponseType(typeof(clientGetirResponse))]
        public IHttpActionResult PostCreateItem(GetirItem item) 
        {
            //INSERT LOG
            _Log = _Log.InsertWebApiLog(Request, "GT", item, HttpContext.Current);

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            clientGetirResponse clientResponse = new clientGetirResponse();
            clientResponse.Errors = new List<clientGetirResponseError>();
            clientResponse.MethodName = "PostCreateItem";

            try
            {
                string message = string.Empty;
                if (item.InventoryItemBarcodes == null)
                {
                    message = "Item barcode list is null.";
                }
                if (item.InventoryItemPackTypes == null)
                {
                    message = message + "Item package types list is null.";
                }
                if (string.IsNullOrEmpty(item.Description))
                {
                    message = message + "Item description is null.";
                }

                if (string.IsNullOrEmpty(item.ItemType))
                {
                    message = message + "Item type is null.";
                }
                //if (string.IsNullOrEmpty(item.InventItemGroup))
                //{
                //    message = message + "Item group is null.";
                //}
                if (string.IsNullOrEmpty(item.InventoryItemCode))
                {
                    message = message + "Item code is null";
                }
                foreach (var obj in item.InventoryItemBarcodes)
                {
                    if (string.IsNullOrEmpty(obj.Barcode))
                    {
                        message = message + "Item barcode is null.";
                    }
                }
                foreach (var obj in item.InventoryItemPackTypes)
                {
                    if (obj.IncludesCUQuantity == 0)
                    {
                        message = message + "Item includes quantity is 0.";
                    }
                }
                using (ESubeContext db = new ESubeContext())
                {
                    if (string.IsNullOrEmpty(message))
                    {
                        item.CreateDate = DateTime.Now;
                        item.SyncStatusCode = EnumSyncStatus.SyncWaiting;
                        db.GetirItem.Add(item);
                        db.SaveChanges();
                        clientResponse.Success = true;
                    }
                    else
                    {
                        clientResponse.Success = false;
                        clientGetirResponseError df = new clientGetirResponseError();
                        df.ErrorCode = EnumResponseCode.createItemError;
                        df.ErrorMessage = string.Format("item inserted error  : {0} - {1}", item.InventoryItemCode, message);
                        clientResponse.Errors.Add(df);
                    }

                    return Ok(clientResponse);
                }
            }
            catch (Exception e)
            {
                clientResponse.Success = false;
                clientGetirResponseError df = new clientGetirResponseError();
                df.ErrorCode = EnumResponseCode.createItemError;
                df.ErrorMessage = string.Format("item inserted error  : {0} - {1}", item.InventoryItemCode, e.InnerException == null ? e.Message : e.InnerException.Message);
                clientResponse.Errors.Add(df);
                return Ok(clientResponse);
            }
        }


        /// <summary>
        /// Müşteri / Depo insert eder
        /// </summary>
        [HttpPost]
        [Route("postCreateCompany")]
        [ResponseType(typeof(clientGetirResponse))]
        public IHttpActionResult PostGetirCompany(GetirCompany item)
        {
            //INSERT LOG
            _Log = _Log.InsertWebApiLog(Request, "GT", item, HttpContext.Current);

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            using (ESubeContext db = new ESubeContext())
            {
                clientGetirResponse clientResponse = new clientGetirResponse();
                var checkItem = db.GetirCompany.Where(x => x.PartyCode == item.PartyCode).FirstOrDefault<GetirCompany>();
                clientResponse.Errors = new List<clientGetirResponseError>();
                clientResponse.MethodName = "PostGetirCompany";

                try
                {
                    string message = string.Empty;
                    if (string.IsNullOrEmpty(item.PartyAddress))
                    {
                        message = "Company adress is null.";
                    }
                    if (string.IsNullOrEmpty(item.PartyCode))
                    {
                        message = "Company code is null";
                    }
                    if (string.IsNullOrEmpty(item.PartyDescription))
                    {
                        message = "Company description is null.";
                    }
                    if (string.IsNullOrEmpty(message))
                    {
                        item.CreateDate = DateTime.Now;
                        item.SyncStatusCode = EnumSyncStatus.SyncWaiting;

                        db.GetirCompany.Add(item);
                        db.SaveChanges();
                        clientResponse.Success = true;
                    }
                    else
                    {
                        clientResponse.Success = false;
                        clientGetirResponseError df = new clientGetirResponseError();
                        df.ErrorCode = EnumResponseCode.createItemError;
                        df.ErrorMessage = string.Format("company inserted error  : {0} - {1}", item.PartyCode, message);
                        clientResponse.Errors.Add(df);
                    }
                    return Ok(clientResponse);
                }
                catch (Exception e)
                {
                    clientResponse.Success = false;
                    clientGetirResponseError df = new clientGetirResponseError();
                    df.ErrorCode = EnumResponseCode.createCompanyError;
                    df.ErrorMessage = string.Format("company inserted error  : {0} - {1}", item.PartyCode, e.InnerException == null ? e.Message : e.InnerException.Message);
                    clientResponse.Errors.Add(df);
                    return Ok(clientResponse);
                }
            }
        }

        /// <summary>
        /// Çıkış sipariş tanımlar
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("postCreateGoodOut")]
        [ResponseType(typeof(clientGetirResponse))]
        public IHttpActionResult PostGetirGoodsOutOrder(GetirGoodsOutOrder item)
        {
            //INSERT LOG
            _Log = _Log.InsertWebApiLog(Request, "GT", item, HttpContext.Current);

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            using (ESubeContext db = new ESubeContext())
            {
                clientGetirResponse clientResponse = new clientGetirResponse();
                clientResponse.MethodName = "postCreateGoodOut";
                clientResponse.Errors = new List<clientGetirResponseError>();

                if (item.WarehouseOrderItems == null)
                {
                    clientResponse.Success = false;
                    clientGetirResponseError df = new clientGetirResponseError();
                    df.ErrorCode = EnumResponseCode.createGoodsOutError;
                    df.ErrorMessage = string.Format("receiptItems is null : " + item.WaorCode);
                    clientResponse.Errors.Add(df);

                    return Ok(clientResponse);
                }
                try
                {

                    switch (item.WaorWarehouseOrderTypeID)
                    {
                        case "100": item.WaorWarehouseOrderTypeID = "G41"; break;
                        case "200": item.WaorWarehouseOrderTypeID = "G42"; break;
                        case "300": item.WaorWarehouseOrderTypeID = "G43"; break;
                        case "400": item.WaorWarehouseOrderTypeID = "G44"; break;
                        case "500": item.WaorWarehouseOrderTypeID = "G45"; break;
                        case "600": item.WaorWarehouseOrderTypeID = "G46"; break;
                        case "101": item.WaorWarehouseOrderTypeID = "G47"; break;
                        case "102": item.WaorWarehouseOrderTypeID = "G48"; break;
                        case "103": item.WaorWarehouseOrderTypeID = "G49"; break;
                        case "104": item.WaorWarehouseOrderTypeID = "G40"; break;
                        case "105": item.WaorWarehouseOrderTypeID = "G39"; break;
                    }



                    item.CreateDate = DateTime.Now;
                    item.SyncStatusCode = EnumSyncStatus.SyncWaiting;
                    db.GetirGoodsOutOrder.Add(item);
                    db.SaveChanges();
                    clientResponse.Success = true;
                    return Ok(clientResponse);
                }
                catch (Exception e)
                {
                    clientResponse.Success = false;
                    clientGetirResponseError df = new clientGetirResponseError();
                    df.ErrorCode = EnumResponseCode.createGoodsOutError;
                    df.ErrorMessage = string.Format("order inserted error  : {0} - {1}", item.WaorCode, e.InnerException == null ? e.Message : e.InnerException.Message);
                    clientResponse.Errors.Add(df);

                    return Ok(clientResponse);
                }
            }
        }
        /// <summary>
        /// Giriş siparişi tanımlar
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("postCreateGoodIn")]
        [ResponseType(typeof(clientGetirResponse))]
        public IHttpActionResult PostGetirGoodsInOrder(GetirGoodsInOrder item)
        {
            //INSERT LOG
            _Log = _Log.InsertWebApiLog(Request, "GT", item, HttpContext.Current);

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            using (ESubeContext db = new ESubeContext())
            {
                clientGetirResponse clientResponse = new clientGetirResponse();
                clientResponse.MethodName = "postCreateGoodIn";
                clientResponse.Errors = new List<clientGetirResponseError>();
                if (item.ReceiptItems == null)
                {
                    clientResponse.Success = false;
                    clientGetirResponseError df = new clientGetirResponseError();
                    df.ErrorCode = EnumResponseCode.createGoodsInError;
                    df.ErrorMessage = string.Format("receiptItems is null : " + item.WareCode);
                    clientResponse.Errors.Add(df);
                    return Ok(clientResponse);
                }
                try
                {



                    switch (item.WareWarehouseReceiptTypeID)
                    {
                        case "100": item.WareWarehouseReceiptTypeID = "G51"; break;
                        case "200": item.WareWarehouseReceiptTypeID = "G52"; break;
                        case "300": item.WareWarehouseReceiptTypeID = "G55"; break;
                        case "301": item.WareWarehouseReceiptTypeID = "G54"; break;
                        case "400": item.WareWarehouseReceiptTypeID = "G53"; break;

                        case "201": item.WareWarehouseReceiptTypeID = "G58"; break;
                        case "202": item.WareWarehouseReceiptTypeID = "G60"; break;
                        case "203": item.WareWarehouseReceiptTypeID = "G59"; break;
                        case "204": item.WareWarehouseReceiptTypeID = "G61"; break;
                    }


                    item.CreateDate = DateTime.Now;
                    item.SyncStatusCode = EnumSyncStatus.SyncWaiting;
                    db.GetirGoodsInOrder.Add(item);
                    db.SaveChanges();
                    clientResponse.Success = true;
                    return Ok(clientResponse);
                }
                catch (Exception e)
                {
                    clientResponse.Success = false;
                    clientGetirResponseError df = new clientGetirResponseError();
                    df.ErrorCode = EnumResponseCode.createGoodsInError;
                    df.ErrorMessage = string.Format("order inserted error  : {0} - {1}", item.WareCode, e.InnerException == null ? e.Message : e.InnerException.Message);
                    clientResponse.Errors.Add(df);
                    return Ok(clientResponse);
                }
            }
        }

        /// <summary>
        /// Giriş siparişi siler
        /// </summary>
        /// <param name="sorder"></param>
        /// <returns></returns>
        [Route("deleteGetirGoodsIn")]
        [ResponseType(typeof(clientGetirResponse))]
        [HttpPost]
        public IHttpActionResult DeleteGetirGoodsInOrder(GetirGoodsInOrder sorder)
        {
            //INSERT LOG
            _Log = _Log.InsertWebApiLog(Request, "GT", sorder, HttpContext.Current);

            clientGetirResponse clientResponse = new clientGetirResponse();
            clientResponse.MethodName = "deleteGetirGoodsIn";
            clientResponse.Errors = new List<clientGetirResponseError>();
            try
            {
                if (string.IsNullOrEmpty(sorder.WareDepositorReceiptNo))
                {
                    clientResponse.Success = false;
                    clientGetirResponseError df = new clientGetirResponseError();
                    df.ErrorCode = EnumResponseCode.deleteGetirGoodsInError;
                    df.ErrorMessage = "document id is empty";
                    clientResponse.Errors.Add(df);
                }
                else
                {
                    using (ESubeContext db = new ESubeContext())
                    {



                        switch (sorder.WareWarehouseReceiptTypeID)
                        {
                            case "100": sorder.WareWarehouseReceiptTypeID = "G51"; break;
                            case "200": sorder.WareWarehouseReceiptTypeID = "G52"; break;
                            case "300": sorder.WareWarehouseReceiptTypeID = "G55"; break;
                            case "400": sorder.WareWarehouseReceiptTypeID = "G53"; break;

                            case "201": sorder.WareWarehouseReceiptTypeID = "G58"; break;
                            case "202": sorder.WareWarehouseReceiptTypeID = "G60"; break;
                            case "203": sorder.WareWarehouseReceiptTypeID = "G59"; break;
                            case "204": sorder.WareWarehouseReceiptTypeID = "G61"; break;
                        }



                        sorder.CreateDate = DateTime.Now;
                        sorder.SyncDesc = "delete";
                        db.GetirGoodsInOrder.Add(sorder);
                        db.SaveChanges();
                        clientResponse.Success = true;
                    }
                }
                return Ok(clientResponse);
            }
            catch (Exception e)
            {
                clientResponse.Success = false;
                clientGetirResponseError df = new clientGetirResponseError();
                df.ErrorCode = EnumResponseCode.deleteGetirGoodsInError;
                df.ErrorMessage = string.Format("order not deleted  : {0}", e.InnerException == null ? e.Message : e.InnerException.Message);
                clientResponse.Errors.Add(df);
                return Ok(clientResponse);
            }

        }
        /// <summary>
        /// Çıkış siparişi siler
        /// </summary>
        /// <param name="sorder"></param>
        /// <returns></returns>
        [Route("deleteGetirGoodsOut")]
        [ResponseType(typeof(clientGetirResponse))]
        [HttpPost]
        public IHttpActionResult DeleteGetirGoodsOutOrder(GetirGoodsOutOrder sorder)
        {
            //INSERT LOG
            _Log = _Log.InsertWebApiLog(Request, "GT", sorder, HttpContext.Current);

            clientGetirResponse clientResponse = new clientGetirResponse();
            clientResponse.MethodName = "deleteGetirGoodsOut";
            clientResponse.Errors = new List<clientGetirResponseError>();
            try
            {
                if (string.IsNullOrEmpty(sorder.WaorDepositorOrderNo))
                {
                    clientResponse.Success = false;
                    clientGetirResponseError df = new clientGetirResponseError();
                    df.ErrorCode = EnumResponseCode.deleteGetirGoodsOutError;
                    df.ErrorMessage = "documentID is empty";
                    clientResponse.Errors.Add(df);
                }
                else
                {
                    using (ESubeContext db = new ESubeContext())
                    {


                        switch (sorder.WaorWarehouseOrderTypeID)
                        {
                            case "100": sorder.WaorWarehouseOrderTypeID = "G41"; break;
                            case "200": sorder.WaorWarehouseOrderTypeID = "G42"; break;
                            case "300": sorder.WaorWarehouseOrderTypeID = "G43"; break;
                            case "400": sorder.WaorWarehouseOrderTypeID = "G44"; break;
                            case "500": sorder.WaorWarehouseOrderTypeID = "G45"; break;
                            case "600": sorder.WaorWarehouseOrderTypeID = "G46"; break;
                            case "101": sorder.WaorWarehouseOrderTypeID = "G47"; break;
                            case "102": sorder.WaorWarehouseOrderTypeID = "G48"; break;
                            case "103": sorder.WaorWarehouseOrderTypeID = "G49"; break;
                            case "104": sorder.WaorWarehouseOrderTypeID = "G40"; break;
                            case "105": sorder.WaorWarehouseOrderTypeID = "G39"; break;
                        }



                        sorder.CreateDate = DateTime.Now;
                        sorder.SyncStatusCode = EnumSyncStatus.SyncWaiting;
                        sorder.SyncDesc = "delete";

                        db.GetirGoodsOutOrder.Add(sorder);
                        db.SaveChanges();
                        clientResponse.Success = true;
                    }
                }
                return Ok(clientResponse);
            }
            catch (Exception e)
            {
                clientResponse.Success = false;
                clientGetirResponseError df = new clientGetirResponseError();
                df.ErrorCode = EnumResponseCode.deleteGetirGoodsOutError;
                df.ErrorMessage = string.Format("order not deleted  : {0}", e.InnerException == null ? e.Message : e.InnerException.Message);
                clientResponse.Errors.Add(df);
                return Ok(clientResponse);
            }
        }

        protected override void Dispose(bool disposing)
        {
            //if (disposing)
            //{
            //    db.Dispose();
            //}
            base.Dispose(disposing);
        }
    }
}