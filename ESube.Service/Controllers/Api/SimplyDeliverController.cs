﻿using ESube.Service.Context;
using ESube.Service.Filters;
using ESube.Service.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;
using ESube.Service.Models.Helpers;
using ESube.Service.Models.WebApi;
using Newtonsoft.Json;
using System.IO;
using System.Text;
using System.Runtime.Serialization.Json;
using ESube.Service.Models.Enums;
using System.Web;
using System.Threading.Tasks;

namespace ESube.Service.Controllers
{

    /// <summary>
    /// Simply Deliver Entegrasyon Apileri
    /// </summary>
    public class SimplyDeliverController : ApiController
    {
        ApiSession _ApiSession = new ApiSession();
        ApiResponse _ApiResponse = new ApiResponse();
        Token _Token = new Token();
        Log _Log = new Log();

        Tender _Tender = new Tender();
        TenderAnswer _TenderAnswer = new TenderAnswer();
        TenderStop _TenderStop = new TenderStop();

        string ExceptionMessage = "";
        string InnerExceptionMessage = "";



        /// <summary>
        ///TR : Taşıma talebi ve uğrama noktaları tanımlar##### 
        ///EN : Creates tender and stops
        /// </summary>
        /// <param name="pTender"></param>
        ///  /// <returns></returns>
        ///   [HttpPost]
        [Route("api/simply/tenderAndStops")]
        [ResponseType(typeof(ApiResponse))]
        [HttpPost]
        public async Task<IHttpActionResult> TenderAndStopsCreate(Tender pTender)
        {
            string CompanyCode = "";
            try
            {
                //TOKEN KONTROLÜ ( Doğru mu, Yanlış Mı)
                ReturnObject _RetObj = _Token.ValidateWebApiToken(Request);
                if (!_RetObj.Result)
                {
                    _ApiResponse.StatusCode = EnumApiHttpStatus.Unauthorized;
                    _ApiResponse.Message = _RetObj.ResultMessage;
                }
                else
                {
                    //TOKEN ÜZERİNDEN KULLANICI BİLGİLERİNİ BUL VE APİ SESSİON A YAZ
                    _ApiSession = _ApiSession.GetByToken(Request);
                    CompanyCode = _ApiSession.MainCompany.Code;

                    //INSERT LOG
                    _Log = _Log.InsertWebApiLog(Request, CompanyCode, pTender, HttpContext.Current);

                    //Validation
                    if (!ModelState.IsValid)
                    {
                        var messages = string.Join(" | ", ModelState.Values
                                            .SelectMany(v => v.Errors)
                                            .Select(e => e.ErrorMessage));

                        _ApiResponse.StatusCode = EnumApiHttpStatus.BadRequest;
                        _ApiResponse.Message = messages;
                    }
                    else
                    {
                        //if (pTender.stops != null)
                        //{
                        //    //TenderCode bilgisini bağlı olduğu classlara aktar
                        //    _Tender.CreateTenderRelation(pTender);
                        //}

                        //TenderCode ile gelen önceki kayıtları henüz aktarılmamış ise iptal e çek
                        _Tender.UpdateSyncStatus(0, pTender.Code, EnumSyncStatus.SyncWaiting, EnumSyncStatus.SyncCancelled);
                        _TenderAnswer.UpdateSyncStatus(0,pTender.Code, EnumSyncStatus.SyncWaiting, EnumSyncStatus.SyncCancelled);
                        _TenderStop.UpdateSyncStatus(pTender.Code, EnumSyncStatus.SyncWaiting, EnumSyncStatus.SyncCancelled);


                        //                //Yeniyi kaydet
                        //                _TenderAnswer.InsertTenderAnswer(pAnswer);


                        //Yeniyi kaydet
                        _Tender.InsertTender(pTender);

                        _ApiResponse.StatusCode = EnumApiHttpStatus.OK;
                        _ApiResponse.Message = _ApiResponse.Status;
                    }

                }

            }
            catch (Exception e)
            {
                _ApiResponse.StatusCode = EnumApiHttpStatus.MethodFailure;
                _ApiResponse.Message = "Error Code : " + _Log.Id.ToString();

                _Log.StatusCode = EnumLogStatus.Error;
                _Log.StatusMessage = _ApiResponse.Message;
                _Log.ErrorMessage = (e.InnerException != null)? e.InnerException.InnerException.Message: "";
                _Log.ErrorMessage = string.IsNullOrEmpty(_Log.ErrorMessage) ? e.Message : _Log.ErrorMessage;

            }

            //LOG Update   
            if (_Log != null)
            {
                _Log.ResponseBody = JsonConvert.SerializeObject(_ApiResponse).ToString();
                _Log.UpdateWebApiLog(_Log);
            }

            return Ok(_ApiResponse);
        }


        /// <summary>
        ///Taşıma talebine gelen cevabı tanımlar
        /// </summary>
        //[Route("api/simply/tenderAnswer")]
        //[ResponseType(typeof(ApiResponse))]
        //[HttpPost]
        //public async Task<IHttpActionResult> TenderAnswerCreate(TenderAnswer pAnswer)
        //{
        //    string CompanyCode = "";
        //    try
        //    {
        //        //TOKEN KONTROLÜ ( Doğru mu, Yanlış Mı)
        //        ReturnObject _RetObj = _Token.ValidateWebApiToken(Request);
        //        if (!_RetObj.Result)
        //        {
        //            _ApiResponse.StatusCode = EnumApiHttpStatus.Unauthorized;
        //            _ApiResponse.Message = _RetObj.ResultMessage;
        //        }
        //        else
        //        {
        //            //TOKEN ÜZERİNDEN KULLANICI BİLGİLERİNİ BUL VE APİ SESSİON A YAZ
        //            _ApiSession = _ApiSession.GetByToken(Request);
        //            CompanyCode = _ApiSession.Company.Code;

        //            //INSERT LOG
        //            _Log = _Log.InsertWebApiLog(Request, CompanyCode, pAnswer, HttpContext.Current);

        //            //Validation
        //            if (!ModelState.IsValid)
        //            {
        //                var messages = string.Join(" | ", ModelState.Values
        //                                    .SelectMany(v => v.Errors)
        //                                    .Select(e => e.ErrorMessage));

        //                _ApiResponse.StatusCode = EnumApiHttpStatus.BadRequest;
        //                _ApiResponse.Message = messages;
        //            }
        //            else
        //            {
        //                //Öncekileri iptal et
        //                _TenderAnswer.UpdateSyncStatus(pAnswer.TenderCode, EnumSyncStatus.SyncCancelled);

        //                //Yeniyi kaydet
        //                _TenderAnswer.InsertTenderAnswer(pAnswer);

        //                _ApiResponse.StatusCode = EnumApiHttpStatus.OK;
        //                _ApiResponse.Message = _ApiResponse.Status;
        //            }

        //        }

        //    }
        //    catch (Exception e)
        //    {
        //        _ApiResponse.StatusCode = EnumApiHttpStatus.MethodFailure;
        //        _ApiResponse.Message = "Error Code : " + _Log.Id.ToString();

        //        _Log.StatusCode = EnumLogStatus.Error;
        //        _Log.StatusMessage = _ApiResponse.Message;
        //    }

        //    //LOG Update   
        //    if (_Log != null)
        //    {
        //        _Log.ResponseBody = JsonConvert.SerializeObject(_ApiResponse).ToString();
        //        _Log.UpdateWebApiLog(_Log);
        //    }

        //    return Ok(_ApiResponse);
        //}

        /// <summary>
        /// Taşıma taleplerini verir
        /// </summary>
        [Route("api/simply/tender")]
        [ResponseType(typeof(ApiResponse))]
        [HttpGet]
        public async Task<IHttpActionResult> TenderGetList(DateTime pMinDate, DateTime pMaxDate)
        {
            string CompanyCode = "";
            try
            {
                //TOKEN KONTROLÜ ( Doğru mu, Yanlış Mı)
                ReturnObject _RetObj = _Token.ValidateWebApiToken(Request);
                if (!_RetObj.Result)
                {
                    _ApiResponse.StatusCode = EnumApiHttpStatus.Unauthorized;
                    _ApiResponse.Message = _RetObj.ResultMessage;
                }
                else
                {
                    //TOKEN ÜZERİNDEN KULLANICI BİLGİLERİNİ BUL VE APİ SESSİON A YAZ
                    _ApiSession = _ApiSession.GetByToken(Request);
                    CompanyCode = _ApiSession.MainCompany.Code;

                    //INSERT LOG
                    _Log = _Log.InsertWebApiLog(Request, CompanyCode, "MinDate : " + pMinDate.ToString() + " | MaxDate : " + pMaxDate.ToString(), HttpContext.Current);

                    //VALIDATION
                    if (!ModelState.IsValid)
                    {
                        var messages = string.Join(" | ", ModelState.Values
                                            .SelectMany(v => v.Errors)
                                            .Select(e => e.ErrorMessage));

                        _ApiResponse.StatusCode = EnumApiHttpStatus.BadRequest;
                        _ApiResponse.Message = messages;
                    }
                    else
                    {

                        _ApiResponse.StatusCode = EnumApiHttpStatus.OK;
                        _ApiResponse.Message = _ApiResponse.Status;
                        _ApiResponse.Data = _Tender.GetListTenderAndDetails();


                        _ApiResponse.StatusCode = EnumApiHttpStatus.OK;
                        _ApiResponse.Message = _ApiResponse.Status;
                    }

                }

            }
            catch (Exception e)
            {
                _ApiResponse.StatusCode = EnumApiHttpStatus.MethodFailure;
                _ApiResponse.Message = "Error Code : " + _Log.Id.ToString();

                _Log.StatusCode = EnumLogStatus.Error;
                _Log.StatusMessage = _ApiResponse.Message;
            }

            //LOG Update   
            if (_Log != null)
            {
                _Log.ResponseBody = JsonConvert.SerializeObject(_ApiResponse).ToString();
                _Log.UpdateWebApiLog(_Log);
            }

            return Ok(_ApiResponse);
        }


    }
}
