﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using ESube.Service.Context;
using ESube.Service.Models;
using ESube.Service.Models.Enums;
using ESube.Service.Models.Helpers;
using ESube.Service.Models.WebApi;
using Newtonsoft.Json;

namespace ESube.Service.Controllers
{
    /// <summary>
    /// Ürün tanımlama methodları
    /// </summary>
    public class ItemController : ApiController
    {
        ApiSession _ApiSession = new ApiSession();
        ApiResponse _ApiResponse = new ApiResponse();
        Token _Token = new Token();
        Log _Log = new Log();

        Item _Item = new Item();



        // POST: api/item
        /// <summary>
        /// Ürün tanımlama
        /// </summary>
        /// <param name="pItem"></param>
        /// <returns></returns>
        //[ResponseType(typeof(Item))]
        [ResponseType(typeof(ApiResponse))]
        [Route("api/item")]
        [HttpPost]
        public async Task<IHttpActionResult> InsertItem(Item pItem)
        {
            string CompanyCode = "";
            try
            {
                //TOKEN KONTROLÜ ( Doğru mu, Yanlış Mı)
                ReturnObject _RetObj = _Token.ValidateWebApiToken(Request);
                if (!_RetObj.Result)
                {
                    _ApiResponse.StatusCode = EnumApiHttpStatus.Unauthorized;
                    _ApiResponse.Message = _RetObj.ResultMessage;
                }
                else
                {
                    //TOKEN ÜZERİNDEN KULLANICI BİLGİLERİNİ BUL VE APİ SESSİON A YAZ
                    _ApiSession = _ApiSession.GetByToken(Request);
                    CompanyCode = _ApiSession.MainCompany.Code;

                    //INSERT LOG
                    _Log = _Log.InsertWebApiLog(Request, CompanyCode, pItem, HttpContext.Current);

                    //MODEL VALIDATION
                    if (!ModelState.IsValid)
                    {
                        var messages = string.Join(" | ", ModelState.Values
                                            .SelectMany(v => v.Errors)
                                            .Select(e => e.ErrorMessage));

                        _ApiResponse.StatusCode = EnumApiHttpStatus.BadRequest;
                        _ApiResponse.Message = messages;
                    }
                    else
                    {
                        //Gelen Item Koduna Ait Henüz Transfer Edilmemiş data var ise Transferlerini İptal Edilmiş Olarak güncelle !
                        _Item.UpdateSyncStatus(pItem.ItemCode, EnumSyncStatus.SyncCancelled);

                        pItem.MainCompanyCode = _ApiSession.MainCompany.Code;
                        _Item.InsertItem(pItem);

                        _ApiResponse.StatusCode = EnumApiHttpStatus.OK;
                        _ApiResponse.Message = _ApiResponse.Status;
                    }

                }

            }
            catch (Exception e)
            {
                _ApiResponse.StatusCode = EnumApiHttpStatus.MethodFailure;
                _ApiResponse.Message = "Error Code : " + _Log.Id.ToString();

                _Log.StatusCode = EnumLogStatus.Error;
                _Log.StatusMessage = _ApiResponse.Message;
            }

            //LOG Update   
            if (_Log != null)
            {
                _Log.ResponseBody = JsonConvert.SerializeObject(_ApiResponse).ToString();
                _Log.UpdateWebApiLog(_Log);
            }

            return Ok(_ApiResponse);
        }



        // GET: api/item?pItemCode=X
        /// <summary>
        /// Ürün koduna ait ürün bilgilerini verir. Ürün kodu boş gönderilir ise tüm ürünleri verir.
        /// </summary>
        /// <param name="pItemCode"></param>
        /// <returns></returns>
        [Route("api/item")]
        [ResponseType(typeof(ApiResponse))]
        [HttpGet]
        public async Task<IHttpActionResult> GetListItem(string pItemCode)
        {
            string CompanyCode = "";
            try
            {
                //TOKEN KONTROLÜ ( Doğru mu, Yanlış Mı)
                ReturnObject _RetObj = _Token.ValidateWebApiToken(Request);
                if (!_RetObj.Result)
                {
                    _ApiResponse.StatusCode = EnumApiHttpStatus.Unauthorized;
                    _ApiResponse.Message = _RetObj.ResultMessage;
                }
                else
                {
                    //TOKEN ÜZERİNDEN KULLANICI BİLGİLERİNİ BUL VE APİ SESSİON A YAZ
                    _ApiSession = _ApiSession.GetByToken(Request);
                    CompanyCode = _ApiSession.MainCompany.Code;

                    //INSERT LOG
                    _Log = _Log.InsertWebApiLog(Request, CompanyCode, "pItemCode : "+pItemCode, HttpContext.Current);

                    //MODEL VALIDATION
                    if (!ModelState.IsValid)
                    {
                        var messages = string.Join(" | ", ModelState.Values
                                            .SelectMany(v => v.Errors)
                                            .Select(e => e.ErrorMessage));

                        _ApiResponse.StatusCode = EnumApiHttpStatus.BadRequest;
                        _ApiResponse.Message = messages;
                    }
                    else
                    {

                        _ApiResponse.StatusCode = EnumApiHttpStatus.OK;
                        _ApiResponse.Message = _ApiResponse.Status;
                        _ApiResponse.Data = _Item.GetList(pItemCode);


                        _ApiResponse.StatusCode = EnumApiHttpStatus.OK;
                        _ApiResponse.Message = _ApiResponse.Status;
                    }

                }

            }
            catch (Exception e)
            {
                _ApiResponse.StatusCode = EnumApiHttpStatus.MethodFailure;
                _ApiResponse.Message = "Error Code : " + _Log.Id.ToString();

                _Log.StatusCode = EnumLogStatus.Error;
                _Log.StatusMessage = _ApiResponse.Message;
            }

            //LOG Update   
            if (_Log != null)
            {
                _Log.ResponseBody = JsonConvert.SerializeObject(_ApiResponse).ToString();
                _Log.UpdateWebApiLog(_Log);
            }

            return Ok(_ApiResponse);
        }





    }
}