﻿using ESube.Service.Models;
using ESube.Service.Models.Enums;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using ESube.Service.Helpers;
using ESube.Service.Models.Helpers;

namespace ESube.Service.Controllers
{
    public class ContentController : Controller
    {
        // GET: Item
        public Report _Report= new Report();

        public ActionResult Report()
        {
            return View();
        }

        [HttpPost]
        public string GetListReport(string pMainCompanyCode,string pName)
        {

            var list = _Report.GetList(pMainCompanyCode,pName,null);

            //Ignore Propertylerin Ignore larını temizlemek için
            JsonSerializerSettings settingsXX = new JsonSerializerSettings();
            settingsXX.ContractResolver = new IgnoreJsonAttributesResolver();

            var json = JsonConvert.SerializeObject(list, settingsXX);

            return json;
        }

        [HttpPost]
        public string InsertOrUpdateReport(Report pReport)
        {
            ReturnObject ret = new ReturnObject();

            ret = _Report.InsertOrUpdate(pReport);

            var json = JsonConvert.SerializeObject(ret);

            return json;
        }
    }
}