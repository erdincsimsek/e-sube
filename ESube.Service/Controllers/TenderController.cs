﻿using ESube.Service.Models;
using ESube.Service.Models.Enums;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using ESube.Service.Helpers;

namespace ESube.Service.Controllers
{
    public class TenderController : Controller
    {
        public Tender _Tender = new Tender();

        # region HEMEL

        #endregion
        public ActionResult Index()
        {
            return View();
        }


        [HttpPost]
        public string GetListTender(string pCode, EnumSyncStatus pStatusCode)
        {


            var list = _Tender.GetListTender(pCode, pStatusCode);

            //Ignore Propertylerin Ignore larını temizlemek için
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.ContractResolver = new IgnoreJsonAttributesResolver();


            var json = JsonConvert.SerializeObject(list, settings);

            return json;
        }

        [HttpPost]
        public string GetTender(int pId, string pCode)
        {
            var list = _Tender.GetTender(pId, pCode);

            //Ignore Propertylerin Ignore larını temizlemek için
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.ContractResolver = new IgnoreJsonAttributesResolver();
            settings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;

            var json = JsonConvert.SerializeObject(list, settings);

            return json;
        }

        [HttpPost]
        public string UpdateSyncStatus(int pId, string pCode)
        {
            // 200 Ü 100 E ÇEK
            _Tender.UpdateSyncStatus(pId,pCode,EnumSyncStatus.SyncSuccess,EnumSyncStatus.SyncWaiting);

            return "";
        }

        //[HttpPost]
        //public string GetListGoodsOut(string pOrderCode, EnumSyncStatus pOrderStatus)
        //{
        //    var list = cGetirGoodsOutOrder.GetListAll(pOrderCode, pOrderStatus);

        //    //Ignore Propertylerin Ignore larını temizlemek için
        //    JsonSerializerSettings settings = new JsonSerializerSettings();
        //    settings.ContractResolver = new IgnoreJsonAttributesResolver();

        //    var json = JsonConvert.SerializeObject(list, settings);

        //    return json;
        //}
        //public string GetListGoodsOutDetail(string pOrderCode)
        //{
        //    var list = cGetirGoodsOutOrderDetail.GetListByOrderCode(pOrderCode);

        //    var json = JsonConvert.SerializeObject(list);
        //    return json;
        //}


    }
}