﻿using ESube.Service.Context;
using ESube.Service.Helpers;
using ESube.Service.Models;
using ESube.Service.Models.Helpers;
using ESube.Service.Models.WebApi;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Http.Description;
using System.Web.Mvc;
using System.Xml;


namespace ESube.Service.Controllers
{
    public class OperationController : Controller
    {
        OrderNew _cOrder = new OrderNew();
         Log _Log = new Log();
        public ActionResult TransferOrder()
        {
            return View();
        }


        [HttpPost]
        public string GetIntOrder(string pMainCompanyCode, string pOrderCode)
        {
            OrderNew _Order = _cOrder.GetIntOrder(pMainCompanyCode, pOrderCode);


            //Ignore Propertylerin Ignore larını temizlemek için
            JsonSerializerSettings settingsXX = new JsonSerializerSettings();
            settingsXX.ContractResolver = new IgnoreJsonAttributesResolver();

            var json = JsonConvert.SerializeObject(_Order, settingsXX);

            return json;
        }


        public ActionResult UploadDocument()
        {
            return View();
        }



        
        [HttpPost]
        public ActionResult UploadDocument(HttpPostedFileBase excelFile)
        {
            //Dosya kontrolü
            if (excelFile == null
            || excelFile.ContentLength == 0)
            {
                ViewBag.Error = "Lütfen dosya seçimi yapınız.";

                return View();
            }
            else
            {
                //Dosyanın uzantısı xls ya da xlsx ise;
                if (excelFile.FileName.EndsWith("xls")
                || excelFile.FileName.EndsWith("xlsx"))
                {

                    //Seçilen dosyanın nereye kaydedileceği belirtiliyor.
                    string path = Server.MapPath("~/" + excelFile.FileName);

                    //Dosya kontrol edilir, varsa silinir.
                    if (System.IO.File.Exists(path))
                    {
                        System.IO.File.Delete(path);
                    }

                    //Excel path altına kaydedilir.
                    excelFile.SaveAs(path);
                    try
                    {
                     
                        Microsoft.Office.Interop.Excel.Application application;
                        Microsoft.Office.Interop.Excel.Workbook workbook;
                        Microsoft.Office.Interop.Excel.Worksheet worksheet;
                        Microsoft.Office.Interop.Excel.Range range;

                      
                            //+Exceli açıyoruz
                            application = new Microsoft.Office.Interop.Excel.Application();
                            workbook = application.Workbooks.Open(path);
                            worksheet = workbook.ActiveSheet;
                            range = worksheet.UsedRange;
                     
                        
                        //-

                        //Verileri listeye atıp listele viewında göstereceğim, o yüzden modelimin
                        //tipinde liste değişkeni tanımlıyorum.
                        //List<ListModel> localList = new List<ListModel>();

                        //tüm verilerde dönüp ilgili veriyi ilgili modele atıyorum. sonrasında modeli
                        //listeye atıyorum.
                        for (int i = 2; i <= range.Rows.Count; i++)
                        {
                            //ilk önce verileri modele ekleyeceğim bunun için
                            //model değişkenimi tanımlıyorum.
                            //Her seferinde yeni kayıtlarımı atacağım için for içinde tanımılıyorum.
                            //for dışında tanımlarsanız daima son aldığı değerleri listede gösterir.
                            //ListModel lm = new ListModel();

                            //lm.Id = Convert.ToInt32(((Microsoft.Office.Interop.Excel.Range)range.Cells[i, 1]).Text);
                            //lm.Name = ((Microsoft.Office.Interop.Excel.Range)range.Cells[i, 2]).Text;
                            //lm.SurName = ((Microsoft.Office.Interop.Excel.Range)range.Cells[i, 3]).Text;
                            //lm.Email = ((Microsoft.Office.Interop.Excel.Range)range.Cells[i, 4]).Text;

                            string OrderCode = (((Microsoft.Office.Interop.Excel.Range)range.Cells[i, 8]).Text);
                            string ContainerType = ((Microsoft.Office.Interop.Excel.Range)range.Cells[i, 5]).Text;


                            using (ESubeContext db = new ESubeContext())
                            {
                                List<SqlParameter> paramlist = new List<SqlParameter>();
                                paramlist.Add(new SqlParameter("OrderCode", OrderCode));
                                paramlist.Add(new SqlParameter("ContainerType", ContainerType));

                                //entities.Database.ExecuteSqlCommand("Sp_User_Update @Name", paramlist.ToString());

                                db.Database.ExecuteSqlCommand("Sp_Transporter_Info_Excel_Import @OrderCode,@ContainerType", paramlist.ToArray());


                            }

                        }

                        application.Quit();

                        if (System.IO.File.Exists(path))
                        {
                            System.IO.File.Delete(path);
                        }
                        //listeyi bu sayfaya taşımak için bu viewBag içine alıyorum.
                        //ViewBag.ListDetay = localList;

                        //listele viewına döndürüyorum.

                    }
                    catch (Exception e)
                    {



                        _Log.LogErrorMessage((e.InnerException == null) ? e.Message.ToString() : e.InnerException.Message.ToString(),
                            Convert.ToInt32(e.StackTrace.Substring(e.StackTrace.LastIndexOf(' '))));

                    }

                }

                else
                {
                    ViewBag.Error = "Dosya tipiniz yanlış, lütfen '.xls' yada '.xlsx' uzantılı dosya yükleyiniz.";

                    return View();
                }
            }

            return View();
        }



    }
}