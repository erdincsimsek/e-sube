﻿using ESube.Service.Models;
using ESube.Service.Models.Enums;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using ESube.Service.Helpers;
using ESube.Service.Context;

namespace ESube.Service.Controllers
{
    public class SettingController : Controller
    {

        public ESubeContext _db = new ESubeContext();

        public ActionResult Index()
        {
            return View();
        }


        [HttpPost]
        public string ChangeSessionGlobalMainCompany(string pMainCompanyCode)
        {
            MainCompany _MainCompany = new MainCompany();
            bool Result = true;
            try
            {

                if (!string.IsNullOrEmpty(pMainCompanyCode))
                {
                    _MainCompany = _db.MainCompany.Where(x => x.Code == pMainCompanyCode).FirstOrDefault();
                    Session["MainCompany"] = _MainCompany;
                    Session["IsMainCompanySelected"] = true;
                }
                else
                {
                    _MainCompany = new MainCompany();
                    Session["MainCompany"] = _MainCompany;
                    Session["IsMainCompanySelected"] = true;
                }

            }
            catch (Exception x)
            {
                Result = false;
            }
         

            return Result.ToString();
        }


    }
}