﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ESube.Service.Context;
using ESube.Service.Models;
using ESube.Service.Filters;
 
namespace ESube.Service.Controllers
{
    [APIAuthorizeAttribute]
    public class IntegrationController : ApiController
    {
        private ESubeContext db = new ESubeContext();

        // GET: api/Integration
        public IQueryable<Order> GetIntegration()
        {
            return db.Order;
        }

        // GET: api/Integration/5
        [ResponseType(typeof(Order))]
        public async Task<IHttpActionResult> GetIntegration(long id)
        {
            Order integration = await db.Order.FindAsync(id);
            if (integration == null)
            {
                return NotFound();
            }

            return Ok(integration);
        }

        // PUT: api/Integration/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutIntegration(long id, Order integration)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != integration.id)
            {
                return BadRequest();
            }

            db.Entry(integration).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!IntegrationExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Integration
        [HttpPost]
        [ResponseType(typeof(Order))]
        public async Task<IHttpActionResult> PostIntegration(Order integration)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Order.Add(integration);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = integration.id }, integration);
        }

        // DELETE: api/Integration/5
        [ResponseType(typeof(Order))]
        public async Task<IHttpActionResult> DeleteIntegration(long id)
        {
            Order integration = await db.Order.FindAsync(id);
            if (integration == null)
            {
                return NotFound();
            }

            db.Order.Remove(integration);
            await db.SaveChangesAsync();

            return Ok(integration);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool IntegrationExists(long id)
        {
            return db.Order.Count(e => e.id == id) > 0;
        }
    }
     
}