﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ESube.Service.Helpers
{
    public class ValidateObject
    {

        public ValidateObject()
        {
            Result = false;
            ResultMessage = "";
        }

        public bool Result { get; set; }
        public string ResultMessage  { get; set;}
    }
}