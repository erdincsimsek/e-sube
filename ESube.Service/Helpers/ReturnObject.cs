﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ESube.Service.Models.Helpers
{
    public class ReturnObject
    {
        public ReturnObject()
        {
            Result = false;
            ResultMessage = "";
        }

        public bool Result { get; set; }
        public string ResultMessage { get; set; }
        public long Id { get; set; }
        public dynamic Data { get; set; }
    }
}