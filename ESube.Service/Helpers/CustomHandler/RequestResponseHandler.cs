﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using ESube.Service.Models;

namespace ESube.Service
{
    public class RequestResponseHandler : DelegatingHandler
    {
        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request,
            CancellationToken cancellationToken)
        {

            var requestedMethod = request.Method;
            var userHostAddress = HttpContext.Current != null ? HttpContext.Current.Request.UserHostAddress : "0.0.0.0";
            var useragent = request.Headers.UserAgent.ToString();
            var requestMessage = await request.Content.ReadAsByteArrayAsync();
            var uriAccessed = request.RequestUri.AbsoluteUri;

            var responseHeadersString = new StringBuilder();
            foreach (var header in request.Headers)
            {
                responseHeadersString.Append($"{header.Key}: {String.Join(", ", header.Value)}{Environment.NewLine}");
            }

            var messageLoggingHandler = new MessageLogging();

            var requestLog = new Log_()
            {
                headers = responseHeadersString.ToString(),
                absoluteUri = uriAccessed,
                host = userHostAddress,
                requestBody = Encoding.UTF8.GetString(requestMessage),
                userHostAddress = userHostAddress,
                userAgent = useragent,
                requestedMethod = requestedMethod.ToString(),
                statusCode = string.Empty,
                UtcTime = DateTime.Now

            };

            messageLoggingHandler.IncomingMessageAsync(requestLog);
            System.Threading.Thread.Sleep(100);
            var response = await base.SendAsync(request, cancellationToken);
            System.Threading.Thread.Sleep(100);

            //geçici olark kapatıldı
            //if (response != null)
            //{
            //    byte[] responseMessage;
            //    if (response.IsSuccessStatusCode)
            //        responseMessage = await response.Content.ReadAsByteArrayAsync();
            //    else
            //        responseMessage = Encoding.UTF8.GetBytes(response.ReasonPhrase);

            //    var responseLog = new Log()
            //    {
            //        headers = responseHeadersString.ToString(),
            //        absoluteUri = uriAccessed,
            //        host = userHostAddress,
            //        requestBody = Encoding.UTF8.GetString(responseMessage),
            //        userHostAddress = userHostAddress,
            //        userAgent = useragent,
            //        requestedMethod = requestedMethod.ToString(),
            //        statusCode = string.Empty,
            //        UtcTime = DateTime.Now
            //    };

            //    messageLoggingHandler.OutgoingMessageAsync(responseLog);
            //}
            return response;
        }
    }

    public class MessageLogging
    {
        public void IncomingMessageAsync(Log_ apiLog)
        {
            apiLog.requestType = "Request";
          //  var nLog = new ApiLogging();
            Logging.InsertLog(apiLog);
        }

        public void OutgoingMessageAsync(Log_ log)
        {
            log.requestType = "Response";
            //var sqlErrorLogging = new ApiLogging();
            Logging.InsertLog(log);
        }
    }

}