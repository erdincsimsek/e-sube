﻿using System;
using System.Web.Http.ExceptionHandling;
using ESube.Service.Models;
using System.Web;


namespace ESube.Service.CustomHandler
{
    public class UnhandledExceptionLogger : ExceptionLogger
    {
        public override void Log(ExceptionLoggerContext context)
        {
            var ex = context.Exception;

            string strLogText = "";
            strLogText += Environment.NewLine + "Source ---\n{0}" + ex.Source;
            strLogText += Environment.NewLine + "StackTrace ---\n{0}" + ex.StackTrace;
            strLogText += Environment.NewLine + "TargetSite ---\n{0}" + ex.TargetSite;

            if (ex.InnerException != null)
            {
                strLogText += Environment.NewLine + "Inner Exception is {0}" + ex.InnerException;//error prone
            }
            if (ex.HelpLink != null)
            {
                strLogText += Environment.NewLine + "HelpLink ---\n{0}" + ex.HelpLink;//error prone
            }
            var userHostAddress = HttpContext.Current != null ? HttpContext.Current.Request.UserHostAddress : "0.0.0.0";
            var requestedURi = (string)context.Request.RequestUri.AbsoluteUri;
            var requestMethod = context.Request.Method.ToString();
            var timeUtc = DateTime.Now;

            // SqlErrorLogging sqlErrorLogging = new SqlErrorLogging();
            Log_ log = new Log_()
            {
                message = strLogText,
                requestBody = "",
                host = userHostAddress,
                absoluteUri = requestedURi,
                requestedMethod = requestMethod,
                UtcTime = DateTime.Now,
                logType = enumLogType.Error

            };
            Logging.InsertLog(log);
        }
    }
}