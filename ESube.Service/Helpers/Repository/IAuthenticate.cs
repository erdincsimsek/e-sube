﻿using ESube.Service.Models;
using System;

namespace ESube.Service.Repository
{
    public interface IAuthenticate
    {
        ClientKey GetClientKeysDetailsbyCLientIDandClientSecert(string clientID , string clientSecert);
        bool ValidateKeys(ClientKey ClientKeys);
        bool IsTokenAlreadyExists(int CompanyID);
        int DeleteGenerateToken(int CompanyID);
        int InsertToken(Token token);
        string GenerateToken(ClientKey ClientKeys, DateTime IssuedOn);
    }
}
