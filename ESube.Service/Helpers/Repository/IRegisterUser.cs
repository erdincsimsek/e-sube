﻿using ESube.Service.Models;

namespace ESube.Service.Repository
{
    public interface IRegister
    {
        void Add(User Register);
        bool ValidateRegisteredUser(User Register);
        bool ValidateUsername(User Register);
        int GetLoggedUserID(User Register);
    }
}
