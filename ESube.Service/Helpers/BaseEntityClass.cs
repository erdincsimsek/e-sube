﻿using ESube.Service.Models.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ESube.Service.Helpers
{
    public abstract class BaseEntityClass
    {
        public BaseEntityClass()
        {
            CreateId = 0;
            UpdateId = 0;
            CreateDate = DateTime.Now;
            UpdateDate = null;
            IsDeleted = false;
            IsActive = true;
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public int? CreateId { get; set; }
        public int? UpdateId { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime CreateDate { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? UpdateDate { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsActive { get; set; }
    }
}