﻿using ESube.Service.Models.Enums;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.Web.Mvc;

namespace ESube.Service.Models.Helpers
{
    public abstract class BaseIntegrationClass
    {
        public BaseIntegrationClass(){
            CreateDate = DateTime.Now;
            SyncStatusCode = EnumSyncStatus.SyncWaiting;
            SyncDesc = "Insert";
        }
        long _Id;
        [IgnoreDataMember]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id
        {
            get { return _Id; }
            set { _Id = value; }

        }
        [IgnoreDataMember]
        [DefaultValue("0")]
        public int? CreateId { get; set; }

        [IgnoreDataMember]
        [DefaultValue("0")]
        public int? UpdateId { get; set; }

        //[DefaultValue("false")]
        //public bool Deleted { get; set; }
        [IgnoreDataMember]
        [Column(TypeName = "datetime2")]
        public DateTime CreateDate{ get; set; }
        [IgnoreDataMember]
        [Column(TypeName = "datetime2")]
        public DateTime? UpdateDate { get; set; }
        //[IgnoreDataMember]
        public string FreeText { get; set; }
        [IgnoreDataMember]
        public EnumSyncStatus SyncStatusCode { get; set; }
        [IgnoreDataMember]
        public string SyncStatus { get { return (Enum.GetName(typeof(EnumSyncStatus), SyncStatusCode)); } set { } }
        [IgnoreDataMember]
        [DefaultValue("Insert")]
        public string SyncDesc { get; set; }

    }
}