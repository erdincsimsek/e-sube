﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ESube.Service.Models.Helpers;

namespace ESube.Service.Models.Helpers
{
    public class BaseController : Controller
    {
        protected User SessionUser
        {
            get { return (User)Session["User"]; }
            set { Session["User"] = value; }
        }
    }
}