﻿using ESube.Service.Models;
using ESube.Service.Models.Views;
using System.Data.Entity;
//using static ESube.Service.Models.DailyStock;

namespace ESube.Service.Context
{
    public class ESubeContext : DbContext
    {
        public ESubeContext() : base("esube")
        {
            this.Configuration.LazyLoadingEnabled = false;
            this.Configuration.ProxyCreationEnabled = false;
        }
        public DbSet<User> User { get; set; }
        public DbSet<MainCompany> MainCompany { get; set; }
        public DbSet<Company> Company { get; set; }
        public DbSet<Token> TokensManager { get; set; }
        public DbSet<ClientKey> ClientKeys { get; set; }
        //public DbSet<OrderOld> OrderOld { get; set; }
        //public DbSet<OrderLineOld> OrderLineOld { get; set; }
        public DbSet<OrderNew> OrderNew { get; set; }
        public DbSet<OrderLineNew> OrderLineNew { get; set; }
        //public DbSet<Log_> Log_ { get; set; }
        public DbSet<Log> Log { get; set; }
        public DbSet<Item> Item { get; set; }
        public DbSet<ItemBarcode> ItemBarcode { get; set; }
        public DbSet<ItemPackType> ItemPackType { get; set; }
        public DbSet<ItemEachCustomer> ItemEachCustomer { get; set; }

        //public DbSet<OrderStatusTransaction> OrderStatusTransaction { get; set; }
        //public DbSet<OrderStatus> OrderStatus { get; set; }
         public DbSet<DailyStokcVW> DailyStokcVW { get; set; }
        //public DbSet<OrderResponse> OrderResponse { get; set; }
        public DbSet<DailyStock> DailyStock { get; set; }


         //GETİR
        public DbSet<GetirCompany> GetirCompany { get; set; }
        public DbSet<GetirItem> GetirItem { get; set; }
        public DbSet<GetirItemBarcode> GetirItemBarcode { get; set; }
        public DbSet<GetirItemPackType> GetirItemPackType { get; set; }
        public DbSet<GetirGoodsOutOrder> GetirGoodsOutOrder { get; set; }
        public DbSet<GetirGoodsInOrder> GetirGoodsInOrder { get; set; }
        public DbSet<GetirGoodsOutOrderLine> GetirGoodsOutOrderLine { get; set; }
        public DbSet<GetirGoodsInOrderLine> GetirGoodsInOrderLine { get; set; }


        public DbSet<Tender> Tender { get; set; }
        public DbSet<TenderStop> TenderStop { get; set; }
        public DbSet<TenderAnswer> TenderAnswer { get; set; }
        public DbSet<Role> Role { get; set; }
        public DbSet<UserRole> UserRole { get; set; }

        public DbSet<Test> Test { get; set; }

        public DbSet<Report> Report{ get; set; }

        public DbSet<QuestionHeader> QuestionHeader { get; set; }
        public DbSet<Question> Question{ get; set; }
        public DbSet<QuestionAnswer> QuestionAnswer { get; set; }


        public DbSet<Document> Document { get; set; }
        public DbSet<Uetds_Yuk_Kaydi> Uetds_Yuk_Kaydi { get; set; }

        public DbSet<Uetds_Yuk_Kaydi_Detayi> Uetds_Yuk_Kaydi_Detayi { get; set; }
        public DbSet<Uetds_Log> Uetds_Log { get; set; }
        public DbSet<Uetds_Islem_Turu> Uetds_Islem_Turu { get; set; }

        public DbSet<Warehouse> Warehouse { get; set; }
        public DbSet<IntegrationEnumType> IntegrationEnumType { get; set; }
        //VİEW LAR
        //public DbSet<OrderWithWayBill> OrderWithWayBill { get; set; }
        //public DbSet<vwOrderReturn> vwOrderReturn { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            // modelBuilder.Ignore<DailyStokcVW>();
        }
    }


}