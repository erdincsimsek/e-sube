﻿using ESube.Service.Models.Helpers;
using ESube.Service.Context;
using ESube.Service.AES256Encryption;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web.Helpers;
using System.Collections.Generic;
using ESube.Service.Helpers;

namespace ESube.Service.Models
{
    [Table("User")]
    public class User : BaseEntityClass
    {
        //Constractor
        public User(){
            RoleList = new List<UserRole>();
        }


        //[Required(ErrorMessage = "Required Username")]
        //[StringLength(30, MinimumLength = 2, ErrorMessage = "Username Must be Minimum 2 Charaters")]
        public string Username { get; set; }

        //[DataType(DataType.Password)]
        //[Required(ErrorMessage = "Required Password")]
        //[MaxLength(30,ErrorMessage = "Password cannot be Greater than 30 Charaters")]
        //[StringLength(31, MinimumLength = 7 , ErrorMessage ="Password Must be Minimum 7 Charaters")]
        public string Password { get; set; }

        //[Required(ErrorMessage = "Required EmailID")]
        //[RegularExpression(@"[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}",ErrorMessage = "Please enter Valid Email ID")]
        public string EmailAddress { get; set; }

        #region RELATIONSHIPS
        public long CompanyId { get; set; }
        public virtual List<UserRole> RoleList { get; set; }
        #endregion

    }

    public class UserForm
    {
        public string EmailAddress { get; set; }
        public string Username { get; set; }
        public string EmailAddressOrUsername { get; set; }
        public string Password { get; set; }
        public string PasswordConfirm { get; set; }

        //Firma İçin
        public string CompanyName { get; set; }
        public string CompanyCode { get; set; }
        public string PersonInCharge { get; set; }
        public int Status{ get; set; }

        public ReturnObject UserRegister()
        {
            ReturnObject retObj = new ReturnObject();
            User _User = new User();
            MainCompany _Company = new MainCompany();

            using (ESubeContext _db = new ESubeContext())
            {
                try
                {
                    //FIRMA
                    _Company = _db.MainCompany.Where(x => x.Id == _User.CompanyId).FirstOrDefault();
                    if (_Company == null)
                    {
                        _Company = new MainCompany();
                        _Company.Name = CompanyName;
                        _Company.Code = CompanyCode.ToUpper();
                        _Company.PersonInCharge = PersonInCharge;
                        _Company.EmailAddress = EmailAddress;
                        _db.MainCompany.Add(_Company);
                        _db.SaveChanges();

                        retObj.Result = true;
                        retObj.ResultMessage = "Kayıt İşlemi Başarılı";
                    }
                    else
                    {
                        retObj.Result = false;
                        retObj.ResultMessage = "Firma Kaydedilemedi";
                        retObj.Id = 0;
                    }


                    //KULLANICI        
                    _User.EmailAddress = EmailAddress;
                    _User.Username = Username;
                    _User.Password = EncryptionLibrary.EncryptText(Password);
                    _User.IsActive = true;
                    _User.CompanyId = _Company.Id;
                
                    _db.User.Add(_User);
                    _db.SaveChanges();

                    retObj.Result = true;
                    retObj.ResultMessage = "Kayıt İşlemi Başarılı";
                    retObj.Id = _User.Id;





                    //ROLE
                    long RoleId = (from x in _db.Role where x.Code == "CMP" select x.Id).FirstOrDefault();
                    var _UserRole = new UserRole();
                    _UserRole.RoleId = RoleId;
                    _UserRole.UserId = _User.Id;
                    _db.UserRole.Add(_UserRole);
                    _db.SaveChanges();


                }
                catch (Exception x)
                {
                    retObj.Result = false;
                    retObj.ResultMessage = "Kullanıcı Bilgileri Kaydedilirken Hata Oluştu";
                }

            }


            return retObj;

        }

        public ReturnObject ValidateRegister()
        {
            ReturnObject retObj = new ReturnObject();
            ESubeContext _db = new ESubeContext();

            if (string.IsNullOrEmpty(EmailAddress))
            {
                retObj.ResultMessage = "Email Adresi Giriniz";
            }
            else if (string.IsNullOrEmpty(Username))
            {
                retObj.ResultMessage = "Kullanıcıadı Giriniz";
            }
            else if (string.IsNullOrEmpty(Password))
            {
                retObj.ResultMessage = "Şifre Giriniz";
            }
            else if (string.IsNullOrEmpty(PasswordConfirm))
            {
                retObj.ResultMessage = "Şifreyi Doğrulaması Yapınız";
            }
            else if (Password != PasswordConfirm)
            {
                retObj.ResultMessage = "Girilen Şifreler Aynı Değil";
            }
            else if (string.IsNullOrEmpty(CompanyName))
            {
                retObj.ResultMessage = "Firma Adını Giriniz";
            }
            else if (string.IsNullOrEmpty(CompanyCode))
            {
                retObj.ResultMessage = "Firmanızı ifade edecek en fazla 5 karakterden oluşan bir kısa kod giriniz";
            }
            else if (string.IsNullOrEmpty(PersonInCharge))
            {
                retObj.ResultMessage = "Firma Yetkili Kişi Bilgisini Giriniz";
            }
            else
            {
                //Hesap zaten varmı yokmu
                bool isExistsUser = _db.User.Where(u => u.EmailAddress == EmailAddress || u.Username == Username).Count() == 1 ? true : false;
                bool isExistCompany = _db.MainCompany.Where(x => x.Code == CompanyCode ).Count() == 1 ? true : false;
                if (isExistsUser)
                {
                    retObj.ResultMessage = "Kayıt Zaten Sistemde Mevcut !";
                }
                else if (isExistCompany)
                {
                    retObj.ResultMessage = "Firma Kodu Zaten Sistemde Mevcut !";
                }
                else
                {
                    retObj.Result = true;
                    retObj.ResultMessage = "";
                }

            }

            return retObj;
        }

        public ReturnObject ValidateLogin()
        {
            ReturnObject retObj = new ReturnObject();
            ESubeContext _db = new ESubeContext();
            User ExistsUser = new User();

            if (string.IsNullOrEmpty(EmailAddressOrUsername))
            {
                retObj.ResultMessage = "Please enter your username or emailAddress";
            }
            else if (string.IsNullOrEmpty(Password))
            {
                retObj.ResultMessage = "Please enter your password";
            }
            else
            {
                Password = EncryptionLibrary.EncryptText(Password);
                try
                {
                    ExistsUser = _db.User.Where(u => (u.EmailAddress == EmailAddressOrUsername || u.Username == EmailAddressOrUsername) && u.Password == Password).FirstOrDefault();
                    if (ExistsUser == null)
                    {
                        retObj.ResultMessage = "Please check your login informations";
                    }
                    else
                    {
                        if (!ExistsUser.IsActive)
                        {
                            retObj.Result = false;
                            retObj.ResultMessage = "Hesabınız Aktif Edilmemiş. Sistem Yöneticinize Başvurunuz.";
                        }
                        else
                        {
                            retObj.Result = true;
                            retObj.ResultMessage = "Login Successful";
                        }
                    }
                }
                catch (Exception e)
                {
                    retObj.Result = false;
                    retObj.ResultMessage = e.Message.ToString();
                }

            

            }
            return retObj;

        }


        //public User GetUserData(string pUsername, string pPassword)
        //{
        //    string pass = EncryptionLibrary.EncryptText(pPassword);
        //    return _db.User.Where(x => x.EmailID == pass && x.Password == pass).Count() == 1 ? true : false;
        //}

    }
}