﻿//using ESube.Service.Context;
//using ESube.Service.Models;
//using ESube.Service.Models.Helpers;
//using System;
//using System.Collections.Generic;
//using System.ComponentModel;
//using System.ComponentModel.DataAnnotations;
//using System.ComponentModel.DataAnnotations.Schema;
//using System.Runtime.Serialization;
//using System.ServiceModel;

//namespace ESube.Service.Models
//{
//    /// <summary>
//    /// Sevk edilen sipariş nesnesi
//    /// </summary>
//    [Table("vw_shippedOrder")]
//    public class ShippedOrderVW
//    {
//        public ShippedOrderVW() { }
//        long _rowNumber;
//        [Key]
//        public long rowNumber
//        {
//            get { return _rowNumber; }
//            set { _rowNumber = value; }
//        }
//        /// <summary>
//        /// Firma kodu
//        /// </summary>
//        [StringLength(25)]
//        public string mainCompanyCode { get; set; }

//        /// <summary>
//        /// Depo kodu
//        /// </summary>
//        [StringLength(25)]
//        public string companyBranchCode { get; set; }

//        /// <summary>
//        /// Nakliye(sevk) kodu
//        /// </summary>
//        [StringLength(50)]
//        public string shipmentCode { get; set; }

//        /// <summary>
//        /// Araç Plakası
//        /// </summary>
//        [StringLength(25)]
//        public string plate { get; set; }

//        /// <summary>
//        /// Araç sürücüsü
//        /// </summary>
//        [StringLength(100)]
//        public string driverName { get; set; }

//        /// <summary>
//        /// Araç sürücüsü TC no
//        /// </summary>
//        [StringLength(20)]
//        public string driverNationalityID { get; set; }

//        /// <summary>
//        /// Sipariş tipi (1 : Mal kabul , -1 : Depo Çıkış)
//        /// </summary>
//        [StringLength(10)]
//        public string orderType { get; set; }

//        /// <summary>
//        /// Sipariş referans Numarası
//        /// </summary>
//        [StringLength(50)]
//        public string orderRefNumber { get; set; }

//        /// <summary>
//        /// Sipariş Numarası
//        /// </summary>
//        [StringLength(50)]
//        public string orderNumber { get; set; }

//        /// <summary>
//        /// Sipariş tarihi
//        /// </summary>
//        [StringLength(10)]
//        public string orderDate { get; set; }

//        /// <summary>
//        /// İrsaliye Numarası
//        /// </summary>
//        [StringLength(20)]
//        public string dispatchNumber { get; set; }

//        /// <summary>
//        /// İrsaliye basım tarihi(YYYY-MM-DD)
//        /// </summary>
//        [StringLength(10)]
//        public string issueDate { get; set; }

//        /// <summary>
//        /// İrsaliye basım zamanı (HH:MM:SS)
//        /// </summary>
//        [StringLength(10)]
//        public string issueTime { get; set; }

//        /// <summary>
//        /// İrsaliye tipi
//        /// </summary>
//        [StringLength(50)]
//        public string dispatchProfileID { get; set; }

//        /// <summary>
//        /// İrsaliye dosya adı
//        /// </summary>
//        [StringLength(200)]
//        public string dispatchImageName { get; set; }

//        /// <summary>
//        /// Palet barkodu
//        /// </summary>
//        [StringLength(50)]
//        public string palletBarcode { get; set; }

//        /// <summary>
//        /// Satır sırası
//        /// </summary>
//        [StringLength(10)]
//        public string lineCode { get; set; }

//        /// <summary>
//        /// Ürün Kodu
//        /// </summary>
//        [StringLength(50)]
//        public string itemCode { get; set; }

//        /// <summary>
//        /// Ürün adı
//        /// </summary>
//        [StringLength(200)]
//        public string itemName { get; set; }

//        /// <summary>
//        /// Lot numarası
//        /// </summary>
//        [StringLength(20)]
//        public string lotCode { get; set; }

//        /// <summary>
//        /// Lot numarası (YYYY-MM-DD)
//        /// </summary>
//        [StringLength(10)]
//        public string expiryDate { get; set; }

//        /// <summary>
//        /// Miktar
//        /// </summary>
//        [StringLength(200)]
//        public string quantity { get; set; }

//        /// <summary>
//        /// Çıkış birimi (Adet,Koli,Palet vb)
//        /// </summary>
//        [StringLength(200)]
//        public string packageType { get; set; }

//        /// <summary>
//        /// Çıkış birimi (Adet,Koli,Palet vb)
//        /// </summary>
//        [StringLength(10)]
//        public string grossWeight { get; set; }

//    }

//}