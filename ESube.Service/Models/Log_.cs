﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using ESube.Service.Context;

namespace ESube.Service.Models
{

    public class Log_: DLBaseObject
    {
        //public _Log()
        //{
           
        //}

        string _host;
        public string host
        {
            get { return _host; }
            set { _host = value; }
        }

        string _headers;
        public string headers
        {
            get { return _headers; }
            set { _headers = value; }
        }

        string _statusCode;
        public string statusCode
        {
            get { return _statusCode; }
            set { _statusCode = value; }
        }

        string _requestBody;
        public string requestBody
        {
            get { return _requestBody; }
            set { _requestBody = value; }
        }

        string _requestedMethod;
        public string requestedMethod
        {
            get { return _requestedMethod; }
            set { _requestedMethod = value; }
        }

        string _userHostAddress;
        public string userHostAddress
        {
            get { return _userHostAddress; }
            set { _userHostAddress = value; }
        }

        string _userAgent;
        public string userAgent
        {
            get { return _userAgent; }
            set { _userAgent = value; }
        }

        string _absoluteUri;
        public string absoluteUri
        {
            get { return _absoluteUri; }
            set { _absoluteUri = value; }
        }

        string _requestType;
        public string requestType
        {
            get { return _requestType; }
            set { _requestType = value; }
        }

        enumLogType _logType;
        public enumLogType logType
        {
            get { return _logType; }
            set { _logType = value; }
        }

        string _message;
        public string message
        {
            get { return _message; }
            set { _message = value; }
        }

        DateTime? _UtcTime;
        public DateTime? UtcTime
        { 
            get { return _UtcTime; }
            set { _UtcTime = value; }
        }

        string _integrationRequestParentCode;
        public string integrationRequestParentCode
        {
            get { return _integrationRequestParentCode; }
            set { _integrationRequestParentCode = value; }
        } 

        string _integrationRequestCode;
        public string integrationRequestCode
        {
            get { return _integrationRequestCode; }
            set { _integrationRequestCode = value; }
        }

        string _integrationRequestData;
        [Column(TypeName = "text")]
        public string integrationRequestData
        {
            get { return _integrationRequestData; }
            set { _integrationRequestData = value; }
        }

        string _integrationResponseCode;
        public string integrationResponseCode
        {
            get { return _integrationResponseCode; }
            set { _integrationResponseCode = value; }
        }

        string _IntegrationResponseMessage;
        public string IntegrationResponseMessage
        {
            get { return _IntegrationResponseMessage; }
            set { _IntegrationResponseMessage = value; }
        }

        string _IntegrationResponseData;
        [Column(TypeName = "text")]
        public string IntegrationResponseData
        {
            get { return _IntegrationResponseData; }
            set { _IntegrationResponseData = value; }
        }


        string _IntegrationApplication;
        public string IntegrationApplication
        {
            get { return _IntegrationApplication; }
            set { _IntegrationApplication = value; }
        }




    }

    public enum enumLogType:int
    {
        log=0,Error=100
    }

    public static class Logging
    {
        private static ESubeContext db = new ESubeContext();

        public static bool InsertLog(Log_ log)
        {
            bool succes = true;
            try
            {
                //db.Log_.Add(log);
                db.SaveChanges();
                return succes;
            }
            catch (Exception ex)
            {
                return false;
                //throw new Exception("Log işlemi sırasında hata alındı.",ex.InnerException);
            }
            
        }
    }
}