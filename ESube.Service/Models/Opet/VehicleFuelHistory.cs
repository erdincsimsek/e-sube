﻿using ESube.Service.Context;
using ESube.Service.Models;
using ESube.Service.Models.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Linq;
using ESube.Service.Models.Views;
using ESube.Service.Models.Enums;
using System.Data.SqlClient;


namespace ESube.Service.Models.Opet
{
    public class VehicleFuelHistory
    {

        public string VEHI_PLATENUMBER { get; set; }
        //public int VEFH_ID { get; set; }
        public string VEFH_ENTEREDBY { get; set; }
        public DateTime? VEFH_ENTRYDATETIME { get; set; }
        public string VEFH_NOTES { get; set; }
        public Decimal? VEFH_BASEPRICE { get; set; }
        public Decimal? VEFH_LITRE { get; set; }
        public string VEFH_CPU { get; set; }
        public string VEFH_TOOLNO { get; set; }
        public string VEFH_VOUCHERNO { get; set; }
        public Decimal? VEFH_KM { get; set; }
        public Int64? VEFH_FUELTYPEID { get; set; }
        public Int64? VEFH_VEHICLEID { get; set; }
        public DateTime? VEFH_LOADDATE { get; set; }
        public Int64? VEFH_TRIPID { get; set; }
        public Decimal? VEFH_CURRENCYPRICE { get; set; }
        public Int64? VEFH_CURRENCYCODEID { get; set; }
        public Decimal? VEFH_CURRENCYRATE { get; set; }
        public Int64? VEFH_GASSTATIONID { get; set; }
        public Int64? VEFH_LASTFUELLOADKM { get; set; }
        public string VEFH_UPDATEDBY { get; set; }
        public DateTime? VEFH_UPDATEDDATETIME { get; set; }
        public Int64? VEFH_PARTYADDRESSID { get; set; }
        public Decimal? VEFH_BEGINNINGLITRE { get; set; }
        public Decimal? VEFH_ENDINGLITRE { get; set; }
        public Boolean? VEFH_ISFULL { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? LastModifiedDate { get; set; }




        public List<VehicleFuelHistory> GetList()
        {
            List<VehicleFuelHistory> _Return = new List<VehicleFuelHistory>();

            using (ESubeContext db = new ESubeContext())
            {
                // SP den al
                //List<SqlParameter> paramlist = new List<SqlParameter>();
                //paramlist.Add(new SqlParameter("pOrderCode", string.IsNullOrEmpty(pOrderCode) ? "" : pOrderCode));
                //paramlist.Add(new SqlParameter("pTripCode", string.IsNullOrEmpty(pTripCode) ? "" : pTripCode));
                _Return = db.Database.SqlQuery<VehicleFuelHistory>("Sp_Opet_Get_VehicleFuelHistory").ToList();

            }

            return _Return;
        }
    }
}