﻿using ESube.Service.Models.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ESube.Service.Models
{
    //[Table("vw_OrderDocument")]
    //[Table("OrderDocument")]
    public class OrderDocument :BaseIntegrationClass
    {
        public Order order { get; set; }

        public string orderCode{ get; set; }
        public int documentCode{ get; set; }
        public int reason { get; set; }
        public int documentType { get; set; }
        public int documentStatus { get; set; }
        public int wareHouseCode { get; set; }
        public int IntegrationCode { get; set; }


        //[Key]
        //public string OrderCode { get; set; }
        //public string Company { get; set; }
        //public string CompanyCode { get; set; }

        //public string ShipmentCode { get; set; }
        //public string DocumentCode { get; set; }
        //public string OrderStatus { get; set; }
        //public string Supplier { get; set; }


        ////[Column(TypeName = "datetime2")]
        //public string ReceiptDate { get; set; }
        //public string MailBody { get; set; }
        //public string MailSubject { get; set; }
        //public string ReceiverMailAddresses { get; set; }

    }
}