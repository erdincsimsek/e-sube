﻿using ESube.Service.Context;
using ESube.Service.Models.Enums;
using ESube.Service.Models.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.Linq;

namespace ESube.Service.Models.Views
{

    //[Table("vwOrderReturn")]
    public class vwOrderReturn
    {
        //[Key]
        public long LineCode { get; set; }

        ////SİPARİŞ BİLGİLERİ
        public string OrderCode { get; set; }
        public int OrderType { get; set; }
        public int OrderSubType { get; set; }
        public string Company { get; set; }
        public string MainCompanyCode { get; set; }
        public string ShipmentCode { get; set; }
        public string ReceiptDate { get; set; }
        public string OrderDate { get; set; }
        public string Supplier { get; set; }
        public string OrderStatus { get; set; }
        public string MailSubject { get; set; }
        public string MailBody { get; set; }
        public string ReceiverMailAddresses { get; set; }


        ////Döküman bilgileri( irsaliye veya fotoğraf veya tutanak)
        public int DocumentType { get; set; }
        public string DocumentTypeDesc { get; set; }
        public string DocumentCode { get; set; }
        public string DocumentDate { get; set; }

        public int PackageType { get; set; }
        public string ExpiryDate { get; set; }

        public string VehiclePlate { get; set; }
        public string PalletBarcode { get; set; }


        ////Ürünün sipariş ile ilgili bilgileri
        public string LotNumber { get; set; }

        ///// <summary>
        ///// Ürün kaç adet çıkılmış
        ///// </summary>
        public double Quantity { get; set; }
        ///// <summary>
        ///// Ürün kaç adet sipariş verilmiş
        ///// </summary>
        public double QuantityTotal { get; set; }
        ///// <summary>
        ///// Çıkılan ürün adedi , kaç pakettir ? kutusunda 10 adet bulunan bir üründen 1 tane çıkılmış ise , 0.1 kabul edilir.
        ///// </summary>
        public double QuantityPackage { get; set; }
        ///// <summary>
        ///// çıkılan ürünlerin toplam ağırlığı
        ///// </summary>
        public double WeightNetPackageTotal { get; set; }
        public string WeightNetPackageTotalStr { get { return (WeightNetPackageTotal.ToString() + " Kg"); } set { } }

        public double WeightGrossPackageTotal { get; set; }
        public string WeightGrossPackageTotalStr { get { return (WeightNetPackageTotal.ToString() + " Kg"); } set { } }



        ////Ürün master bilgileri
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public double WeightNetItemUnit { get; set; }
        public double WeightGrossItemUnit { get; set; }
        public double QuantityItemInStandart { get; set; }
    }
}