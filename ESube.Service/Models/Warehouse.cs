﻿using ESube.Service.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ESube.Service.Models
{
    public class Warehouse : BaseEntityClass
    {
        public string WarehouseCode { get; set; }
        public string WarehouseName { get; set; }
        public string CountryCode { get; set; }
        public string CityName { get; set; }
        public string CityCode { get; set; }
        public string TownName { get; set; }
        public string TownCode { get; set; }
        public decimal? StorageSpace { get; set; }
        public decimal? StorageVolume { get; set; }
        public decimal? StoreVolume { get; set; }
        public int? StorablePalletsPiece { get; set; }




    }
}