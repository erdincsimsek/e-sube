﻿using ESube.Service.Context;
using ESube.Service.Helpers;
using ESube.Service.Models.Enums;
using ESube.Service.Models.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Web;

namespace ESube.Service.Models
{
    /// <summary>
    /// Günlük stok tablosu
    /// </summary>
    [Table("DailyStock")]
    public class DailyStock : BaseIntegrationClass
    {
        public DailyStock()
        {

            StockSourceCode = EnumStockSource.Customer;
        }

        string _mainCompanyCode;
        /// <summary>
        /// Firma kodu
        /// </summary>
        [IgnoreDataMember]
        public string mainCompanyCode
        {
            get { return _mainCompanyCode; }
            set { _mainCompanyCode = value; }
        }


        string _warehouseCode;
        /// <summary>
        /// Depo kodu
        /// </summary>
        [Required]
        public string warehouseCode
        {
            get { return _warehouseCode; }
            set { _warehouseCode = value; }
        }

        string _stockDate;
        /// <summary>
        /// Stok tarihi
        /// </summary>
        [Required]
        public string stockDate
        {
            get { return _stockDate; }
            set { _stockDate = value; }
        }

        public DateTime stockDateSQL { get; set; }

        string _itemCode;
        /// <summary>
        /// Ürün kodu
        /// </summary>
        public string itemCode
        {
            get { return _itemCode; }
            set { _itemCode = value; }
        }

        string _itemName;
        /// <summary>
        /// Ürün adı
        /// </summary>
        public string itemName
        {
            get { return _itemName; }
            set { _itemName = value; }
        }

        long _quantity;
        /// <summary>
        /// Miktar
        /// </summary>
        public long quantity
        {
            get { return _quantity; }
            set { _quantity = value; }
        }

        long _packageCapacity;
        /// <summary>
        /// Paket kapasitesi
        /// </summary>
        public long packageCapacity
        {
            get { return _packageCapacity; }
            set { _packageCapacity = value; }
        }

        long _palleteCapacity;
        /// <summary>
        /// Palet kapasitesi
        /// </summary>
        public long palleteCapacity
        {
            get { return _palleteCapacity; }
            set { _palleteCapacity = value; }
        }

        string _location;
        /// <summary>
        /// Lokasyon. Ürünün bulunduğu lokasyon.
        /// </summary>
        public string location
        {
            get { return _location; }
            set { _location = value; }
        }

        string _expireDate;
        /// <summary>
        /// Son kullanma tarihi
        /// </summary>
        public string expireDate
        {
            get { return _expireDate; }
            set { _expireDate = value; }
        }

        string _lotCode;
        /// <summary>
        /// Lot numarası
        /// </summary>
        public string lotCode
        {
            get { return _lotCode; }
            set { _lotCode = value; }
        }
        [IgnoreDataMember]
        public EnumStockSource StockSourceCode { get; set; }
        [IgnoreDataMember]
        public string StockSource { get { return (Enum.GetName(typeof(EnumSyncStatus), StockSourceCode)); } set { } }


        string _ItemSmallUnit;
        /// <summary>
        /// Ürün Küçük Birimi
        /// </summary>
        public string itemSmallUnit
        {
            get { return _ItemSmallUnit; }
            set { _ItemSmallUnit = value; }
        }
        #region FUNCTIONS
        public void AddStockObjectList(StockObject pStockObject, string pMainCompanyCode)
        {
            using (ESubeContext db = new ESubeContext())
            {

                foreach (StockObjectItem item in pStockObject.ListItems)
                {

                    DailyStock obj = new DailyStock();

                    obj.mainCompanyCode = pMainCompanyCode;
                    obj.warehouseCode = pStockObject.companyBranchCode;
                    obj.stockDate = pStockObject.stockDate;
                    obj.itemCode = item.ItemCode;
                    obj.lotCode = item.LotCode;
                    obj.quantity = item.Quantity;
                    obj.expireDate = item.ExpiryDate;
                    obj.CreateDate = DateTime.Now;
                    obj.palleteCapacity = 0;
                    obj.packageCapacity = 0;
                    obj.StockSourceCode = EnumStockSource.Customer;
                    obj.SyncStatusCode = 0;
                    obj.SyncDesc = "";


                    db.DailyStock.Add(obj);
                }
                db.SaveChanges();
            }
        }
        public List<DailyStock> GetList(string pRequestDate, string pWareHouseCode, string pMainCompanyCode)
        {
            using (ESubeContext db = new ESubeContext())
            {
                var list = db.DailyStock.Where(x =>
                (string.IsNullOrEmpty(pRequestDate) ? 1 == 1 : x.stockDate.Replace("-", "") == pRequestDate.Replace("-", ""))
                && (string.IsNullOrEmpty(pWareHouseCode) ? 1 == 1 : x.warehouseCode == pWareHouseCode)
                && (string.IsNullOrEmpty(pMainCompanyCode) ? 1 == 1 : x.mainCompanyCode == pMainCompanyCode)

                ).ToList();
                return list;
            }
        }




        public DataTable ExcelExport(string pRequestDate, string pWareHouseCode, string pMainCompanyCode)
        {
            DataTable dt = new DataTable();
            using (ESubeContext db = new ESubeContext())
            {

                var dailyStocks = db.DailyStock.Where(x =>
                (string.IsNullOrEmpty(pRequestDate) ? 1 == 1 : x.stockDate.Replace("-", "") == pRequestDate.Replace("-", ""))
                && (string.IsNullOrEmpty(pWareHouseCode) ? 1 == 1 : x.warehouseCode == pWareHouseCode)
                && (string.IsNullOrEmpty(pMainCompanyCode) ? 1 == 1 : x.mainCompanyCode == pMainCompanyCode)).Select(d => new
                {
                    d.mainCompanyCode,
                    d.warehouseCode,
                    d.stockDate,
                    d.itemCode,
                    d.itemName,
                    d.quantity,
                    d.itemSmallUnit,
                    d.expireDate,
                }
                ).ToList();


                if (dailyStocks.Any())
                {
                    ListtoDataTableConverter converter = new ListtoDataTableConverter();
                    dt = converter.ToDataTable(dailyStocks);
                }

            }
            return dt;

        }

        #endregion
    }
        [Table("vw_dailyStock")]
        public class DailyStokcVW
        {
            long _rowNumber;
            [Key]
            public long rowNumber
            {
                get { return _rowNumber; }
                set { _rowNumber = value; }
            }

            string _expireDate;
            public string expireDate
            {
                get { return _expireDate; }
                set { _expireDate = value; }
            }

            string _itemCode;
            public string itemCode
            {
                get { return _itemCode; }
                set { _itemCode = value; }
            }

            string _warehouseCode;
            public string warehouseCode
            {
                get { return _warehouseCode; }
                set { _warehouseCode = value; }
            }

            long _quantity;
            public long quantity
            {
                get { return _quantity; }
                set { _quantity = value; }
            }

            [Required]
            string _stockDate;
            public string stockDate
            {
                get { return _stockDate; }
                set { _stockDate = value; }
            }

            string _lotCode;
            public string lotCode
            {
                get { return _lotCode; }
                set { _lotCode = value; }
            }

            string _locationCode;
            public string locationCode
            {
                get { return _locationCode; }
                set { _locationCode = value; }
            }

            public string mainCompanyCode { get; set; }

        }



        /// <summary>
        /// Müşteriden gelen stok Dincer veritabanına aktarılır.
        /// </summary>
        public class StockObject : BaseIntegrationClass
        {
            public StockObject() { ListItems = new List<StockObjectItem>(); }
            /// <summary>
            /// Depo kodu
            /// </summary>
            [Required]
            public string companyBranchCode { get; set; }
            /// <summary>
            /// Stok Tarihi
            /// </summary>
            [Required]
            public string stockDate { get; set; }
            public DateTime stockDateSQL { get; set; }

            public virtual List<StockObjectItem> ListItems { get; set; }




        }

        public class StockObjectItem : BaseIntegrationClass
        {

            public StockObjectItem() { }
            /// <summary>
            /// Ürün Kodu
            /// </summary>
            public string ItemCode { get; set; }
            /// <summary>
            /// Lot(Parti) numarası
            /// </summary>
            public string LotCode { get; set; }
            /// <summary>
            /// Son Kullanma Tarihi
            /// </summary>
            public string ExpiryDate { get; set; }
            /// <summary>
            /// Adet
            /// </summary>
            public long Quantity { get; set; }

            [IgnoreDataMember]
            public virtual StockObject StockObjectId { get; set; }
        }

    }
