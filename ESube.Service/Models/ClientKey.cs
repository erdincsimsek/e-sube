﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Xml.Serialization;
using System.Runtime.Serialization;
namespace ESube.Service.Models
{
    [Table("ClientKey")]
    public class ClientKey
    {
        [Key]
        [IgnoreDataMember]
        public int ClientKeyID { get; set; }
        [IgnoreDataMember]
        public long CompanyID { get; set; }
       public string ClientID { get; set; }
       
        public string ClientSecret { get; set; }
        [IgnoreDataMember]
        public DateTime CreateOn { get; set; }
        [IgnoreDataMember]
        public long UserID { get; set; }

    }
}