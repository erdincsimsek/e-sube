﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using System.ComponentModel.DataAnnotations;
using Newtonsoft;
using Newtonsoft.Json.Serialization;
using System.Runtime.Serialization;
using System.ServiceModel;
using ESube.Service.Models.Enums;

namespace ESube.Service.Models
{
    [Table("ServiceResponse")]
    public class ServiceResponse
    { 
        public ServiceResponse() { }

        [Key]
        public string Code { get; set; }
        public string Description { get; set; }
    }

    [Table("IntegrationResponse")]
    public class IntegrationResponse : DLBaseObject
    {
        public IntegrationResponse() { }

        string _code;
        public string code
        {
            get { return _code; }
            set { _code = value; }
        }

        string _description;
        public string description
        {
            get { return _description; }
            set { _description = value; }
        }

    }

    public class clientResponse
    {
        public EnumResponseCode code { get; set; }
        public string orderCode { get; set; }

        public string message { get; set; }

        public string errorMessage { get; set; }
        //public bool result { get; set; }
        //public string resultMessage { get; set; }
        //public int InsertedId { get; set; }

        public List<WayBillGoodsIn> HemelWayBillGoodsInList { get; set; }

    }
    public class clientItemResponse
    {
        public EnumResponseCode code { get; set; }
        public string itemCode { get; set; }

        public string message { get; set; }

        public string errorMessage { get; set; }

    }

    public class clientGetirResponse
    {
        public string MethodName { get; set; }
        public bool Success { get; set; }
        public List<clientGetirResponseError> Errors { get; set; }
    }

    public class clientGetirResponseError
    {
        public EnumResponseCode ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
    }
}