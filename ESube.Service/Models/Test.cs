﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ESube.Service.Models
{
    public class Test
    {
        [Key]
        public int Id { get; set; }
        public string  Description{ get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime CreateDate { get; set; }
    }
}