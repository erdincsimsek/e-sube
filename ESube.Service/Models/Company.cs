﻿using ESube.Service.Context;
using ESube.Service.Models;
using ESube.Service.Models.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Linq;
using ESube.Service.Models.Views;
using ESube.Service.Models.Enums;
using System.Data.SqlClient;

namespace ESube.Service.Models
{
    /// <summary>
    /// Firmalar
    /// </summary>
    [Table("Company")]
    public class Company : BaseIntegrationClass
    {
        /// <summary>
        /// Müşteri kodu
        /// </summary>
        //[StringLength(50)]
        //[IgnoreDataMember]
        public string MainCompanyCode { get; set; }
        public EnumCompanyType CompanyType { get; set; }
        [IgnoreDataMember,NotMapped]
        public virtual string CompanyTypeDesc { get { return (Enum.GetName(typeof(EnumCompanyType), CompanyType)); } set { } }

        public string CompanyCode { get; set; }
        public string ParentCompanyCode { get; set; }

        /// <summary>
        /// Müşteri adı
        /// </summary>
        //[StringLength(500)]
        public string Name { get; set; }
        //[Required]
        public string TaxNumber { get; set; }
        public string TaxDepartmentName { get; set; }
        //[Required]
        public string CountryCode { get; set; }
        //[Required]
        public string CityCode { get; set; }
        public string CityName { get; set; }
        //[Required]
        public string TownCode { get; set; }
        public string TownName { get; set; }
        public string AddressText { get; set; }
        /// <summary>
        /// Müşteri GLN numarası
        /// </summary>
        //[StringLength(50)]
        public string GLN { get; set; }

        /// <summary>
        /// e-irsaliye mükellefi mi?
        /// </summary>
        public bool IsDispatcher { get; set; }

        /// Firma Telefon numarası
        /// </summary>
        // [StringLength(50)]
        public string Phone { get; set; }



        /// <summary>
        /// Posta kodu
        /// </summary>
        //[StringLength(10)]
        public string PostCode { get; set; }

        /// <summary>
        /// Kontak kişi kodu
        /// </summary>
       // [StringLength(25)]
        public string ContactCode { get; set; }

        /// <summary>
        /// Kontak kişi adı (teslim alacak kişi)
        /// </summary>
       [StringLength(12)]
        public string ContactName { get; set; }



        /// <summary>
        /// Kontak mail adresi
        /// </summary>
       // [StringLength(250)]
        public string ContactMail { get; set; }

        /// <summary>
        /// Kontak Telefon numarası
        /// </summary>
        // [StringLength(50)]
        public string ContactPhone { get; set; }


        /// <summary>
        /// Firmanın siparişteki rolü
        /// </summary>
        [Required]
        public CompanyOrderRoleType OrderRoleType  {get;set;}


    }
}