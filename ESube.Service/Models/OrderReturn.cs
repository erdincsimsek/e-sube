﻿using ESube.Service.Context;
using ESube.Service.Models;
using ESube.Service.Models.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Linq;
using ESube.Service.Models.Views;
using ESube.Service.Models.Enums;
using System.Data.SqlClient;

namespace ESube.Service.Models.OrderReturn
{
    public class OrderReturn : BaseIntegrationClass
    {
        public string OrderCode { get; set; }
        public int OrderType { get; set; }
        [NotMapped]
        public string OrderTypeDesc { get { return (Enum.GetName(typeof(EnumOrderType), OrderType)); } set { } }
        public int OrderSubType { get; set; }
        public string OrderSubTypeDesc { get { return (Enum.GetName(typeof(EnumOrderSubType), OrderSubType)); } set { } }

        public string OrderDate { get; set; }
        public string ReceiptDate { get; set; }
        public double TotalPackage { get; set; }
        public string TotalPackageDesc { get { return (TotalPackage.ToString() + " Package"); } set { } }

        public double TotalNetWeight { get; set; }
        public string TotalNetWeightDesc { get { return (TotalNetWeight.ToString() + " Kg"); } set { } }
        public double TotalGrossWeight { get; set; }
        public string TotalGrossWeightDesc { get { return (TotalGrossWeight.ToString() + " Kg"); } set { } }

        //public string ShipmentDate { get; set; }
        //public string WareHouseCode { get; set; }
        public List<Document> DocumentList { get; set; }
        public List<Status> StatusList { get; set; }


        #region FUNCTIONS
        public List<OrderReturn> GetList(string pCompanyCode, EnumOrderType pOrderType, string pOrderCode)
        {
            List<OrderReturn> _OrderReturn = new List<OrderReturn>();

            using (ESubeContext db = new ESubeContext())
            {

                // SP den al
                List<SqlParameter> paramlist = new List<SqlParameter>();
                paramlist.Add(new SqlParameter("pMainCompanyCode", pCompanyCode));
                paramlist.Add(new SqlParameter("pOrderType", Convert.ToInt32(pOrderType)));
                paramlist.Add(new SqlParameter("pOrderCode", pOrderCode.ToString()));
                List<vwOrderReturn> data = db.Database.SqlQuery<vwOrderReturn>("Sp_Get_Order_Return_Data @pMainCompanyCode,@pOrderType,@pOrderCode", paramlist.ToArray()).OrderBy(x=>x.LineCode).ToList();
               
                //Siparişe göre grupladım...
                if (data.Count > 0)
                {
                    _OrderReturn = (from x in data
                                    group x by new
                                    {
                                        x.OrderCode,
                                        x.OrderType,
                                        x.OrderSubType,
                                        x.ReceiptDate,
                                        x.OrderDate
                                    } into tmpListx
                                    select new OrderReturn()
                                    {
                                        OrderCode = tmpListx.Key.OrderCode,
                                        OrderType = tmpListx.Key.OrderType,
                                        OrderSubType = tmpListx.Key.OrderSubType,
                                        ReceiptDate = tmpListx.Key.ReceiptDate,
                                        OrderDate = tmpListx.Key.OrderDate
                                    }).ToList();
                    //her bir sipariş için dön dedim, dökümanlarını içine eklemek için
                    for (int i = 0; i < _OrderReturn.Count; i++)
                    {
                        //Dökümana göre grupladım...
                        var documentList = (from y in data.Where(x => x.OrderCode == _OrderReturn[i].OrderCode).ToList()
                                            group y by new
                                            {
                                                y.OrderCode,
                                                y.DocumentType,
                                                //y.DocumentTypeDesc,
                                                y.DocumentCode
                                            } into docList
                                            select new Document()
                                            {
                                                DocumentType = docList.Key.DocumentType,
                                                //DocumentTypeDesc = docList.Key.DocumentTypeDesc,
                                                DocumentCode = docList.Key.DocumentCode

                                            }).ToList();

                        _OrderReturn[i].DocumentList = documentList;

                        //döküman liste, ürünleri ekleyeğim ItemList diye...
                        for (int j = 0; j < _OrderReturn[i].DocumentList.Count; j++)
                        {

                            List<DocumentItem> DocumentItemList = new List<DocumentItem>();

                            var tmpFilteredData = data.Where(c => c.OrderCode == _OrderReturn[i].OrderCode).ToList();


                            foreach (var itemK in tmpFilteredData)
                            {
                                if (!string.IsNullOrEmpty(itemK.ItemCode))
                                {

                                    DocumentItem _DocumentItem = new DocumentItem();


                                    //Ürünün sipariş ile ilgili bilgileri
                                    _DocumentItem.LineCode = itemK.LineCode;
                                    _DocumentItem.LotNumber = itemK.LotNumber;
                                    _DocumentItem.WeightNetPackageTotal = (itemK.WeightNetItemUnit / 1000) * itemK.Quantity;
                                    _DocumentItem.WeightGrossPackageTotal = (itemK.WeightGrossItemUnit / 1000) * itemK.Quantity;
                                    _DocumentItem.QuantityPackage = itemK.Quantity / (itemK.Quantity == 0 ? 1 : itemK.QuantityItemInStandart);
                                    _DocumentItem.PalletBarcode = itemK.PalletBarcode;
                                    //_DocumentItem.QuantityPackage += (itemK.Quantity / itemK.QuantityItemInStandart);
                                    _OrderReturn[i].TotalPackage += (itemK.Quantity / itemK.QuantityItemInStandart);
                                    _OrderReturn[i].TotalNetWeight += (itemK.Quantity * (itemK.WeightNetItemUnit / 1000));
                                    _OrderReturn[i].TotalGrossWeight += (itemK.Quantity * (itemK.WeightGrossItemUnit / 1000));


                                    //Ürün master bilgileri
                                    _DocumentItem.Code = itemK.ItemCode;
                                    _DocumentItem.Name = itemK.ItemName;
                                    _DocumentItem.ExpiryDate = itemK.ExpiryDate;
                                    _DocumentItem.PackageType = itemK.PackageType;
                                    _DocumentItem.WeightNetItemUnit = itemK.WeightNetItemUnit / 1000;
                                    _DocumentItem.WeightGrossItemUnit = itemK.WeightGrossItemUnit/ 1000;
                                    _DocumentItem.QuantityItemInStandart = itemK.QuantityItemInStandart;

                                    _DocumentItem.Quantity = itemK.Quantity;
                                    _DocumentItem.QuantityTotal = itemK.QuantityTotal;


                                    _DocumentItem.MainCompanycode = itemK.MainCompanyCode;
                                    DocumentItemList.Add(_DocumentItem);
                                }

                            }

                            _OrderReturn[i].DocumentList[j].DocumentItemList = DocumentItemList;


                        }
                    }



                    //Statüler
                    // SP den al
                    if (_OrderReturn != null)
                    {
                        List<SqlParameter> paramlistX = new List<SqlParameter>();
                        paramlistX.Add(new SqlParameter("pMainCompanyCode", pCompanyCode));
                        paramlistX.Add(new SqlParameter("pOrderType", Convert.ToInt32(pOrderType)));
                        paramlistX.Add(new SqlParameter("pOrderCode", pOrderCode.ToString()));

                        List<Status> statusList = db.Database.SqlQuery<Status>("Sp_Get_Order_Return_Status @pMainCompanyCode,@pOrderType,@pOrderCode", paramlistX.ToArray()).ToList();

                        _OrderReturn[0].StatusList = statusList;
                    }

                    //for (int i = 0; i < ORD; i++)
                    //{

                    //}
                    //foreach (var itemB in documentList)
                    //{
                    //    //Dökümanlar ( irsaliye, fotoğraf, tutanak vs )
                    //    _OrderReturn.DocumentList = new List<Document>();


                    //    _OrderReturn.StatusList = new List<Status>();
                    //}

                    //var list = db.OrderStatus.Where(x => x.order.mainCompanyCode == pCompanyCode && x.orderCode == pOrderCode).ToList();
                    //var list = db.OrderStatus.ToList();

                }

                return _OrderReturn;
            }
        }
        #endregion
    }



    public class Status
    {
        [IgnoreDataMember]
        public string OrderCode { get; set; }
        public int StatusCode { get; set; }
        public string StatusDesc { get { return (Enum.GetName(typeof(EnumOrderStatus), StatusCode)); } set { } }
        public string StatusDate { get; set; }

    }
    public class Document
    {

        public int DocumentType { get; set; }
        public string DocumentTypeDesc { get; set; }
        public string DocumentCode { get; set; }
        public List<DocumentItem> DocumentItemList { get; set; }
    }

    public class DocumentItem
    {
        public long LineCode { get; set; }
        public string MainCompanycode { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public int PackageType { get; set; }
        public double Quantity { get; set; }
        public string QuantityStr { get { return (Quantity.ToString() + " Adet"); } set { } }
        public double QuantityTotal { get; set; }
        public string QuantityTotalStr { get { return (QuantityTotal.ToString() + " Adet"); } set { } }

        public string LotNumber { get; set; }
        public string ExpiryDate { get; set; }
        public string PalletBarcode { get; set; }

        public double WeightNetItemUnit { get; set; }
        public string WeightNetItemUnitStr { get { return (WeightNetItemUnit.ToString() + " Kg"); } set { } }
        public double WeightGrossItemUnit { get; set; }
        public string WeightGrossItemUnitStr { get { return (WeightGrossItemUnit.ToString() + " Kg"); } set { } }

        public double QuantityPackage { get; set; }
        public string QuantityPackageStr { get { return (QuantityPackage.ToString() + " Paket"); } set { } }


        public double WeightNetPackageTotal { get; set; }
        public string WeightNetPackageTotalStr { get { return (WeightNetPackageTotal.ToString() + " Kg"); } set { } }
        public double WeightGrossPackageTotal { get; set; }
        public string WeightGrossPackageTotalStr { get { return (WeightGrossPackageTotal.ToString() + " Kg"); } set { } }

        public double QuantityItemInStandart { get; set; }

    }

}














//public OrderStatus()
//{
//    statuses = new List<OrderStatusTransaction>();
//}

//Order _order;
//[IgnoreDataMember]
//public Order order
//{
//    get { return _order; }
//    set { _order = value; }
//}

//EnumOrderType _orderType;
//public EnumOrderType orderType
//{
//    get { return _orderType; }
//    set { _orderType = value; }
//}

//string _orderSubType;
//[StringLength(50)]
//public string orderSubType
//{
//    get { return _orderSubType; }
//    set { _orderSubType = value; }
//}

//string _orderCode;
//[StringLength(400)]
//public string orderCode
//{
//    get { return _orderCode; }
//    set { _orderCode = value; }
//}
//public virtual List<OrderStatusTransaction> statuses { get; set; }
//public virtual List<OrderDocument> documents { get; set; }