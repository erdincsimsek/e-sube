﻿using ESube.Service.Context;
using ESube.Service.Models.Enums;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Linq;
using System.Data.Entity;
using System.Net.Http.Headers;
using System.Net.Http;
using Newtonsoft.Json;
using System.Collections.Generic;
using ESube.Service.Models.Helpers;

namespace ESube.Service.Models
{
    //[Serializable]
    [Table("Log")]
    public class Log
    {
        public Log()
        {
            MainCompanyCode = "";
            RequestBody = "";
            ResponseBody = "";
            StatusCode = EnumLogStatus.Success;
            StatusMessage = "";
            CreateDate = DateTime.Now;
            SourceType = EnumLogSource.WebService;
            StoredProcedureName = "";
            ErrorLine = 0;
            ErrorMessage = "";
        }

        long _Id;
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id
        {
            get { return _Id; }
            set { _Id = value; }

        }

        public EnumLogSource SourceType { get; set; }

        public string Source { get { return (Enum.GetName(typeof(EnumLogSource), SourceType)); } set { } }

        public string MainCompanyCode { get; set; }


        public string RequestUrl { get; set; }
        public string RequestMethod { get; set; }
        public string RequestBody { get; set; }


        public string ResponseBody { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime CreateDate { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime ResponseDate { get; set; }

        public string HostAddress { get; set; }
        public string UserAgent { get; set; }

        public string Headers { get; set; }
        public EnumLogStatus StatusCode { get; set; }
        public string Status { get { return (Enum.GetName(typeof(EnumLogStatus), StatusCode)); } set { } }
        public string StatusMessage { get; set; }


        public string StoredProcedureName { get; set; }
        public int ErrorLine { get; set; }
        public string ErrorMessage { get; set; }

        public Log InsertWebApiLog(HttpRequestMessage pRequest,string pCompanyCode,  object pRequestBody, HttpContext pHost)
        {
            //Çalışmıyor
            var responseHeadersString = new StringBuilder();
            if (pRequest != null)
            {
                foreach (var header in pRequest.Headers)
                {
                    responseHeadersString.Append($"{header.Key}: {String.Join(", ", header.Value)}  {Environment.NewLine}");
                }
            }

            //Çalışmıyor

            Log _Log = new Log();
            _Log.Headers = responseHeadersString.ToString();

            _Log.MainCompanyCode = pCompanyCode;
            _Log.RequestUrl = pRequest == null ? null : pRequest.RequestUri.ToString();
            _Log.RequestMethod = pRequest == null ? null : pRequest.Method.ToString();
            _Log.RequestBody = JsonConvert.SerializeObject(pRequestBody).ToString();
            _Log.HostAddress = pHost != null ? pHost.Request.UserHostAddress : "0.0.0.0";
            _Log.UserAgent = pRequest == null ? null : pRequest.Headers.UserAgent.ToString();

            using (ESubeContext db = new ESubeContext())
            {
                db.Log.Add(_Log);
                db.SaveChanges();
            }


            return _Log;

        }

        public void UpdateWebApiLog(Log pLog)
        {

            try
            {

                using (ESubeContext _db = new ESubeContext())
                {
                    //Log _Log = _db.Log.Where(x=> x.Id == pLog.Id).FirstOrDefault();
                    pLog.ResponseDate = DateTime.Now;
                    _db.Entry(pLog).State = EntityState.Modified;
                    _db.SaveChanges();

                }
            }
            catch (Exception e)
            { }

        }

        public  ReturnObject  GetListLog(string pSourceType ="",int pLogId = 0, string pCompanyCode= "",string  pSearchText="", EnumSyncStatus pStatusCode=0)
        {

            ReturnObject retObj = new ReturnObject();


            try
            {

                using (ESubeContext _db = new ESubeContext())
                {
                    var LogList = _db.Log.Where(x=> 

                            (pSourceType == "" ? 1 == 1 : x.Source == pSourceType)
                        &&  (pLogId == 0 ? 1 == 1 : x.Id == pLogId)
                        &&  (string.IsNullOrEmpty(pCompanyCode) ? 1 == 1 : x.MainCompanyCode == pCompanyCode)
                        &&  (string.IsNullOrEmpty(pSearchText) ? 1 == 1 : (x.RequestBody.Contains(pSearchText) || x.ResponseBody.Contains(pSearchText)))
                        &&  (pStatusCode  == 0 ? 1 == 1 : (int)x.StatusCode == (int)pStatusCode)

                    ).OrderByDescending(x => x.CreateDate).Take(1000).ToList();

                    retObj.Result = true;
                    retObj.Data = LogList;

                }
            }
            catch (Exception e)
            {

                retObj.Result = false;
                retObj.Data = null;
                retObj.ResultMessage = "ERROR: "+e.Message.ToString() +" - "+e.InnerException.Message.ToString();
                //using (ESubeContext _db = new ESubeContext())
                //{
                //    Log _Log = new Log();
                //    _Log.StatusCode = EnumLogStatus.Error;
                //    _Log.StatusMessage = "Logları Çekerken Hata Oluştu : "+e.Message.ToString();
                //    _db.Log.Add(_Log);
                //    _db.SaveChanges();
                //}
            }

            return retObj;

        }


        public void LogErrorMessage (string ErrorMessage, int ErrorLine)
        {
            Log log = new Log();
            log.ErrorMessage = ErrorMessage;
            log.ErrorLine = ErrorLine;
            using (ESubeContext db = new ESubeContext())
            {
                db.Log.Add(log);
                db.SaveChanges();
            }
        }

    }

}