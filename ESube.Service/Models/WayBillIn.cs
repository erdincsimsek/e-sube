﻿using ESube.Service.Context;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ESube.Service.Models
{
    [Table("vw_WayBillGoodsIn")]
    public class WayBillGoodsIn
    {
        [Key]
        public long Id { get; set; }
        public string Company { get; set; }
        public string CompanyCode { get; set; }

        public string ShipmentCode { get; set; }
        public string WayBillCode { get; set; }
        public string OrderCode { get; set; }

        public string OrderStatus { get; set; }
        public string Supplier { get; set; }

        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public string PackageType { get; set; }
        public  string Quantity { get; set; }
        public string LotNumber { get; set; }

        //[Column(TypeName = "datetime2")]
        public string ReceiptDate { get; set; }
        public string MailBody { get; set; }
        public string MailSubject { get; set; }
        public string ReceiverMailAddresses { get; set; }

        public List<WayBillGoodsIn> GetList(string pCompanyCode,  string pOrderCode)
        {
            using (var db = new ESubeContext())
            {
                //var list = db.WayBillGoodsIn.Where(x => x.CompanyCode == pCompanyCode && x.OrderCode == pOrderCode).ToList();
                return null;
            }
        }
    }

}