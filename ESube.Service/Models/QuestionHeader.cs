﻿using ESube.Service.Context;
using ESube.Service.Models.Enums;
using ESube.Service.Models.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.Linq;
using ESube.Service.Helpers;
using System.Data.Entity;



namespace ESube.Service.Models
{
    [Table("QuestionHeader")]
    public class QuestionHeader : BaseEntityClass
    {

        public QuestionHeader(){
            QuestionList = new List<Question>();
        }

        public string Name { get; set; }
        public List<Question> QuestionList { get; set; }


        public ReturnObject GetList(string pMainCompanyCode)
        {

            ReturnObject retObj = new ReturnObject();

            try
            {
                using (ESubeContext _db = new ESubeContext())
                {
                    var list = _db.QuestionHeader.Include(x => x.QuestionList).ToList();

                    foreach (var itemA in list)
                    {
                        foreach (var itemB in itemA.QuestionList)
                        {
                            itemB._QuestionAnswer = _db.QuestionAnswer.Where(x=>x._Question.Id ==itemB.Id && x._MainCompany.Code == pMainCompanyCode && x.IsDeleted != true).ToList();

                        }

                    }

                    retObj.Result = true;
                    retObj.Data = list;

                }
            }
            catch (Exception e)
            {

                retObj.Result = false;
                retObj.Data = null;
                retObj.ResultMessage = "ERROR: " + e.Message.ToString() + " - " + e.InnerException.Message.ToString();
            }

            return retObj;

        }
    }
}