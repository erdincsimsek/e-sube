﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ESube.Service.Models.SimpleyDeliver
{
    [Table("PreOrderOffer")]
    public class PreOrderOffer: DLReceivedBaseObject
    {
        public string AnswerById { get; set; }
        public string AnswerDate { get; set; }
        public string StatusCode { get; set; }
        public string CurrencyCode { get; set; }
        public string Comments { get; set; }
        public string Notes { get; set; }
        public string TenderPriceType1 { get; set; }
        public string TenderPrice1 { get; set; }
        public string TenderPriceType2 { get; set; }
        public string TenderPrice2 { get; set; }
        public string TenderPriceType3 { get; set; }
        public string TenderPrice3 { get; set; }
        public string TenderPriceType4 { get; set; }
        public string TenderPrice4 { get; set; }
        public string TenderPriceType5 { get; set; }
        public string TenderPrice5 { get; set; }
        public string TenderPriceType6 { get; set; }
        public string TenderPrice6 { get; set; }
        public string TenderPriceType7 { get; set; }
        public string TenderPrice7 { get; set; }
        public string TenderPriceType8 { get; set; }
        public string TenderPrice8 { get; set; }
        public string Commission { get; set; }
        public string DriverName { get; set; }
        public string DriverExternalSystemId { get; set; }
        public string DriverCharachterName { get; set; }
        public string DriverMobileNR { get; set; }
        public string CarrierName { get; set; }
        public string CarrierExternalSystemId { get; set; }
    }
}