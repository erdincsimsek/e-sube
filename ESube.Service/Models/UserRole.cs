﻿using ESube.Service.Context;
using ESube.Service.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ESube.Service.Models
{
    [Table("UserRole")]
    public class UserRole : BaseEntityClass
    {

      public long UserId{ get; set; }
      public long RoleId { get; set; }

     #region FUNCTIONS
        public string  GetRoleListStrByUserId(long pUserId)
        {
            string roleListStr = "";

            using (ESubeContext db = new ESubeContext())
            {
                //var x = db.UserRole.Where(y=> y.UserId == pUserId && IsDeleted == false && IsActive).ToList();
                var RoleCodes = (from m in db.UserRole
                                  join d1 in db.Role on m.RoleId equals d1.Id
                                  where m.UserId == pUserId
                                  select new
                                  {
                                      RoleCode = d1.Code
                                  });

                foreach (var item in RoleCodes)
                {
                    roleListStr+= item.RoleCode.ToString()+"#";
                }

            }

            return roleListStr;
        }
     #endregion
    }
}