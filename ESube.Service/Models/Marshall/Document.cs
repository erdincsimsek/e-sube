﻿using ESube.Service.Context;
using ESube.Service.Helpers;
using ESube.Service.Models.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ESube.Service.Models
{
    public class Document: BaseEntityClass
    {
       // public string MainCompanyCode { get; set; }
        public int MDOC_ID { get; set; }
        public string MDOC_NAME { get; set; }
        public string MDOC_MESSAGETYPE { get; set; }
        public string MDOC_IDOCNUMBER { get; set; }
        public DateTime MDOC_ENTRYDATETIME { get; set; }
        public DateTime MDOC_FROMSYSTEM { get; set; }
        public string MDOC_TOSYSTEM { get; set; }
        public string MDOC_ERRORTEXT { get; set; }
        public string MDOC_REFERANCE { get; set; }
        public int MDOC_TYPE { get; set; }

        public List<Document> GetListAll()
        {
            List<Document> Document = new List<Document>(); ;

            using (ESubeContext db = new ESubeContext())
            {
                Document = (from marshall in db.Document select marshall).ToList();

            }

            return Document;
        }
    }
}