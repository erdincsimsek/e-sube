﻿using ESube.Service.Context;
using ESube.Service.Models.Enums;
using ESube.Service.Models.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.Linq;

namespace ESube.Service.Models
{

    /// <summary>
    /// Her Müşteri İçin Ürün Kodu
    /// </summary>
    [Table("ItemEachCustomer")]
    public class ItemEachCustomer : BaseIntegrationClass
    {
        public ItemEachCustomer()
        {

        }
        [IgnoreDataMember]
        public Item Item { get; set; }


        /// <summary>
        /// Ana firma kodu
        /// </summary>
        public string MainCompanyCode { get; set; }

        /// <summary>
        /// Müşterinin Kodu
        /// </summary>
        public string CustomerCode { get; set; }
        /// <summary>
        /// Müşterinin Ünvanı
        /// </summary>
        public string CustomerTitle { get; set; }
        /// <summary>
        /// Ürün Kodu
        /// </summary>
        public string ItemCode { get; set; }

        /// <summary>
        /// Müşteriye Göre Ürün Kodu
        /// </summary>
        public string CustItemCode { get; set; }

        
    }
}