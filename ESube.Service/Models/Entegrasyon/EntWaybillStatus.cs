﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ESube.Service.Models.Entegrasyon
{
    public class EntWaybillStatus
    {
        public string sirket_kodu { get; set; }
        public string HKod { get; set; }
        public string DocumentId { get; set; }
        public string ItemId { get; set; }
        public int Quantity { get; set; }
        public string WarehouseCode { get; set; }
        public string integrationCode { get; set; }
        public string integrationDate { get; set; }
        public int  integrationStatus { get; set; }
        public string integrationStatusDesc { get; set; }
        public string orderCode { get; set; }
        public int OrderStatus { get; set; }
        public int domaintype { get; set; }
    }
}
