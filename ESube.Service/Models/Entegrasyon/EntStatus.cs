﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ESube.Service.Models.Entegrasyon
{
    public class EntStatus
    {
        public string OrderCode { get; set; }
        public string StatusCode { get; set; }
        public string StatusDate { get; set; }
    }
}