﻿using ESube.Service.Context;
using ESube.Service.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ESube.Service.Models
{
    [Table("MainCompany")]
    public class MainCompany 
    {
        public MainCompany()
        {
            Description = "-";
            Status = true;
        }
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [StringLength(7)]
        public string Code { get; set; }
        public string Name { get; set; }

        //[Required(ErrorMessage = "Required Description")]
        public string Description { get; set; }

        //[Required(ErrorMessage = "Required PersonInCharge")]
        public string PersonInCharge { get; set; }

        //[Required(ErrorMessage = "Required EmailID")]
        public string EmailAddress { get; set; }

        //[Required(ErrorMessage = "Required Status")]
        public bool Status { get; set; }


        public MainCompany GetIByUserId(long pUserId)
        {
            MainCompany _Company = new MainCompany();;

            using (ESubeContext db = new ESubeContext())
            {
                long CompanyId = (from x in db.User
                               where x.Id == pUserId
                               select x.CompanyId).FirstOrDefault();


                _Company = (from companies in db.MainCompany
                            where companies.Id == CompanyId
                                select companies).FirstOrDefault();

            }

            return _Company;
        }

        public List<MainCompany> GetListAll()
        {
            List<MainCompany> _CompanyList = new List<MainCompany>(); ;

            using (ESubeContext db = new ESubeContext())
            {
                _CompanyList = (from companies in db.MainCompany select companies).OrderBy(x=>x.Name) .ToList();

            }

            return _CompanyList;
        }



    }
}