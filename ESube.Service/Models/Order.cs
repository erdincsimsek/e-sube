﻿using ESube.Service.Context;
using ESube.Service.Models.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.ServiceModel;
//using static ESube.Service.Models.DailyStock;

namespace ESube.Service.Models
{
    /// <summary>
    /// Sipariş nesnesi
    /// </summary>
    //[Table("Order")]
    public class Order : DLReceivedBaseObject
    {
        public Order()
        {
            Lines = new List<OrderLine>();
        }

        /// <summary>
        /// Firma kodu
        /// </summary>
       // [StringLength(25)]
        public string mainCompanyCode { get; set; }

        /// <summary>
        /// Depo kodu
        /// </summary>
       // [StringLength(25)]
        public string companyBranchCode { get; set; }

        /// <summary>
        /// Sipariş numarası
        /// </summary>
       // [StringLength(50)]
        public string orderCode { get; set; }


        /// <summary>
        /// Sipariş referans numarası
        /// </summary>
        [StringLength(50)]
        public string orderReferenceCode { get; set; }


        /// <summary>
        /// İrsaliye numarası. Giriş siparişi için.
        /// </summary>
      //  [StringLength(50)]
        public string documentCode { get; set; }

        /// <summary>
        /// Sipariş tarihi
        /// </summary>
       // [StringLength(20)]
        public string orderDate { get; set; }

        /// <summary>
        /// Siparişin planlanan giriş-çıkış tarihi
        /// </summary>
        //[StringLength(20)]
        public string plannedDate { get; set; }

        /// <summary>
        /// Sipariş önceliği
        /// </summary>
       // [StringLength(2)]
        public string orderPriority { get; set; }

        /// <summary>
        /// Sipariş tipi
        /// </summary>
        public EnumOrderType orderType { get; set; }

        /// <summary>
        /// Siparişi yükleme tipi
        /// </summary>
        public EnumLoadingType loadingType { get; set; }

        /// <summary>
        /// Müşteri kodu
        /// </summary>
        //[StringLength(50)]
        public string companyCode { get; set; }

        /// <summary>
        /// Müşteri adı
        /// </summary>
        //[StringLength(500)]
        public string companyName { get; set; }

        /// <summary>
        /// Müşteri GLN numarası
        /// </summary>
        //[StringLength(50)]
        public string companyGLN { get; set; }

        /// <summary>
        /// Müşteri vergi numarası
        /// </summary>
        //[StringLength(50)]
        public string companyTaxNumber { get; set; }

        /// <summary>
        /// Firma vergi dairesi adı
        /// </summary>
        //[StringLength(100)]
        public string companyTaxDepartmentName { get; set; }

        /// <summary>
        /// Firma il kodu
        /// </summary>
       // [StringLength(50)]
        public string companyCityCode { get; set; }

        /// <summary>
        /// Firma il adı
        /// </summary>
        //[StringLength(100)]
        public string companyCityName { get; set; }

        /// <summary>
        /// Firma ilçe kodu
        /// </summary>
        //[StringLength(50)]
        public string companyTownCode { get; set; }

        /// <summary>
        /// Firma ilçe adı
        /// </summary>
       // [StringLength(100)]
        public string companyTownName { get; set; }

        /// <summary>
        /// Firma mahalle adı
        /// </summary>
        //[StringLength(50)]
        public string companyDistrictName { get; set; }

        /// <summary>
        /// Firma açık adresi
        /// </summary>
       // [StringLength(500)]
        public string companyAddressText { get; set; }

        /// <summary>
        /// e-irsaliye mükellefi mi?(Y: yes , N: No)
        /// </summary>
        [StringLength(1)]
        public string IsDispatcher { get; set; }

        /// <summary>
        /// Telefon numarası
        /// </summary>
       // [StringLength(50)]
        public string companyPhone { get; set; }

        /// <summary>
        /// Posta kodu
        /// </summary>
        //[StringLength(10)]
        public string companyPostCode { get; set; }

        /// <summary>
        /// Kontak kişi kodu
        /// </summary>
       // [StringLength(25)]
        public string companyContactCode { get; set; }

        /// <summary>
        /// Kontak kişi adı
        /// </summary>
       // [StringLength(50)]
        public string companyContactFirstName { get; set; }

        /// <summary>
        /// Kontak kişi soyadı
        /// </summary>
       // [StringLength(50)]
        public string companyContactLastName { get; set; }

        /// <summary>
        /// Kontak telefon numarası
        /// </summary>
        //[StringLength(50)]
        public string companyContactPhoneNumber { get; set; }

        /// <summary>
        /// Kontak cep telefonu
        /// </summary>
       // [StringLength(50)]
        public string companyContactMobilePhoneNumber { get; set; }

        /// <summary>
        /// Kontak mail adresi
        /// </summary>
       // [StringLength(250)]
        public string companyContactMail { get; set; }

        /// <summary>
        /// Sipariş alt tipi
        /// </summary>
        public EnumOrderTypeReason orderTypeReason { get; set; }

        /// <summary>
        /// Sevk numarası
        /// </summary>
        [StringLength(400)]
        public string shipmentCode { get; set; }

        /// <summary>
        /// Entegrasyon aktarım tarihi
        /// </summary>
        [IgnoreDataMember]
        public DateTime integrationDate { get; set; }
        /// <summary>
        /// Sipariş ürün detayı
        /// </summary>
        public virtual List<OrderLine> Lines { get; set; }


    }

    /// <summary>
    /// Sipariş satırları
    /// </summary>
    //[Table("OrderLine")]
    public class OrderLine
    {
        public OrderLine() { }

        [IgnoreDataMember]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long id { get; set; }

        [IgnoreDataMember]
        public string status { get; set; }

        [IgnoreDataMember]
        [StringLength(5000)]
        public string statusDescription { get; set; }
        [IgnoreDataMember]
        public DateTime? createDate { get; set; }
        [IgnoreDataMember]
        public string integrationCode { get; set; }
        public string freeText { get; set; }
        [IgnoreDataMember]
        public Order order { get; set; }
        /// <summary>
        /// Satır numarası
        /// </summary>
        [StringLength(400)]
        public string lineCode { get; set; }
        /// <summary>
        /// Ürün kodu
        /// </summary>
        [StringLength(400)]
        public string itemCode { get; set; }

        /// <summary>
        /// Ürün açıklaması
        /// </summary>
        [StringLength(400)]
        public string itemDescription { get; set; }
        /// <summary>
        /// Miktar
        /// </summary>
        public double? quantity { get; set; }
        /// <summary>
        /// Ladometer : Ürünün treylerin içinde kapladığı her 1 metre, 1 LDM’ye tekabül eder. Bir treylerin uzunluğu 13,6, eni 2,40, yüksekliği 2,40 metredir. Bir treyler 13,6 LDM’dir. 1 LDM = 1750 kg
        /// </summary>
        public double ldm { get; set; }
        /// <summary>
        /// Hacim
        /// </summary>
        public double? volume { get; set; }
        /// <summary>
        /// Brüt ağırlık(gram)
        /// </summary>
        public double? grossWeight { get; set; }
        /// <summary>
        /// Net ağırlık (gram)
        /// </summary>
        public double? netWeight { get; set; }
        /// <summary>
        /// Taşıma birimi : Palet, Paket,Adet
        /// </summary>
        public EnumContainerType containerType { get; set; }
        /// <summary>
        /// Lot numarası
        /// </summary>
        [StringLength(400)]
        public string lotCode { get; set; }
        /// <summary>
        /// Üretim tarihi
        /// </summary>
        [StringLength(400)]
        public string productionDate { get; set; }
        /// <summary>
        /// Son kullanma tarihi
        /// </summary>
        [StringLength(400)]
        public string expireDate { get; set; }
        /// <summary>
        /// ADR etiketi
        /// </summary>
        [StringLength(400)]
        public string adrTypeLabel { get; set; }
        /// <summary>
        /// Barkod
        /// </summary>
        [StringLength(400)]
        public string barcode { get; set; }
        /// <summary>
        /// Ürün GTIN numarası. Küresel Ticari Ürün Numarası (Global Trade Item Number)
        /// </summary>
        [StringLength(400)]
        public string gtin { get; set; }
        /// <summary>
        /// Paket içi miktar
        /// </summary>
        public long packageCapacity { get; set; }
        /// <summary>
        /// Palet kapasitesi
        /// </summary>
        public long palleteCapacity { get; set; }
        /// <summary>
        /// Birim Tipi
        /// </summary>
        public EnumUnitType unitType { get; set; }
        /// <summary>
        /// Ürün grup kodu
        /// </summary>
        public string itemGroupCode { get; set; }
        /// <summary>
        /// Ürün palet numarası (SSCC)
        /// </summary>
        public string sscc { get; set; }
        /// <summary>
        /// Son kullanma tarihi kontrolü zorunlu mu?
        /// </summary>
        [StringLength(5)]
        public string expireDateRequired { get; set; }
        /// <summary>
        /// Raf ömrü
        /// </summary>
        public int shelfLife { get; set; }
        /// <summary>
        /// Paket barkodu
        /// </summary>
        [StringLength(400)]
        public string packageBarcode { get; set; }
        /// <summary>
        /// ADR numarası
        /// </summary>
        [StringLength(40)]
        public string adrUnNumber { get; set; }
        /// <summary>
        /// ADR ürün adı
        /// </summary>
        [StringLength(200)]
        public string adrItemName { get; set; }
        /// <summary>
        /// ADR ana sınıf
        /// </summary>
        [StringLength(20)]
        public string adrMainRiskClass { get; set; }
        /// <summary>
        /// ADR ek risk sınıf
        /// </summary>
        [StringLength(20)]
        public string adrAdditionalRisk { get; set; }
        /// <summary>
        /// ADR paket grubu
        /// </summary>
        [StringLength(20)]
        public string adrPackageGroup { get; set; }
        /// <summary>
        /// ADR tünel kısıt kodu
        /// </summary>
        [StringLength(20)]
        public string adrTunnelRestrictionCode { get; set; }

    }

    //[Table("OrderStatus")]
    public class OrderStatus : DLSentBaseObject
    {
        public OrderStatus()
        {
            statuses = new List<OrderStatusTransaction>();
        }

        Order _order;
        [IgnoreDataMember]
        public Order order
        {
            get { return _order; }
            set { _order = value; }
        }

        EnumOrderType _orderType;
        public EnumOrderType orderType
        {
            get { return _orderType; }
            set { _orderType = value; }
        }

        string _orderSubType;
        [StringLength(50)]
        public string orderSubType
        {
            get { return _orderSubType; }
            set { _orderSubType = value; }
        }

        string _orderCode;
        [StringLength(400)]
        public string orderCode
        {
            get { return _orderCode; }
            set { _orderCode = value; }
        }
        public virtual List<OrderStatusTransaction> statuses { get; set; }
    }

    //[Table("OrderStatusTransaction")]
    public class OrderStatusTransaction : DLSentBaseObject
    {
        public OrderStatusTransaction() { }

        OrderStatus _orderStatus;
        [IgnoreDataMember]
        public virtual OrderStatus orderStatus
        {
            get { return _orderStatus; }
            set { _orderStatus = value; }
        }

        string _subType;
        [StringLength(50)]
        public string subType
        {
            get { return _subType; }
            set { _subType = value; }
        }

        string _orderSubType;
        [StringLength(50)]
        public string orderSubType
        {
            get { return _orderSubType; }
            set { _orderSubType = value; }
        }


        EnumOrderType _orderType;
        [IgnoreDataMember]
        public EnumOrderType orderType
        {
            get { return _orderType; }
            set { _orderType = value; }
        }

        string _orderCode;
        [IgnoreDataMember]
        [StringLength(50)]
        public string orderCode
        {
            get { return _orderCode; }
            set { _orderCode = value; }
        }

        string _lineCode;
        [StringLength(50)]
        public string lineCode
        {
            get { return _lineCode; }
            set { _lineCode = value; }
        }

        string _statusName;
        [StringLength(50)]
        public string statusName
        {
            get { return _statusName; }
            set { _statusName = value; }
        }

        DateTime _statusDate;
        //[StringLength(50)]
        public DateTime statusDate
        {
            get { return _statusDate; }
            set { _statusDate = value; }
        }

        string _changeUser;
        [StringLength(50)]
        [IgnoreDataMember]
        public string changeUser
        {
            get { return _changeUser; }
            set { _changeUser = value; }
        }
    }

    public class OrderResponse : DLBaseObject
    {
        Order _order;
        [IgnoreDataMember]
        public virtual Order order { get; set; }
        public string orderCode { get; set; }
        public string orderDate
        {
            get { return order.orderDate; }

        }

        public string stockDate { get; set; }
        public string startedDate { get; set; }
        public string officeExecutedDate { get; set; }
        public string closedDate { get; set; }

    }
    /// <summary>
    /// Sevk edilen sipariş geri dönüş objesi
    /// </summary>
    public class ShippedOrderResponse
    {
        public ShippedOrderResponse()
        {
            Lines = new List<ShippedOrderLine>();
        }
        /// <summary>
        /// Firma kodu
        /// </summary>
        [StringLength(25)]
        public string mainCompanyCode { get; set; }

        /// <summary>
        /// Depo kodu
        /// </summary>
        [StringLength(25)]
        public string companyBranchCode { get; set; }

        /// <summary>
        /// Sevk İrsaliyesi Numarası
        /// </summary>
        [StringLength(50)]
        public string despatchID { get; set; }
        /// <summary>
        /// Sevk İrsaliyesinin Asıl veya Suret Bilgisi Asıl ise false , kopya ise true
        /// </summary>
        [StringLength(10)]
        public string CopyIndicator { get; set; }
        /// <summary>
        /// Sevk İrsaliyesinin Evrensel Tekliğini Sağlayan Numara (ETTN)
        /// </summary>
        [StringLength(50)]
        public string UUID { get; set; }
        /// <summary>
        ///  İrsaliye Düzenleme Tarihi  YYYY-MM-DD
        /// </summary>
        [StringLength(10)]
        public string IssueDate { get; set; }
        /// <summary>
        /// İrsaliye Düzenleme zamanı  HH:MM:SS
        /// </summary>
        [StringLength(10)]
        public string IssueTime { get; set; }
        /// <summary>
        /// Sevk İrsaliyesinin Tip Kodu (SEVK/MATBUDAN)
        /// </summary>
        [StringLength(10)]
        public string DespatchAdviceTypeCode { get; set; }
        /// <summary>
        /// Sevk İrsaliyesi ile ilgili genel not
        /// </summary>
        [StringLength(200)]
        public string Note { get; set; }
        /// <summary>
        /// İrsaliyedeki satır sayısı
        /// </summary>
        [StringLength(5)]
        public int LineCountNumeric { get; set; }

        // LineCountNumeric Sevk İrsaliyesi Kalem Sayısı
        /// <summary>
        /// Sipariş ürün detayı
        /// </summary>
        public virtual List<ShippedOrderLine> Lines { get; set; }
        /// <summary>
        /// Sipariş Bilgileri
        /// </summary>
        public virtual List<ShippedOrderReference> OrderReference { get; set; }
    }
    /// <summary>
    /// Sipariş bilgileri
    /// </summary>
    public class ShippedOrderReference
    {
        /// <summary>
        /// 
        /// </summary>
        [StringLength(50)]
        public string ID { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [StringLength(50)]
        public string SalesOrderID { get; set; }
        /// <summary>
        ///  İrsaliye Düzenleme Tarihi  YYYY-MM-DD
        /// </summary>
        [StringLength(10)]
        public string IssueDate { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [StringLength(10)]
        public string OrderTypeCode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [StringLength(10)]
        public string DocumentReference { get; set; }

    }

    public class AdditionalDocumentReference
    {
        /// <summary>
        /// 
        /// </summary>
        [StringLength(50)]
        public string ID { get; set; }
        /// <summary>
        ///  İrsaliye Düzenleme Tarihi  YYYY-MM-DD
        /// </summary>
        [StringLength(10)]
        public string IssueDate { get; set; }

        /// <summary>
        /// Dosya Bilgileri
        /// </summary>
        public virtual List<FileAttachment> Attachment { get; set; }
    }
    /// <summary>
    /// İrsaliyelerin dosya halleri.
    /// </summary>
    public class FileAttachment
    {
        /// <summary>
        /// İrsaliyenin dosya hali
        /// </summary>
        public string EmbeddedDocumentBinaryObject { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class DespatchSupplierParty
    {
        //WebsiteURI
        //    PartyIdentification
        //PartyName
        //    PostalAddress
        //PartyTaxScheme
        //    Contact
    }

    /// <summary>
    /// sipariş detatyları
    /// </summary>
    public class ShippedOrderLine
    {
        public ShippedOrderLine() { }

        [IgnoreDataMember]
        public ShippedOrderResponse order { get; set; }

        /// <summary>
        /// İrsaliye Numarası
        /// </summary>
        [StringLength(20)]
        public string dispatchNumber { get; set; }

        /// <summary>
        /// İrsaliye basım tarihi(YYYY-MM-DD)
        /// </summary>
        [StringLength(10)]
        public string issueDate { get; set; }

        /// <summary>
        /// İrsaliye basım zamanı (HH:MM:SS)
        /// </summary>
        [StringLength(10)]
        public string issueTime { get; set; }

        /// <summary>
        /// İrsaliye tipi
        /// </summary>
        [StringLength(50)]
        public string dispatchType { get; set; }

        /// <summary>
        /// İrsaliye dosya adı
        /// </summary>
        [StringLength(200)]
        public string dispatchImageName { get; set; }

        /// <summary>
        /// Palet barkodu
        /// </summary>
        [StringLength(50)]
        public string palletBarcode { get; set; }

        /// <summary>
        /// Satır sırası
        /// </summary>
        [StringLength(10)]
        public string lineCode { get; set; }

        /// <summary>
        /// Ürün Kodu
        /// </summary>
        [StringLength(50)]
        public string itemCode { get; set; }

        /// <summary>
        /// Ürün adı
        /// </summary>
        [StringLength(200)]
        public string itemName { get; set; }

        /// <summary>
        /// Lot numarası
        /// </summary>
        [StringLength(20)]
        public string lotCode { get; set; }

        /// <summary>
        /// Lot numarası (YYYY-MM-DD)
        /// </summary>
        [StringLength(10)]
        public string expiryDate { get; set; }

        /// <summary>
        /// Miktar
        /// </summary>
        [StringLength(200)]
        public string quantity { get; set; }

        /// <summary>
        /// Çıkış birimi (Adet,Koli,Palet vb)
        /// </summary>
        [StringLength(200)]
        public string packageType { get; set; }

        /// <summary>
        /// Çıkış birimi (Adet,Koli,Palet vb)
        /// </summary>
        [StringLength(10)]
        public string grossWeight { get; set; }
    }




    [ServiceContract]
    public interface IOrder
    {
        [OperationContract]
        string sCreateOrder(Order order);
        [OperationContract]
        string deleteOrder(string mainCompanyCode, string companyBranchCode, string orderCode);
        [OperationContract]
        string createItem(Item item);
        [OperationContract]
        string sendStockSnapshot(StockObject stock);

    }

    public class orderStatus
    {
        string _status;
        public string status
        {
            get { return _status; }
            set { _status = value; }
        }

        DateTime _date;
        public DateTime date
        {
            get { return _date; }
            set { _date = value; }
        }
    }

    /// <summary>
    /// Sevk edilen sipariş nesnesi
    /// </summary>
    [Table("vw_shippedOrder")]
    public class ShippedOrderVW
    {
        public ShippedOrderVW() { }
        long _rowNumber;
        [Key]
        public long rowNumber
        {
            get { return _rowNumber; }
            set { _rowNumber = value; }
        }
        /// <summary>
        /// Firma kodu
        /// </summary>
        [StringLength(25)]
        public string mainCompanyCode { get; set; }

        /// <summary>
        /// Depo kodu
        /// </summary>
        [StringLength(25)]
        public string companyBranchCode { get; set; }

        /// <summary>
        /// Nakliye(sevk) kodu
        /// </summary>
        [StringLength(50)]
        public string shipmentCode { get; set; }

        /// <summary>
        /// Araç Plakası
        /// </summary>
        [StringLength(25)]
        public string plate { get; set; }

        /// <summary>
        /// Araç sürücüsü
        /// </summary>
        [StringLength(100)]
        public string driverName { get; set; }

        /// <summary>
        /// Araç sürücüsü TC no
        /// </summary>
        [StringLength(20)]
        public string driverNationalityID { get; set; }

        /// <summary>
        /// Sipariş tipi (1 : Mal kabul , -1 : Depo Çıkış)
        /// </summary>
        [StringLength(10)]
        public string orderType { get; set; }

        /// <summary>
        /// Sipariş referans Numarası
        /// </summary>
        [StringLength(50)]
        public string orderRefNumber { get; set; }

        /// <summary>
        /// Sipariş Numarası
        /// </summary>
        [StringLength(50)]
        public string orderNumber { get; set; }

        /// <summary>
        /// Sipariş tarihi
        /// </summary>
        [StringLength(10)]
        public string orderDate { get; set; }

        /// <summary>
        /// İrsaliye Numarası
        /// </summary>
        [StringLength(20)]
        public string dispatchNumber { get; set; }

        /// <summary>
        /// İrsaliye basım tarihi(YYYY-MM-DD)
        /// </summary>
        [StringLength(10)]
        public string issueDate { get; set; }

        /// <summary>
        /// İrsaliye basım zamanı (HH:MM:SS)
        /// </summary>
        [StringLength(10)]
        public string issueTime { get; set; }

        /// <summary>
        /// İrsaliye tipi
        /// </summary>
        [StringLength(50)]
        public string dispatchProfileID { get; set; }

        /// <summary>
        /// İrsaliye dosya adı
        /// </summary>
        [StringLength(200)]
        public string dispatchImageName { get; set; }

        /// <summary>
        /// Palet barkodu
        /// </summary>
        [StringLength(50)]
        public string palletBarcode { get; set; }

        /// <summary>
        /// Satır sırası
        /// </summary>
        [StringLength(10)]
        public string lineCode { get; set; }

        /// <summary>
        /// Ürün Kodu
        /// </summary>
        [StringLength(50)]
        public string itemCode { get; set; }

        /// <summary>
        /// Ürün adı
        /// </summary>
        [StringLength(200)]
        public string itemName { get; set; }

        /// <summary>
        /// Lot numarası
        /// </summary>
        [StringLength(20)]
        public string lotCode { get; set; }

        /// <summary>
        /// Lot numarası (YYYY-MM-DD)
        /// </summary>
        [StringLength(10)]
        public string expiryDate { get; set; }

        /// <summary>
        /// Miktar
        /// </summary>
        [StringLength(200)]
        public string quantity { get; set; }

        /// <summary>
        /// Çıkış birimi (Adet,Koli,Palet vb)
        /// </summary>
        [StringLength(200)]
        public string packageType { get; set; }

        /// <summary>
        /// Çıkış birimi (Adet,Koli,Palet vb)
        /// </summary>
        [StringLength(10)]
        public string grossWeight { get; set; }

    }
}


