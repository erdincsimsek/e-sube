﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.Web.Mvc;

namespace ESube.Service.Models
{

    public abstract class DLReceivedBaseObject : DLBaseObject
    {

    }

    public abstract class DLSentBaseObject : DLBaseObject
    {
        string _integrationResponseCode;
        [IgnoreDataMember]
        [StringLength(50)]
        public string integrationResponseCode
        {
            get { return _integrationResponseCode; }
            set { _integrationResponseCode = value; }
        }

        string _integrationResponseMessage;
        [IgnoreDataMember]
        [StringLength(50)]
        public string integrationResponseMessage
        {
            get { return _integrationResponseMessage; }
            set { _integrationResponseMessage = value; }
        }

        DateTime _integrationDate;
        [IgnoreDataMember]
        public DateTime integrationDate
        {
            get { return _integrationDate; }
            set { _integrationDate = value; }
        }

        string _integrationData;
        [IgnoreDataMember]
        public string integrationData
        {
            get { return _integrationData; }
            set { _integrationData = value; }
        }

        string _IntegrationErrorText;
        [IgnoreDataMember]
        public string IntegrationErrorText
        {
            get { return _IntegrationErrorText; }
            set { _IntegrationErrorText = value; }
        }
    }

    public abstract class DLBaseObject
    {
        long _id;
        [IgnoreDataMember]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long id
        {
            get { return _id; }
            set { _id = value; }
        }

        string _status;
        [IgnoreDataMember]
        public string status
        {
            get { return _status; }
            set { _status = value; }
        }

        string _statusDescription;
        [IgnoreDataMember]
        [StringLength(5000)]
        public string statusDescription
        {
            get { return _statusDescription; }
            set { _statusDescription = value; }
        }

        DateTime? _createDate;
        [IgnoreDataMember]
        public DateTime? createDate
        {
            get { return _createDate; }
            set { _createDate = value; }
        }

        string _integrationCode;
        [IgnoreDataMember]
        public string integrationCode
        {
            get { return _integrationCode; }
            set { _integrationCode = value; }
        }

        string _freeText;
        public string freeText
        {
            get { return _freeText; }
            set { _freeText = value; }
        }
    }

}