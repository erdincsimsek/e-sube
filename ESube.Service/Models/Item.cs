﻿using ESube.Service.Context;
using ESube.Service.Models.Enums;
using ESube.Service.Models.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.Linq;


namespace ESube.Service.Models
{
    /// <summary>
    /// Ürünler
    /// </summary>
    [Table("Item")]
    public class Item: BaseIntegrationClass
    {
        public Item()
        {
            ItemBarcodes = new List<ItemBarcode>();
            //ItemPackTypes = new List<ItemPackType>();
            ItemEachCustomerList = new List<ItemEachCustomer>();
        }

        //[Required]
        public string MainCompanyCode { get; set; }

        /// <summary>
        /// Ürün Kodu - Zorunlu
        /// </summary>
        [StringLength(200)]
       //[Required]
        public string ItemCode { get; set; }

        /// <summary>
        /// Firmalara Göre Ürün Kodları  ( AAA-xxx;BBB-yyy;)
        /// </summary>
        //[StringLength(200)]
        //public string itemReferenceCodes { get; set; }

        /// <summary>
        /// Ürün Açıklaması - Zorunlu
        /// </summary>
        [StringLength(400)]
       //[Required]
        public string ItemDescription { get; set; }
        /// <summary>
        /// Ladometer : Ürünün treylerin içinde kapladığı her 1 metre, 1 LDM’ye tekabül eder. Bir treylerin uzunluğu 13,6, eni 2,40, yüksekliği 2,40 metredir. Bir treyler 13,6 LDM’dir. 1 LDM = 1750 kg
        /// </summary>
        public double? Ldm { get; set; }



        /// <summary>
        /// Üst Tüketim birimi : Palet, Paket,Adet
        /// </summary>
        //[Required]
        public int UnitTopType { get; set; }

        /// <summary>
        /// Küçük Tüketim birimi : Palet, Paket,Adet  - Zorunlu
        /// </summary>
        //[Required]
        public int UnitSubType { get; set; }


        /// <summary>
        /// Ürün GTIN numarası. Küresel Ticari Ürün Numarası (Global Trade Item Number)
        /// </summary>
        [StringLength(60)]
        public string Gtin { get; set; }
        /// <summary>
        /// Ana kategorisi
        /// </summary>
        [StringLength(50)]
        public string ItemMainCategory { get; set; }
        /// <summary>
        /// Alt kategorisi 
        /// </summary>
        [StringLength(50)]
        public string ItemSubCategory { get; set; }
        /// <summary>
        /// Detay kategorisi
        /// </summary>
        [StringLength(50)]
        public string ItemDetailCategory { get; set; }


        /// <summary>
        /// Son kullanma tarihi kontrolü zorunlu mu ? (Y : yes , N: NO)  - Zorunlu
        /// </summary>
        public bool ExpireDateRequired { get; set; }

        /// <summary>
        /// Lot(Parti) numarsı kontrolü zorunlu mu ? (Y : yes , N: NO)  - Zorunlu
        /// </summary>
        public bool LotRequired { get; set; }

        /// <summary>
        /// Ürünün raf ömrü
        /// </summary>
        public int ShelfLife { get; set; }

        /// <summary>
        /// Son kullanma tarihi uyarı verme süresi. (Gün)(Sakıncalı sevk süresi)
        /// </summary>
        public int? ExpireDateLeftDayCount { get; set; }

        /// <summary>
        /// Raf Ömrü uyarı verme süresi. (Gün) 
        /// </summary>
        public int? WarningForExpireDateMin { get; set; }

        /// <summary>
        /// Mal kabul için SKT uyarı verme süresi. (Gün) 
        /// </summary>
        public int? WarningForGoodsInExpireDate { get; set; }

        /// <summary>
        /// Adr etiketi
        /// </summary>
        //[DataMember]
        [StringLength(50)]
        public string AdrTypeLabel { get; set; }
        /// <summary>
        /// Adr numarası
        /// </summary>
        [StringLength(40)]
        public string AdrUnNumber { get; set; }
        /// <summary>
        /// Ürünün Adr tanımı
        /// </summary>
        [StringLength(200)]
        public string AdrItemName { get; set; }
        /// <summary>
        /// Adr ana risk sınıfı
        /// </summary>
        [StringLength(20)]
        public string AdrMainRiskClass { get; set; }
        /// <summary>
        /// Adr ek risk sınıfı
        /// </summary>
        [StringLength(20)]
        public string AdrAdditionalRisk { get; set; }
        /// <summary>
        /// Adr paket grubu
        /// </summary>
        [StringLength(20)]
        public string AdrPackageGroup { get; set; }
        /// <summary>
        /// Adr tünel risk kodu
        /// </summary>
        [StringLength(20)]
        public string AdrTunnelRestrictionCode { get; set; }
        /// <summary>
        /// Bildirimli mi? (Y:Yes, N: No)
        /// </summary>
        public bool IsNotified { get; set; }

        /// <summary>
        ///  İşlem türü
        /// </summary>
        public EnumRecordType RecordType { get; set; }

        /// <summary>
        /// Desi bilgisi ( litre )
        /// </summary>
        public double? Volume { get; set; }
        public double? GrossWeight { get; set; }
        public double? NetWeight { get; set; }

        /// <summary>
        /// Paket içindeki en küçük birim miktarı. Ör; 1 koli sigara => 10 karton => 100 kutu sigara , burada 100 
        /// </summary>
       //[Required]
        public double QuantityInPackage { get; set; }
        public int UnitType { get; set; }
        public double Width { get; set; }
        public double Height { get; set; }
        public double Length { get; set; }

        /// <summary>
        /// Ürün paket tipleri
        /// </summary>
        public virtual List<ItemBarcode> ItemBarcodes { get; set; }
    
        /// <summary>
        /// Ürün ün Müşteriye Özel Kodu
        /// </summary>
        public virtual List<ItemEachCustomer> ItemEachCustomerList { get; set; }

        #region FUNCTIONS
        public ReturnObject UpdateSyncStatus(string pItemCode, EnumSyncStatus pSyncStatus)
        {
            ReturnObject retObj = new ReturnObject();
            retObj.Result = true;
            retObj.ResultMessage = "";

            try
            {
                using (ESubeContext db = new ESubeContext())
                {
                    Item existsItem = (from p in db.Item where p.ItemCode == pItemCode && p.SyncStatusCode == EnumSyncStatus.SyncWaiting select p).FirstOrDefault();

                    if (existsItem != null)
                    {

                        existsItem.UpdateDate = DateTime.Now;

                        //Item ın sync statüsünü güncelle
                        existsItem.SyncStatusCode = pSyncStatus;

                        //Item Barcode sync statülerini güncelle 
                        //existsItem.ItemBarcodes = (from p in db.ItemBarcode where p.Item.Id == existsItem.Id && p.SyncStatusCode == EnumSyncStatus.SyncWaiting select p).ToList();


                        //if (existsItem.ItemBarcodes != null)
                        //{
                        //    existsItem.ItemBarcodes
                        //            .ToList()
                        //            .ForEach(a => {
                        //                a.SyncStatusCode = pSyncStatus;
                        //                a.UpdateDate = DateTime.Now;
                        //            }
                        //            );
                        //};


                        //ItemPackType sync statülerini güncelle 
                        //existsItem.ItemPackTypes = (from p in db.ItemPackType where p.Item.Id == existsItem.Id && p.SyncStatusCode == EnumSyncStatus.SyncWaiting select p).ToList();


                        //if (existsItem.ItemPackTypes != null)
                        //{
                        //    existsItem.ItemPackTypes
                        //            .ToList()
                        //            .ForEach(a => {
                        //                a.SyncStatusCode = pSyncStatus;
                        //                a.UpdateDate = DateTime.Now;
                        //            }
                        //            );
                        //};


                        //Item Customer a Özel Kodlar tablosunun  sync statülerini güncelle 
                        existsItem.ItemEachCustomerList = (from z in db.ItemEachCustomer where z.Item.Id == existsItem.Id && z.SyncStatusCode == EnumSyncStatus.SyncWaiting select z).ToList();


                        if (existsItem.ItemEachCustomerList != null)
                        {
                            existsItem.ItemEachCustomerList
                                    .ToList()
                                    .ForEach(a => {
                                        a.SyncStatusCode = pSyncStatus;
                                        a.UpdateDate = DateTime.Now;
                                    }
                                    );
                        };


                        db.SaveChanges();
                    }
                }
            }
            catch (Exception e)
            {
                retObj.Result = false;
                retObj.ResultMessage = e.Message + " " + e.InnerException;
            }



            return retObj;
        }
        public void InsertItem(Item pItem)
        {
            using (ESubeContext db = new ESubeContext())
            {

                db.Item.Add(pItem);
                db.SaveChanges();
            }

        }

        public List<Item> GetList(string pItemCode)
        {
            using (ESubeContext db = new ESubeContext())
            {
                var list = db.Item.Where(x => x.Id > 0 
                
                    && (string.IsNullOrEmpty(pItemCode) ? 1 == 1 : x.ItemCode == pItemCode)
                
                ).ToList();

                return list;
            }

        }
        #endregion

    }

    //Sadece ürün bilgilerini çekmek için
    public class ItemCustom
    {
        public long Id { get; set; }
        public string ItemCode { get; set; }
        public string ItemDescription { get; set; }
        public double Quantity { get; set; }

    }

 
}