﻿using ESube.Service.Context;
using ESube.Service.Models.Enums;
using ESube.Service.Models.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.Linq;

namespace ESube.Service.Models
{
    /// <summary>
    /// Ürün Paketleri tanımları. 
    /// </summary>
    [Table("ItemPackType")]
    public class ItemPackType : BaseIntegrationClass
    {
        public ItemPackType() { }
        /// <summary>
        /// Ürün
        /// </summary>
        [IgnoreDataMember]
        public Item Item { get; set; }


        public string ItemCode { get; set; }

        /// <summary>
        /// Çıkış birimi mi? (Y : yes , N: NO)
        /// </summary>
        [StringLength(1)]
        public string isDefault { get; set; }
        /// <summary>
        /// Kod (ADET)
        /// </summary>
        [StringLength(40)]
        public string typeCode { get; set; }
        /// <summary>
        /// Açıklama (Adet)
        /// </summary>
        [StringLength(400)]
        public string typeDescription { get; set; }
        /// <summary>
        /// Birim içi adet(Adet için 1 olmalı) - Zorunlu
        /// </summary>
        public int includeQuantity { get; set; }
        /// <summary>
        /// Birimi(Koli,Palet,Adet,metre ...) - Zorunlu
        /// </summary>
        public EnumUnitType UnitType { get; set; }
        /// <summary>
        /// Brüt ağırlık
        /// </summary>
        public double? itemGrossWeight { get; set; }
        /// <summary>
        /// Net ağırlık
        /// </summary>
        public double? itemNetWeight { get; set; }
        /// <summary>
        /// Hacim
        /// </summary>
        public double? itemVolume { get; set; }
        /// <summary>
        /// Genişlik (mm)
        /// </summary>
        public double? itemWidht { get; set; }
        /// <summary>
        /// Uzunluk (mm)
        /// </summary>
        public double? itemLength { get; set; }
        /// <summary>
        /// Yükseklik (mm)
        /// </summary>
        public double? itemHeight { get; set; }
    }
}