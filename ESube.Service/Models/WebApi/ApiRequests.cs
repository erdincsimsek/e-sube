﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ESube.Service.Models
{
    public class ApiRequest
    {
        public string Token { get; set; }
        public dynamic Parameters { get; set; }

        public ApiRequest()
        {
            Token = string.Empty;
            Parameters = null;
        }
    }

    //public class ApiRequestData
    //{
    //}
}   