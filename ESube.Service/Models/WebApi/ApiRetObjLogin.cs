﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ESube.Service.Models.WebApi
{
    public class ApiResponseLogin
    {
        public ApiResponseLogin()
        {
            StatusCode = EnumApiHttpStatus.OK;
            Message = "";
        }
        public EnumApiHttpStatus StatusCode { get; set; }
        public string Status { get { return (Enum.GetName(typeof(EnumApiHttpStatus), StatusCode)); } set { } }
        public string Message { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Token { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string TokenExpireDate { get; set; }


    }
}