﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ESube.Service.Models.WebApi
{
    public enum EnumApiHttpStatus : int
    {
        //MESAJ
        Continue = 100,//İstek başarılı alındığı ve devam edilebileceği belirtilir
        SwitchingProtocols = 101,//Sunucu, istemciden aldığı protokol değiştirme isteğine uyacağını belirtmektedir
        Processing = 102,//İşlemde

        //BAŞARI
        OK =200,//İstek başarılı alınmış ve cevap başarılı verilmiştir.
        Created = 201,//Oluşturuldu, İstek başarılı olmuş ve sunucuda yeni bir kaynak yaratılmıştır.
        Accepted = 202,//Onaylandı Sunucu isteği kabul etti ancak henüz işlemedi.
        NonAuthoritativeInformation=203,//Yetersiz Bilgi, Sunucu isteği başarılı işledi, ancak başka kaynakta olabilecek bilgi döndürmektedir.
        NoContent = 204,//İçerik Yok İstek başarılı alınmış ancak geri içerik döndürülmemektedir.
        ResetContent = 205, // İçeriği Baştan al İstek başarılı alınmış ancak geri içerik döndürülmemektedir. Ancak içerik temizlenecektir (örneğin bir web formunda doldurulan bilgiler).
        PartialContent = 206,//Kısmi İçerik GET için kısmi içerik (içeriğin bir belirli bir parçası) başarılıyla döndürülmüştür.
        MultiStatus = 207,// Çok-Statü
        ContentDifferent = 210,// Farklı İçerik
        
        //YÖNLENDİRME
        MultipleChoices = 300,// Çok Seçenek Sunucuda isteğe göre birden fazla seçenek olduğunu bildirir. Sunucu seçeneği kendisi seçebilir veya seçenek listesini görüntüleyebilir.
        MovedPermanently = 301,// Kalıcı Taşındı Bir kaynağın (veya sayfanın) kalıcı olarak başka bir yere taşındığını bildirir ve o yere yönlendirme sağlar.
        MovedTemporarily = 302,//Geçici Taşındı Bir kaynağın (veya sayfanın) kalıcı değil geçici olarak başka bir kaynağa yönlendirir. Kaynağın ana adresi değişmemiştir.
        SeeOther = 303,// Diğerlerine Bak Farklı bir kayanağa GET yapılması gerektiğini belirtir.
        NotModified = 304,// Güncellenmedi İstenilen kaynakta daha önce yapılan istekten beri herhangi bir değişikliğin olmadı belirtilir ve içerik gönderilmez.
        UseProxyProxy = 305,//Kullan Sunucu tarafından döndürülen proxy'in kullanılması gerektiği belirtilir.
        TemporaryRedirect = 307 ,//Geçici olarak yeniden gönder Bir kaynağın (veya sayfanın) kalıcı değil geçici olarak başka bir kaynağa yönlendirir.

        //İSTEMCİ HATALARI
        BadRequest=400,//  Kötü İstek İstek hatalı (isteğin yapısı hatalı) olduğu belirtilir.
        Unauthorized =401,// Yetkisiz İstek için kimlik doğrulaması gerekiyor.
        PaymentRequired =402,// Ödeme Gerekli Ödeme gerekiyor. (gelecekte kullanılması için ayrılmıştır).
        Forbidden =403,// Yasaklandı Kaynağın yasaklandığını belirtir.
        NotFound =404,// Sayfa Bulunamadı İstek yapılan kaynağın (veya sayfanın) bulunamadığını belirtir.
        MethodNotAllowed =405,// İzin verilmeyen Metod Sunucu, HTTP Method'u kabul etmiyor.
        NotAcceptable =406,// Kabul Edilemez İstemcinin Accept header'ında verilen özellik karşılanamıyor.
        ProxyAuthenticationRequired =407,// Sunucuda login olmak gerekli Proxy üzerinden yetkilendirme gerekir.
        RequestTimeout = 408,// İstek zaman aşamına uğradı İstek zaman aşımına uğradı (belirli bir sürede istek tamamlanamadı).
        Conflict =409,// (Hatlar) Çakıştı, Çakışma İstek içinde çelişki var.
        Gone =410,// Bak Kaynak artık yok. 
        LengthRequired =411,// İstekte "Content-Length" (içeriğin boyutu) belirtilmemiş.
        PreconditionFailed =412,// Sunucu istekte belirtilen bazı önkoşulları karşılamıyor.
        RequestEntity =413,// Too Large İsteğin boyutu çok büyük olduğu için işlenemedi.
        RequestURI=414,//Too Long URI (URL) fazla büyük.
        RequestedRangeUnsatifiable =416,// İstenilen kaynak istenilen medya tipini desteklemiyor.
        ExpectationFailed =417,// İstek yapılan parça(bir dosyanın bir parçası vb..) sunucu tarafından verilebiliyor veya uygun değil.
        UnprocessableEntity =422,// Sunucu "Expect" ile istenileni desteklemiyor veya yerine getiremiyor.
        Locked =423 ,//
        MethodFailure=424,//
        UnavailableForLegalReasons = 451,//Yasal nedenlerle gösterilemiyor

        // SUNUCU HATALARI
        InternalServerError = 500,// Sunucuda bir hata oluştu ve istek karşılanamadı.
        NotImplemented=501,// Uygulanmamış Sunucu istenilen isteği yerine getirecek şekilde yapılandırılmamıştır.
        BadGateway= 502,// Geçersiz Ağ Geçidi Gateway veya Proxy sunucusu, kaynağın bulunduğu sunucudan (upstream sunucusu) cevap alamıyor.
        ServiceUnavailable=503,// Hizmet Yok Sunucu şu anda hizmet vermiyor (kapalı veya erişilemiyor).
        GatewayTimeout =504,// Gateway veya Proxy sunucusu, kaynağın bulunduğu sunucudan (upstream sunucusu) belirli bir zaman içinde cevap alamadı.
        HTTPVersionNotSupported=505,// HTTP Protokol versiyonu desteklenmiyor.
        InsufficientStorage= 507,

        Error = 1000,//Custom error


    }

}