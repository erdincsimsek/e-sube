﻿using ESube.Service.Models.WebApi;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace ESube.Service.Models
{
    //[Serializable]
    public class ApiResponse
    {
        public ApiResponse()
        {
            StatusCode = EnumApiHttpStatus.OK;
            Message = "";
        }

        public EnumApiHttpStatus StatusCode{ get; set; }
        public string Status { get { return (Enum.GetName(typeof(EnumApiHttpStatus), StatusCode)); } set { } }
        public string Message { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public dynamic Data { get; set; }

    }

    //[Serializable]
    //public class ApiResponseData
    //{


    //    [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
    //    public List<Tender> TenderList { get; set; }

    //    [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
    //    public List<HemelWayBillGoodsIn> HemelWayBillGoodsInList { get; set; }

    //    [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
    //    public List<HemelWayBillGoodsOut> HemelWayBillGoodsOutList { get; set; }

    //}



}