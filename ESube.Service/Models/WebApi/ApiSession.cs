﻿using ESube.Service.Context;
using ESube.Service.Models.Helpers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace ESube.Service.Models.WebApi
{
    public class ApiSession
    {
        public ApiSession()
        {
            Token = string.Empty;
            LoginValid = true;
            LoginValidDate = new DateTime(1900, 1, 1);
            MainCompany = new MainCompany();
        }

        public string Message { get; set; }
        public string Token { get; set; }
        public bool LoginValid { get; set; }
        public DateTime LoginValidDate { get; set; }
        public MainCompany MainCompany { get; set; }


        public ApiSession GetByToken(HttpRequestMessage  pRequest)
        {
            ApiSession _ApiSession = new ApiSession();


                string _TokenStr = JsonConvert.DeserializeObject(pRequest.Headers.GetValues("Token").First()).ToString();
                _ApiSession.Token = _TokenStr;


                using (ESubeContext db = new ESubeContext())
                {
                    long CompanyId = (from x in db.TokensManager where x.TokenKey == _TokenStr select x.CompanyId).FirstOrDefault();
                    _ApiSession.MainCompany = (from x in db.MainCompany where x.Id == CompanyId select x).FirstOrDefault();
                }

            return _ApiSession;
        }



    }

    public class ApiSessionData
    {
        public static ApiSession GetByToken(string token)
        {
            //DataTable dt = DAL.GetSessionMobile(token);
            //if (dt.Rows.Count > 0)
            //{
            //    DataRow row = dt.Rows[0];

            //    SessionMobile obj = new SessionMobile()
            //    {
            //        UserId = row.Field<int>("UserId"),
            //        UserName = row.Field<string>("UserName"),
            //        Date = row.Field<DateTime>("Date"),
            //        ValidDate = row.Field<DateTime>("ValidDate"),
            //        DeviceBrand = row.Field<string>("DeviceBrand"),
            //        DeviceModel = row.Field<string>("DeviceModel"),
            //        DeviceImei = row.Field<string>("DeviceImei"),
            //        Token = row.Field<string>("Token"),

            //    };
            //    return obj;
            //}

            return null;
        }

    }
}