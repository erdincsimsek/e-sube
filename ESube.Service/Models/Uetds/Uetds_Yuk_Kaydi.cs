﻿using ESube.Service.Context;
using ESube.Service.Helpers;
using ESube.Service.Models.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ESube.Service.Models
{
    public class Uetds_Yuk_Kaydi : BaseEntityClass
    {
        
        public string OrderCode {get;set;}
        public string SonucKodu { get; set; }
        public string SonucMesaji { get; set; }
        public string EsyaSonucKodu { get; set; }
        public string EsyaSonucMesaji { get; set; }
        public string UetdsBildirimRefNo { get; set; }

    }
}