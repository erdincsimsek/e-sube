﻿using ESube.Service.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ESube.Service.Models
{
    public class Uetds_Yuk_Kaydi_Detayi
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public string SonucKodu { get; set; }
        public string SonucMesaji { get; set; }
        public string AliciUnvan { get; set; }
        public string AliciVergiNo { get; set; }
        public string BildirimTarihi { get; set; }
        public string BosaltmaIlAdi { get; set; }
        public int?    BosaltmaIlMernisKodu { get; set; }
        public string BosaltmaIlceAdi { get; set; }
        public int?    BosaltmaIlceKodu { get; set; }
        public string BosaltmaSaati { get; set; }
        public string BosaltmaTarihi { get; set; }
        public string BosaltmaUlkeAdi { get; set; }
        public string BosaltmaUlkeKodu { get; set; }
        public string Durum { get; set; }
        public string FirmaSeferNo { get; set; }
        public string FirmaYukNo { get; set; }
        public string GonderenUnvan { get; set; }
        public string GonderenVergiNo { get; set; }
        public string IptalAciklama { get; set; }
        public string IptalTurKodu { get; set; }
        public string Plaka1 { get; set; }
        public string Plaka2 { get; set; }
        public string Sofor1TcNo { get; set; }
        public string Sofor2TcNo { get; set; }
        public string SonGuncellemeTarihi { get; set; }
        public string TasimaBedeli { get; set; }
        public string TasimaTuruAciklama { get; set; }
        public int?    TasimaTuruKodu { get; set; }
        public string TehlikeliMaddeTasimaSekliAciklama { get; set; }
        public int?    TehlikeliMaddeTasimaSekliKodu { get; set; }
        public string UetdsBildirimRefNo { get; set; }
        public string UnNumarasi { get; set; }
        public string YukBirimi { get; set; }
        public string YukBirimiAciklama { get; set; }
        public string YukCinsAdi { get; set; }
        public int?    YukCinsId { get; set; }
        public string YukDigerAciklama { get; set; }
        public string YukMiktari { get; set; }
        public string YuklemeIlAdi { get; set; }
        public int?    YuklemeIlMernisKodu { get; set; }
        public string YuklemeIlceAdi { get; set; }
        public int?    YuklemeIlceMernisKodu { get; set; }
        public string YuklemeSaati { get; set; }
        public string YuklemeTarihi { get; set; }
        public string YuklemeUlkeAdi { get; set; }
        public string YuklemeUlkeKodu { get; set; }
        public DateTime? CreateDate { get; set; }
    }
}