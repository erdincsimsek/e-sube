﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ESube.Service.Models
{
    public class Uetds_Log
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public int? Islem_Id { get; set; }
        public string SonucKodu { get; set; }
        public string SonucMesaji { get; set; }
        public string UetdsBildirimRefNo { get; set; }
        public string IptalKodu { get; set; }
        public string IptalAciklama { get; set; }
        public DateTime? CreateDate { get; set; }
    }
}