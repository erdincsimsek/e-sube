﻿using ESube.Service.Models.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace ESube.Service.Models
{
    /// <summary>
    /// Ürün Barkodları
    /// </summary>
    [Table("GetirItemBarcode")]
    public class GetirItemBarcode : BaseIntegrationClass
    {

        [IgnoreDataMember]
        public GetirItem Item { get; set; }



        [StringLength(50),Required]
        public string Barcode { get; set; }
        [StringLength(50)]
        public string InventoryItemPackType { get; set; }
        public string IntegrationCode { get; set; }


    }
}