﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace ESube.Service.Models
{
    [Table("vw_Getir_IntegrationDetails")]
    public class IntegrationDetails
    {
        long _rowNumber;
        [Key]
        public long rowNumber
        {
            get { return _rowNumber; }
            set { _rowNumber = value; }
        }

        public string orderMainType { get; set; }
        public string DocumentId { get; set; }
        public int orderSubType { get; set; }
        public string S75HTIP1 { get; set; }
        public string WaybillNumber { get; set; }
        
        public string WaybillDate { get; set; }
        public string VehiclePlate { get; set; }
        public string PalletBarcode { get; set; }
        public string WarehouseCode { get; set; }
        public string S75GT_IntegrationCode { get; set; }
        public DateTime? S75GT_IntegrationDate { get; set; }
        public string S75GT_IntegrationStatus { get; set; }
        public string S75GT_IntegrationStatusDescription { get; set; }
        public string S75GT_IntegrationContent { get; set; }
        public string orderCode { get; set; }
        public int S75STAT { get; set; }
        [DataType(DataType.Date)]
        public DateTime? createDate { get; set; }
    }
}