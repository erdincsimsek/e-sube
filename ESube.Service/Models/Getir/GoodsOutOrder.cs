﻿using ESube.Service.Context;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Linq;
using ESube.Service.Models.Enums;
using ESube.Service.Models.Helpers;

namespace ESube.Service.Models
{
    /// <summary>
    /// Sipariş
    /// </summary>
    [Table("GetirGoodsOutOrder")]
    public class GetirGoodsOutOrder : BaseIntegrationClass
    {
        public GetirGoodsOutOrder()
        {
            WarehouseOrderItems = new List<GetirGoodsOutOrderLine>();
        }

        [Description("Order Code")]
        [StringLength(100)]
        public string WaorCode { get; set; }
        [Description("Address Code")]
        [StringLength(250)]
        public string WaorCustomerAddressCode { get; set; }

        [Description("Order Document Id")]
        [StringLength(100)]
        public string WaorDepositorOrderNo { get; set; }

        [Description("Warehouse Code")]
        [StringLength(10)]
        public string WaorInventorySiteCode { get; set; }

        [Description("Note")]
        [StringLength(250)]
        public string WaorNotes { get; set; }

        [Description("Order Type")]
        public string WaorWarehouseOrderTypeID { get; set; }

        [StringLength(50)]
        public string OrderReferenceCode { get; set; }

        public string IntegrationCode { get; set; }


        public virtual List<GetirGoodsOutOrderLine> WarehouseOrderItems { get; set; }



        #region FUNCTIONS


        public List<GetirGoodsOutOrder> GetListAll(string pOrderCode = "", EnumSyncStatus pOrderStatus = 0)
        {
            List<GetirGoodsOutOrder> _List = new List<GetirGoodsOutOrder>();

            using (ESubeContext db = new ESubeContext())
            {
                //_List = (from p in db.GetirGoodsOutOrder select p).Take(1000).OrderByDescending(x => x.createDate).ToList();

                _List = (from p in db.GetirGoodsOutOrder.Where(x => (x.WaorCode.Contains(pOrderCode) || x.WaorCustomerAddressCode.Contains(pOrderCode) || x.OrderReferenceCode.Contains(pOrderCode)) && pOrderStatus == 0 ? 1 == 1 : x.SyncStatusCode == pOrderStatus) select p).Take(1000).OrderByDescending(x => x.CreateDate).ToList();
            }

            return _List;
        }

        public void UpdateSyncStatus(string pUniqueCode, EnumSyncStatus pSyncStatus)
        {

            using (ESubeContext db = new ESubeContext())
            {
                GetirGoodsOutOrder existsData = (from x in db.GetirGoodsOutOrder where x.WaorCode == pUniqueCode && x.SyncStatusCode == EnumSyncStatus.SyncWaiting select x).FirstOrDefault();

                if (existsData != null)
                {
                    existsData.UpdateDate = DateTime.Now;
                    existsData.SyncStatusCode = pSyncStatus;


                    existsData.WarehouseOrderItems = (from x in db.GetirGoodsOutOrderLine where x.order.Id == existsData.Id && x.SyncStatusCode == EnumSyncStatus.SyncWaiting select x).ToList();

                    // sync statülerini güncelle 
                    if (existsData.WarehouseOrderItems != null)
                    {
                        existsData.WarehouseOrderItems
                                .ToList()
                                .ForEach(a => {
                                    a.SyncStatusCode = pSyncStatus;
                                    a.UpdateDate = DateTime.Now;
                                }
                                );
                    };


                    db.SaveChanges();
                }
            }

        }

        public void AddOrder(GetirGoodsOutOrder pOrder)
        {
            using (ESubeContext db = new ESubeContext())
            {
                db.GetirGoodsOutOrder.Add(pOrder);
                db.SaveChanges();

            }
        }


        public void DeleteOrder(string pOrderCode, string pDocumentId)
        {
            using (ESubeContext db = new ESubeContext())
            {
                GetirGoodsOutOrder order = new GetirGoodsOutOrder();
                order.SyncStatusCode = EnumSyncStatus.Delete;
                order.SyncDesc = "Delete";

                order.WaorCode = pOrderCode;
                order.WaorDepositorOrderNo = pDocumentId;

                db.GetirGoodsOutOrder.Add(order);
                db.SaveChanges();
            }
        }

        #endregion

    }
}

