﻿using ESube.Service.Models.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace ESube.Service.Models
{
    /// <summary>
    /// Ürün Paketleri
    /// </summary>
    [Table("GetirItemPackType")]
    public class GetirItemPackType : BaseIntegrationClass
    {

        [IgnoreDataMember]
        public GetirItem Item { get; set; }



        public bool ConsumerUnit { get; set; }
        [StringLength(400)]
        public string Description { get; set; }
        [StringLength(10)]
        public string Height { get; set; }
        public int IncludesCUQuantity { get; set; }
        [StringLength(10)]
        public string Length { get; set; }
        [StringLength(10)]
        public string NetWeight { get; set; }
        public string Unit { get; set; }
        [StringLength(10)]
        public string Volume { get; set; }
        [StringLength(10)]
        public string Weight { get; set; }
        [StringLength(10)]
        public string Widht { get; set; }
        public string IntegrationCode { get; set; }

    }
}