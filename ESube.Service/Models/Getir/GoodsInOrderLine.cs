﻿using ESube.Service.Context;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Linq;
using ESube.Service.Models.Helpers;

namespace ESube.Service.Models
{
    /// <summary>
    /// Sipariş satırları
    /// </summary>
    [Table("GetirGoodsInOrderLine")]
    public class GetirGoodsInOrderLine : BaseIntegrationClass
    {
        public GetirGoodsInOrderLine() { }

        [IgnoreDataMember]
        public GetirGoodsInOrder order { get; set; }
        [StringLength(400)]
        public string WareInventoryItemDetailNotes { get; set; }
        [StringLength(100)]
        public string WareInventoryItemID { get; set; }
        public int WareInventoryItemPlanCUQty { get; set; }
        [StringLength(40)]
        public string WareInventoryItemUnitID { get; set; }
        [StringLength(10)]
        public string WareInventoryItemWRDetailCode { get; set; }
        public string IntegrationCode { get; set; }
        public string FreeText { get; set; }

        public List<GetirGoodsInOrderLine> GetListByOrderCode(string pOrderCode)
        {
            List<GetirGoodsInOrderLine> List = new List<GetirGoodsInOrderLine>();

            using (ESubeContext db = new ESubeContext())
            {
                long OrderId = (from x in db.GetirGoodsInOrder where x.WareCode == pOrderCode select x.Id).FirstOrDefault();
                List = (from x in db.GetirGoodsInOrderLine where x.order.Id == OrderId select x).ToList();

            }

            return List;
        }

    }
}