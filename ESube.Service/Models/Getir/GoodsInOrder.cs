﻿using ESube.Service.Context;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Linq;
using ESube.Service.Models.Enums;
using ESube.Service.Models.Helpers;

namespace ESube.Service.Models
{

    /// <summary>
    /// Giriş Sipariş
    /// </summary>
    [Table("GetirGoodsInOrder")]
    public class GetirGoodsInOrder : BaseIntegrationClass
    {
        public GetirGoodsInOrder()
        {
            ReceiptItems = new List<GetirGoodsInOrderLine>();
        }



        [Description("Order Code")]
        [StringLength(100)]
        public string WareCode { get; set; }

        [Description("Address Code")]
        [StringLength(250)]
        public string WareSupplierAddressCode { get; set; }

        [Description("Order Reference Code")]
        [StringLength(100)]
        public string WareDepositorReceiptNo { get; set; }

        [Description("Warehouse Code")]
        [StringLength(10)]
        public string WareInventorySiteCode { get; set; }

        [Description("Note")]
        [StringLength(250)]
        public string WareNotes { get; set; }

        [Description("Planned date")]
        [StringLength(100)]
        public string WarePlannedReceiptDate { get; set; }

        [StringLength(50)]
        public string OrderReferenceCode { get; set; }

        [Description("Order Type")]
        public string WareWarehouseReceiptTypeID { get; set; }
        public string IntegrationCode { get; set; }
        public string FreeText { get; set; }

        public virtual List<GetirGoodsInOrderLine> ReceiptItems { get; set; }



        #region FUNCTIONS
        
        public List<GetirGoodsInOrder> GetListAll(string pOrderCode="", EnumSyncStatus pOrderStatus=0)
        {
            List<GetirGoodsInOrder> _List = new List<GetirGoodsInOrder>();

            using (ESubeContext db = new ESubeContext())
            {
                _List = (from p in db.GetirGoodsInOrder.Where(x=>(x.WareCode.Contains(pOrderCode) || x.WareDepositorReceiptNo.Contains(pOrderCode) || x.WareSupplierAddressCode.Contains(pOrderCode)) && pOrderStatus == 0 ? 1==1 :  x.SyncStatusCode == pOrderStatus)   select p).Take(1000).OrderByDescending(x=>x.CreateDate).ToList();
            }

            return _List;
        }


        public void UpdateSyncStatus(string pUniqueCode, EnumSyncStatus pSyncStatus)
        {

            using (ESubeContext db = new ESubeContext())
            {
                GetirGoodsInOrder existsData = (from x in db.GetirGoodsInOrder where x.WareCode == pUniqueCode && x.SyncStatusCode == EnumSyncStatus.SyncWaiting select x).FirstOrDefault();

                if (existsData != null)
                {
                    existsData.UpdateDate = DateTime.Now;
                    existsData.SyncStatusCode = pSyncStatus;


                    existsData.ReceiptItems = (from x in db.GetirGoodsInOrderLine where x.order.Id == existsData.Id && x.SyncStatusCode == EnumSyncStatus.SyncWaiting select x).ToList();



                    // sync statülerini güncelle 
                    if (existsData.ReceiptItems != null)
                    {
                        existsData.ReceiptItems
                                .ToList()
                                .ForEach(a => {
                                    a.SyncStatusCode = pSyncStatus;
                                    a.UpdateDate = DateTime.Now;
                                }
                                );
                    };


                    db.SaveChanges();
                }
            }

        }

        public void AddOrder(GetirGoodsInOrder pOrder)
        {
            using (ESubeContext db = new ESubeContext())
            {
                db.GetirGoodsInOrder.Add(pOrder);
                db.SaveChanges();
            }
        }


        public void DeleteOrder(string pOrderCode, string pDocumentId)
        {
            using (ESubeContext db = new ESubeContext())
            {
                GetirGoodsInOrder order = new GetirGoodsInOrder();
                order.SyncStatusCode = EnumSyncStatus.Delete;
                order.SyncDesc = order.SyncStatus;

                order.WareCode = pOrderCode;
                order.WareDepositorReceiptNo = pDocumentId;

                db.GetirGoodsInOrder.Add(order);
                db.SaveChanges();
            }
        }

        #endregion

    }
}