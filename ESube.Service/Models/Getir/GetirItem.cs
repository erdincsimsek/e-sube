﻿using ESube.Service.Context;
using ESube.Service.Models.Enums;
using ESube.Service.Models.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Linq;

namespace ESube.Service.Models
{
    /// <summary>
    /// Ürün Tanımları
    /// </summary>
    [Table("GetirItem")]
    public class GetirItem : BaseIntegrationClass
    {
        public GetirItem()
        {
            InventoryItemBarcodes = new List<GetirItemBarcode>();
            InventoryItemPackTypes = new List<GetirItemPackType>();
        }
        public virtual ICollection<GetirItemPackType> InventoryItemPackTypes { get; set; }
        public virtual ICollection<GetirItemBarcode> InventoryItemBarcodes { get; set; }


        [StringLength(5)]
        public string AbcCode { get; set; }
        public bool AllocatesAgainstQCPolicy { get; set; }
        public int? CountingCycle { get; set; }
        [StringLength(200),Required]
        public string Description { get; set; }
        [StringLength(50)]
        public string InventItemGroup { get; set; }
        public int? InventoryItemAllowedExpDate { get; set; }
        [StringLength(50),Required]
        public string InventoryItemCode { get; set; }
        public bool? InventoryItemExpDateTracking { get; set; }
        public bool? InventoryItemLotTracking { get; set; }

        public int? InventoryItemShelfLife { get; set; }
        public int? InventoryItemWarningForExpDate { get; set; }
        public int? InventoryItemWarningForExpDateMin { get; set; }
        public bool IsFragile { get; set; }
        public bool IsKitItem { get; set; }
        [StringLength(50)]
        public string ItemDetailCategory { get; set; }
        [StringLength(50)]
        public string ItemMainCategory { get; set; }
        [StringLength(50)]
        public string ItemSubCategory { get; set; }
        [StringLength(50),Required]
        public string ItemType { get; set; }
        public int? MinimumOrderQTY { get; set; }
        public string Notes { get; set; }
        public string QualityPolicy { get; set; }
        public string SafetyStockCU { get; set; }
        public string SecondaryCode { get; set; }
        public string Volume { get; set; }
        public string Weight { get; set; }
        public string IntegrationCode { get; set; }


        #region FUNCTIONS

        public void UpdateSyncStatus(string pUniqueCode, EnumSyncStatus pSyncStatus)
        {

            using (ESubeContext db = new ESubeContext())
            {
                GetirItem existsData = (from x in db.GetirItem where x.InventoryItemCode == pUniqueCode && x.SyncStatusCode == EnumSyncStatus.SyncWaiting select x).FirstOrDefault();

                if (existsData != null)
                {
                    existsData.UpdateDate = DateTime.Now;
                    existsData.SyncStatusCode = pSyncStatus;

                    //InventoryItemBarcodes
                    existsData.InventoryItemBarcodes = (from x in db.GetirItemBarcode where (x.Item.Id == existsData.Id && x.SyncStatusCode == EnumSyncStatus.SyncWaiting) select x).ToList();

                    //statüleri güncelle 
                    if (existsData.InventoryItemBarcodes != null)
                    {
                        existsData.InventoryItemBarcodes
                                .ToList()
                                .ForEach(a =>
                                {
                                    a.SyncStatusCode = pSyncStatus;
                                    a.UpdateDate = DateTime.Now;
                                }
                                );
                    };




                    //InventoryItemPackTypes
                    existsData.InventoryItemPackTypes = (from x in db.GetirItemPackType where (x.Item.Id == existsData.Id && x.SyncStatusCode == EnumSyncStatus.SyncWaiting) select x).ToList();

                    //statüleri güncelle 
                    if (existsData.InventoryItemPackTypes != null)
                    {
                        existsData.InventoryItemBarcodes
                                .ToList()
                                .ForEach(a =>
                                {
                                    a.SyncStatusCode = pSyncStatus;
                                    a.UpdateDate = DateTime.Now;
                                }
                                );
                    };



                    db.SaveChanges();
                }
            }

        }


        public void ItemAdd( GetirItem item)
        {
            using (ESubeContext db = new ESubeContext())
            {
                db.GetirItem.Add(item);
                db.SaveChanges();
            }
        }

        #endregion

    }
}