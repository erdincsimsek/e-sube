﻿using ESube.Service.Context;
using ESube.Service.Models.Enums;
using ESube.Service.Models.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Linq;

namespace ESube.Service.Models
{
    /// <summary>
    /// Firma Tanımları
    /// </summary>
    [Table("GetirCompany")]
    public class GetirCompany : BaseIntegrationClass
    {
        public GetirCompany() { }

        [StringLength(500),Required]
        public string PartyAddress { get; set; }
        [StringLength(500)]
        public string PartyAddressDistrict { get; set; }
        [StringLength(500)]
        public string PartyAddressEmail { get; set; }
        [StringLength(500)]
        public string PartyAddressLangtitude { get; set; }
        [StringLength(500)]
        public string PartyAddressLongtitude { get; set; }
        [StringLength(50)]
        public string PartyAddressPhone { get; set; }
        [StringLength(50)]
        public string PartyAddressTimeClosed { get; set; }
        [StringLength(50)]
        public string PartyAddressTimeopened { get; set; }
        [StringLength(50)]
        public string PartyCity { get; set; }
        [StringLength(100),Required]
        public string PartyCode { get; set; }
        [StringLength(100)]
        public string PartyDepositor { get; set; }
        [StringLength(400),Required]
        public string PartyDescription { get; set; }
        [StringLength(400)]
        public string PartyGeoArea { get; set; }
        public bool PartyIsChanged { get; set; }
        public bool PartyIsCustomer { get; set; }
        public bool PartyIsSupplier { get; set; }
        [StringLength(10)]
        public string PartyPostcode { get; set; }
        [StringLength(20)]
        public string PartyTaxCode { get; set; }
        [StringLength(100)]
        public string PartyTaxoffice { get; set; }
        [StringLength(100)]
        public string PartyTown { get; set; }
        public string IntegrationCode { get; set; }



        #region FUNCTIONS
        public void UpdateSyncStatus(string pUniqueCode, EnumSyncStatus pSyncStatus)
        {

            using (ESubeContext db = new ESubeContext())
            {
                GetirCompany existsData = (from x in db.GetirCompany  where x.PartyCode == pUniqueCode && x.SyncStatusCode == EnumSyncStatus.SyncWaiting select x).FirstOrDefault();

                if (existsData != null)
                {
                    existsData.UpdateDate = DateTime.Now;
                    existsData.SyncStatusCode = pSyncStatus;

                    db.SaveChanges();
                }
            }

        }

        public void AddGetirCompany(GetirCompany pItem)
        {
            using (ESubeContext db = new ESubeContext())
            {
                db.GetirCompany.Add(pItem);
                db.SaveChanges();
            }
        }


        #endregion

    }
}