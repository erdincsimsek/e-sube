﻿using ESube.Service.Context;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Linq;
using ESube.Service.Models.Helpers;
using ESube.Service.Models.Enums;

namespace ESube.Service.Models
{
    /// <summary>
    /// Sipariş satırları
    /// </summary>
    [Table("GetirGoodsOutOrderLine")]
    public class GetirGoodsOutOrderLine : BaseIntegrationClass
    {
        public GetirGoodsOutOrderLine() { }

        [IgnoreDataMember]
        public GetirGoodsOutOrder order { get; set; }

        [StringLength(100)]
        public string WaorInventoryItemCode { get; set; }
        [StringLength(3)]
        public string WaorInventoryItemDetailNo { get; set; }
        [StringLength(400)]
        public string WaorInventoryItemNotes { get; set; }
        public int WaorInventoryItemPlanCUQty { get; set; }
        [StringLength(40)]
        public string WaorInventoryItemUnitID { get; set; }

        public string IntegrationCode { get; set; }

        #region FUNCTIONS
        public List<GetirGoodsOutOrderLine> GetListByOrderCode(string pOrderCode)
        {
            List<GetirGoodsOutOrderLine> List = new List<GetirGoodsOutOrderLine>();

            using (ESubeContext db = new ESubeContext())
            {
                long OrderId = (from x in db.GetirGoodsOutOrder where x.WaorCode == pOrderCode select x.Id).FirstOrDefault();
                List = (from x in db.GetirGoodsOutOrderLine where x.order.Id == OrderId select x).ToList();

            }

            return List;

        }

        #endregion

    }
}