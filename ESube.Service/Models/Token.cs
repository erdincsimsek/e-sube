﻿using ESube.Service.AES256Encryption;
using ESube.Service.Context;
using ESube.Service.Models.Helpers;
using ESube.Service.Repository;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Web;
using static ESube.Service.AES256Encryption.EncryptionLibrary;

namespace ESube.Service.Models
{
    [Table("Token")]
    public class Token
    {
        public Token()
        {

            

        }

        [Key]
        public int TokenID { get; set; }
        public string TokenKey { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime IssuedOn { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime ExpireDate { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime CreateDate { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime UpdateDate { get; set; }
        public long CompanyId { get; set; }


        #region FUNCTIONS
        public Token InsertOrUpdateToken(string pEmailAddressOrUsername, string pPassword)
        {
            Token _Token = new Token();
            using (var db = new ESubeContext())
            {

                //Kullanıcıya ait bilgileri çek...
                string pass = EncryptionLibrary.EncryptText(pPassword);
                long UserId = (from p in db.User where (p.EmailAddress == pEmailAddressOrUsername || p.Username == pEmailAddressOrUsername) && p.Password == pass select p.Id).FirstOrDefault();
                long CompanyId = (from p in db.User where (p.EmailAddress == pEmailAddressOrUsername || p.Username == pEmailAddressOrUsername) && p.Password == pass select p.CompanyId).FirstOrDefault();

                _Token = (from p in db.TokensManager where p.CompanyId == CompanyId && p.ExpireDate >= DateTime.Now select p).FirstOrDefault();

                #region NEW TOKEN STR
                string NewToken =
                  string.Join(":", new string[]
                  {    Convert.ToString(UserId),//USERID
                         KeyGenerator.GetUniqueKey(),
                         Convert.ToString(CompanyId),// COMPANYID
                         Convert.ToString(IssuedOn.Ticks),
                         EncryptionLibrary.KeyGenerator.GetUniqueKey() //CLIENT ID
                  });

                NewToken = EncryptionLibrary.EncryptText(NewToken);
                //ERSOY 
                //NewToken = Guid.NewGuid().ToString().ToUpper();
                #endregion

                //Token var ise
                if (_Token != null)
                {
                    _Token.TokenKey = NewToken;
                    _Token.UpdateDate = DateTime.Now;
                    _Token.ExpireDate = DateTime.Now.AddMonths(Convert.ToInt32(ConfigurationManager.AppSettings["TokenExpiry"]));

                }
                else{
                    _Token = new Token();
                    _Token.TokenKey = NewToken;
                    _Token.CompanyId = CompanyId;//Client key.CompanyId
                    _Token.IssuedOn = DateTime.Now;
                    _Token.CreateDate = DateTime.Now;
                    _Token.ExpireDate = DateTime.Now.AddMonths(Convert.ToInt32(ConfigurationManager.AppSettings["TokenExpiry"]));
                    db.TokensManager.Add(_Token);
                }

                db.SaveChanges();
            }
            return _Token;

        }

        public ReturnObject ValidateWebApiToken(HttpRequestMessage pRequeest)
        {
            ReturnObject retObj = new ReturnObject();
            string _TokenStr = "";
            Token _Token = new Token();
            ESubeContext db = new ESubeContext();
                 
            try
            {
                _TokenStr = JsonConvert.DeserializeObject(pRequeest.Headers.GetValues("Token").First()).ToString();

                _Token = db.TokensManager.Where(x=> x.TokenKey == _TokenStr).FirstOrDefault();


                if (_Token == null)
                {
                    retObj.ResultMessage = "Invalid Token";

                }else if (_Token.ExpireDate < DateTime.Now)
                {
                    retObj.ResultMessage = "Token Expired";
                }
                else
                {
                    retObj.Result = true;
                }

            }
            catch (Exception e){
                retObj.ResultMessage = "Error Occured When Checking Token";
            }

            return retObj;
        }
        #endregion

    }
}