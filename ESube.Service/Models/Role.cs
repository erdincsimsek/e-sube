﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ESube.Service.Context;
using System.Data.Entity;
using ESube.Service.Models.Helpers;
using ESube.Service.Helpers;

namespace ESube.Service.Models
{
    [Table("Role")]
    public class Role : BaseEntityClass
    {
        public string Name { get; set; }
        public string Code { get; set; }

        #region RELATIONSHIPS
        public virtual List<UserRole> RoleList { get; set; }
        #endregion

        #region FUNCTIONS
        
        
        #endregion

    }
}