﻿using ESube.Service.Context;
using ESube.Service.Models.Enums;
using ESube.Service.Models.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace ESube.Service.Models
{
    /// <summary>
    /// Taşıma Talebi Cevabı
    /// </summary>

    [Table("TenderAnswer")]
    public class TenderAnswer : BaseIntegrationClass
    {
        //Tender _Tender;
        //public Tender Tender
        //{
        //    get { return _Tender; }
        //    set { _Tender = value; }
        //}
        [IgnoreDataMember]
        public Tender tender { get; set; }
        public string TenderCode { get; set; }
        public string AnsweredBy { get; set; }
        public string AnsweredDate { get; set; }
        public string StatusCode { get; set; }
        public string CurrencyCode { get; set; }
        public string Comments { get; set; }
        public string Notes { get; set; }
        public string TenderPriceType1 { get; set; }
        public string TenderPriceType2 { get; set; }
        public string TenderPriceType3 { get; set; }
        public string TenderPriceType4 { get; set; }
        public string TenderPriceType5 { get; set; }
        public string TenderPriceType6 { get; set; }
        public string TenderPriceType7 { get; set; }
        public string TenderPriceType8 { get; set; }
        public string TenderPrice1 { get; set; }
        public string TenderPrice2 { get; set; }
        public string TenderPrice3 { get; set; }
        public string TenderPrice4 { get; set; }
        public string TenderPrice5 { get; set; }
        public string TenderPrice6 { get; set; }
        public string TenderPrice7 { get; set; }
        public string TenderPrice8 { get; set; }

        public string Commission { get; set; }
        public string TotalPrice { get; set; }
        public string TotalAmount { get; set; }
        public string ManualAmount { get; set; }
        public string DriverSeq { get; set; }
        public string DriverName { get; set; }
        public string DriverExternalSystemId { get; set; }
        public string DriverCharachterName { get; set; }
        public string DriverMobileNR { get; set; }
        public string LicensePlate { get; set; }
        public string PaymentMethod { get; set; }
        public string PaymentInfo { get; set; }
        public string CarrierName { get; set; }
        public string CarrierExternalSystemId { get; set; }
        public string TrailerNr { get; set; }


        #region FUNCTIONS
        public ReturnObject UpdateSyncStatus(int pId, string pTenderCode, EnumSyncStatus pSourceStatusCode, EnumSyncStatus pTargetSyncStatusCode)
        {
            ReturnObject retObj = new ReturnObject();

            using (ESubeContext db = new ESubeContext())
            {
                List<TenderAnswer> list = (from a in db.TenderAnswer where (pId == 0 ? 1 == 1 : a.Id == pId) && a.TenderCode == pTenderCode && a.SyncStatusCode == pSourceStatusCode select a).ToList();

                //TenderAnswer existsAnswer = (from p in db.TenderAnswer where p.TenderCode == pTenderCode && p.SyncStatusCode == EnumSyncStatus.SyncWaiting select p).FirstOrDefault();

                if (list != null)
                {
                    foreach (var item in list)
                    {

                        item.UpdateDate = DateTime.Now;

                        // sync statüsünü güncelle
                        item.SyncStatusCode = pTargetSyncStatusCode;

                    }

                    db.SaveChanges();
                    retObj.Result = true;
                    retObj.ResultMessage = "Sync statü güncellendi";
                }
            }

            return retObj;
        }

        public void DeleteTenderAnswer(string pTenderCode = "", string pAnsweredBy = "")
        {

            //Sadece şu tenderdaki şu answer silinsin isteniyor ise...
            if (!string.IsNullOrEmpty(pTenderCode) && !string.IsNullOrEmpty(pAnsweredBy))
            {
                using (ESubeContext db = new ESubeContext())
                {
                    TenderAnswer existsAnswer = (from p in db.TenderAnswer where p.TenderCode == pTenderCode && p.AnsweredBy == pAnsweredBy select p).SingleOrDefault();
                    //existsAnswer.Deleted = true;
                    //existsAnswer.UpdateDate = DateTime.Now;
                    db.SaveChanges();
                }


            }
            //Şu tender a ait tüm answerlar silinsin isteniyor ise...
            else if (!string.IsNullOrEmpty(pTenderCode) && string.IsNullOrEmpty(pAnsweredBy))
            {

                using (ESubeContext db = new ESubeContext())
                {
                    db.TenderAnswer
                      .Where(x => x.TenderCode == pTenderCode)
                      .ToList()
                      .ForEach(a =>
                      {
                              //a.Deleted = true;
                              //a.UpdateDate = DateTime.Now;
                          }
                                );
                    db.SaveChanges();
                }
            }



        }

        public void InsertTenderAnswer(TenderAnswer pTenderAnswer)
        {
            using (ESubeContext db = new ESubeContext())
            {
                db.TenderAnswer.Add(pTenderAnswer);
                db.SaveChanges();
            }
        }
        #endregion

    }
}