﻿using ESube.Service.Context;
using ESube.Service.Models.Enums;
using ESube.Service.Models.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace ESube.Service.Models
{
    /// <summary>
    /// Taşıma Talebi Uğrama Noktaları
    /// </summary>
    [Table("TenderStop")]
    public class TenderStop : BaseIntegrationClass
    {

        //Tender _Tender;
        //public Tender Tender
        //{
        //    get { return _Tender; }
        //    set { _Tender = value; }
        //}


        //Tender _Tender;
        //public Tender Tender
        //{
        //    get { return _Tender; }
        //    set { _Tender = value; }

        //}

        //[ForeignKey("TenderId"), DatabaseGenerated(DatabaseGeneratedOption.None)]
        //public int TenderId { get; set; }

        [IgnoreDataMember]
        public Tender tender { get; set; }

        public int ClaimId { get; set; }
        public string TenderCode { get; set; }
        public int StopNr { get; set; }
        public string StopType { get; set; }
        public string Name { get; set; }
        public string Street { get; set; }
        public string HouseNumber { get; set; }
        public string PostCode { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string CountryCode { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }


        #region FUNCTIONS
        public ReturnObject UpdateSyncStatus(string pTenderCode,EnumSyncStatus pSourceStatusCode, EnumSyncStatus pTargetSyncStatusCode)
        {
            ReturnObject retObj = new ReturnObject();

            using (ESubeContext db = new ESubeContext())
            {
                List<TenderStop> list = (from a in db.TenderStop where  a.TenderCode == pTenderCode && a.SyncStatusCode == pSourceStatusCode select a).ToList();


                if (list != null)
                {
                    foreach (var item in list)
                    {

                        item.UpdateDate = DateTime.Now;

                        // sync statüsünü güncelle
                        item.SyncStatusCode = pTargetSyncStatusCode;

                    }

                    db.SaveChanges();
                    retObj.Result = true;
                    retObj.ResultMessage = "Sync statü güncellendi";
                }
            }

            return retObj;
        }

        #endregion

        //public bool UpdateSyncStatusOld(string pTenderCode = "")
        //{
        //    bool result = false;

        //    //Sadece şu tenderdaki şu stop silinsin isteniyor ise...
        //    if (!string.IsNullOrEmpty(pTenderCode) && pStopNr > 0 )
        //    {
        //        try
        //        {
        //            using (ESubeContext db = new ESubeContext())
        //            {
        //                TenderStop existsStop = (from p in db.TenderStop where p.TenderCode == pTenderCode && p.StopNr == pStopNr select p).FirstOrDefault();
        //                existsStop.UpdateDate = DateTime.Now;
        //                db.SaveChanges();
        //                result = true;
        //            }
        //        }
        //        catch (Exception x)
        //        {
        //            result = false;
        //        }


        //    }
        //    //Şu tender a ait tüm stoplar silinsin isteniyor ise...
        //    else if (!string.IsNullOrEmpty(pTenderCode) && pStopNr > 0)
        //    {
        //        try
        //        {
        //            using (ESubeContext db = new ESubeContext())
        //            {
        //                db.TenderStop
        //                  .Where(x => x.TenderCode == pTenderCode && x.SyncStatusCode == EnumSyncStatus.SyncWaiting)
        //                  .ToList()
        //                  .ForEach(a =>
        //                                {
        //                                    //a.Deleted = true;
        //                                    a.UpdateDate = DateTime.Now;
        //                                }
        //                            );
        //                db.SaveChanges();
        //            }
        //            result = true;
        //        }
        //        catch (Exception x)
        //        {
        //            result = false;
        //        }
        //    }


        //    return result;

        //}

        public bool InsertTenderStop(string pTenderCode, List<TenderStop> pTenderStopList)
        {
            bool result = false;
            try
            {
                using (ESubeContext db = new ESubeContext())
                {
                    foreach (var item in pTenderStopList)
                    {
                        item.TenderCode = pTenderCode;
                        //item.TransferStatu = EnumTransferStatus.TransferWaiting;
                        db.TenderStop.Add(item);
                    }
                    db.SaveChanges();
                }
                result = true;
            }
            catch (Exception x)
            {
                result = false;
            }

            return result;
        }

    }
}