﻿using ESube.Service.Context;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using ESube.Service.Models.Helpers;
using ESube.Service.Models.Enums;

namespace ESube.Service.Models
{
    /// <summary>
    /// Taşıma Talebi
    /// </summary>
    [Table("Tender")]
    public class Tender : BaseIntegrationClass
    {
        public Tender()
        {
            StopCountLoading = 0;
            StopCountUnLoading = 0;

            stops = new List<TenderStop>();
            answers = new List<TenderAnswer>();
        }

        public string Code { get; set; }
        public string ExternalSystemId { get; set; }
        public string TypeCode { get; set; }
        public string PricingTypeCode { get; set; }
        public string Price { get; set; }
        public string Currency { get; set; }
        public string DeadLineDate { get; set; }
        public string ProductDescription { get; set; }
        public string MessageText { get; set; }
        public string StatusCode { get; set; }
        public string ShipmentType { get; set; }
        public string ShipmentCode { get; set; }
        public string TransporterCode { get; set; }
        public string CargoType { get; set; }
        public string TruckType { get; set; }
        public string CompanyName { get; set; }
        public string CompanyCode { get; set; }
        public string CompanyExternalSystemId { get; set; }

        public string CarrierName_1 { get; set; }
        public string CarriereExternalSystemId_1 { get; set; }
        public string CarrierContact_1 { get; set; }
        public string CarrierEmail_1 { get; set; }

        public string CarrierName_2 { get; set; }
        public string CarriereExternalSystemId_2 { get; set; }
        public string CarrierContact_2 { get; set; }
        public string CarrierEmail_2 { get; set; }

        public string CarrierName_3 { get; set; }
        public string CarriereExternalSystemId_3 { get; set; }
        public string CarrierContact_3 { get; set; }
        public string CarrierEmail_3 { get; set; }

        public string CarrierName_4 { get; set; }
        public string CarriereExternalSystemId_4 { get; set; }
        public string CarrierContact_4 { get; set; }
        public string CarrierEmail_4 { get; set; }

        public string CarrierName_5 { get; set; }
        public string CarriereExternalSystemId_5 { get; set; }
        public string CarrierContact_5 { get; set; }
        public string CarrierEmail_5 { get; set; }

        public string FromAddress { get; set; }
        public string FromAddressCode { get; set; }
        public string FromAddressName { get; set; }
        public string FromAddressCity { get; set; }
        public string FromAddressTown { get; set; }
        public string FromAddressStreet { get; set; }
        public string FromAddressPostCode { get; set; }
        public string FromAddressHouseNumber { get; set; }
        public string FromAddressCountryCode { get; set; }
        public string FromAddressExternalSystemId { get; set; }
        public string FromAddressLatitude { get; set; }
        public string FromAddressLongitude { get; set; }

        public string ToAddress { get; set; }
        public string ToAddressCode { get; set; }
        public string ToAddressName { get; set; }
        public string ToAddressCity { get; set; }
        public string ToAddressTown{ get; set; }
        public string ToAddressStreet { get; set; }
        public string ToAddressPostCode { get; set; }
        public string ToAddressHouseNumber { get; set; }
        public string ToAddressCountryCode { get; set; }
        public string ToAddressExternalSystemId { get; set; }
        public string ToAddressLatitude { get; set; }
        public string ToAddressLongitude { get; set; }

        public string TotalWeight { get; set; }
        public string TotalWeightBase { get; set; } 
        public string TotalVolume { get; set; }
        public string CalculatedKM { get; set; }
        public string ConfirmedKM { get; set; }
        public string RequestSentYN { get; set; }
        public int StopCountUnLoading { get; set; }
        public int StopCountLoading { get; set; }

        public string CreateUser { get; set; }
        public string UpdateUser { get; set; }

        public string LocationCode { get; set; }
        public string SEQ { get; set; }



        public virtual List<TenderStop> stops { get; set; }
        public virtual List<TenderAnswer> answers { get; set; }


        public List<Tender> GetListTender(string pCode, EnumSyncStatus pStatusCode)
        {
            var TenderList = new List<Tender>();

            using (ESubeContext db = new ESubeContext())
            {
               TenderList = db.Tender.Where(x=>x.Code.Contains(pCode) 
               && (pStatusCode == 0 ? 1 == 1 : x.SyncStatusCode == pStatusCode)
               ).OrderByDescending(x=> x.CreateDate).ToList();
            }

            return TenderList;
        }

        public Tender GetTender(int pId,string pCode)
        {
            var Tender = new Tender();

            using (ESubeContext db = new ESubeContext())
            {
               Tender = db.Tender.Where(x => x.Id == pId && x.Code == pCode).FirstOrDefault();

                if (Tender != null)
                {
                    Tender.stops = db.TenderStop.Where(y => y.TenderCode == Tender.Code && y.tender.Id== Tender.Id).ToList();
                    Tender.answers = db.TenderAnswer.Where(y => y.TenderCode == Tender.Code && y.tender.Id == Tender.Id).ToList();
                }

            }

            return Tender;
        }

        public void UpdateSyncStatus(int pId,string pTenderCode,EnumSyncStatus pSourceStatusCode ,EnumSyncStatus pTargetSyncStatusCode)
        {

            using (ESubeContext db = new ESubeContext())
            {
                Tender existsTender = (from a in db.Tender where (pId == 0 ? 1==1 : a.Id == pId) &&  a.Code == pTenderCode && a.SyncStatusCode == pSourceStatusCode select a).FirstOrDefault();

                if (existsTender != null)
                {
                    //Tenderı n sync statüsünü güncelle
                    existsTender.UpdateDate = DateTime.Now;
                    existsTender.SyncStatusCode = pTargetSyncStatusCode;
                    db.SaveChanges();

                    //Tender Stopların sync statülerini güncelle 
                    existsTender.stops = (from b in db.TenderStop where  b.tender.Id == existsTender.Id && b.TenderCode == pTenderCode &&  b.SyncStatusCode == pSourceStatusCode select b).ToList();

                    if (existsTender.stops != null)
                    {
                        existsTender.stops
                                .ToList()
                                .ForEach(a => {
                                    a.SyncStatusCode = pTargetSyncStatusCode;
                                    a.UpdateDate = DateTime.Now;
                                }
                            );
                        db.SaveChanges();
                    };

                    //Tender Answerlarının sync statülerini güncelle 
                    existsTender.answers = (from c in db.TenderAnswer where c.tender.Id == existsTender.Id && c.TenderCode == pTenderCode select c).ToList();

                    if (existsTender.answers != null)
                    {
                        existsTender.answers
                                .ToList()
                                .ForEach(a => {
                                    a.SyncStatusCode = pTargetSyncStatusCode;
                                    a.UpdateDate = DateTime.Now;
                                }
                            );
                        db.SaveChanges();
                    };


                    db.SaveChanges();
                }
            }

        }



        public ReturnObject InsertTender(Tender pTender)
        {
            ReturnObject retObj = new ReturnObject();

                using (ESubeContext db = new ESubeContext())
                {
                    //Tender _tender = pTender;
                    //Tender _tender = pTender.ShallowCopy();
                    //_tender.stops = null;
                    //_tender.answers = null;

                    pTender.CreateDate = DateTime.Now;
                    pTender.CreateId = 0;

                    if (pTender.stops.Count >0)
                    {
                        pTender.StopCountLoading = pTender.stops.Where(a => a.StopType == "LOADING").Count();
                        pTender.StopCountUnLoading = pTender.stops.Where(a => a.StopType == "UNLOADING").Count();
                        //pTender.TransferStatu = EnumTransferStatus.TransferWaiting;
                    }


                    db.Tender.Add(pTender);
                    db.SaveChanges();
                    retObj.Result = true;
                    retObj.ResultMessage = "Tender ve Stops Kaydedildi";
                }

            return retObj;
        }

        public void CreateTenderRelation(Tender pTender)
        {
            //Tender Stoplara Tender Code u yazar...
            if (pTender.stops != null)
            {
                pTender.stops
                        .ToList()
                        .ForEach(a => {
                            a.TenderCode = pTender.Code;
                        }
                        );
            };


            //Tender Answerlara Tender Code u yazar...
            if (pTender.answers != null)
            {
                pTender.answers
                        .ToList()
                        .ForEach(a => {
                            a.TenderCode = pTender.Code;
                        }
                );
            }

        }

        public List<Tender> GetListTenderAndDetails()
        {
            List<Tender> TenderList =new List<Tender>();


            using (ESubeContext db = new ESubeContext())
            {
                 TenderList = db.Tender.Where(x => x.SyncStatusCode == EnumSyncStatus.SyncWaiting).ToList();

                foreach (var item in TenderList)
                {
                    item.stops = db.TenderStop.Where(y => y.TenderCode == item.Code && y.SyncStatusCode == EnumSyncStatus.SyncWaiting).ToList();
                    item.answers = db.TenderAnswer.Where(y => y.TenderCode == item.Code && y.SyncStatusCode == EnumSyncStatus.SyncWaiting).ToList();
                }
            }

            return TenderList;

        }

    }
}