﻿using ESube.Service.Context;
using ESube.Service.Models;
using ESube.Service.Models.Enums;
using ESube.Service.Models.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Data.Entity;
using System.Linq;
using System.Data.SqlClient;
using ESube.Service.Helpers;
using Newtonsoft.Json;

namespace ESube.Service.Models
{
    [Table("Question")]

    public class Question : BaseEntityClass
    {
        public Question()
        {
            _QuestionHeader = new QuestionHeader();
            _QuestionAnswer = new List<QuestionAnswer>();
        }
        public virtual QuestionHeader _QuestionHeader { get; set; }
        public string QuestionText { get; set; }

        public List<QuestionAnswer> _QuestionAnswer { get; set; }


        #region FUNCTIONS

        public ReturnObject GetListQuestion()
        {

            ReturnObject retObj = new ReturnObject();

            try
            {
                using (ESubeContext _db = new ESubeContext())
                {
                    var List = _db.Question
                               .Include(x => x._QuestionAnswer)
                               .ToList();

                    retObj.Result = true;
                    retObj.Data = List;

                }
            }
            catch (Exception e)
            {

                retObj.Result = false;
                retObj.Data = null;
                retObj.ResultMessage = "ERROR: " + e.Message.ToString() + " - " + e.InnerException.Message.ToString();
            }

            return retObj;

        }
        #endregion
    }
}