﻿using ESube.Service.Context;
using ESube.Service.Models.Enums;
using ESube.Service.Models.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.Linq;

namespace ESube.Service.Models
{
    /// <summary>
    /// Ürün Barkodları
    /// </summary>
    [Table("ItemBarcode")]
    public class ItemBarcode : BaseIntegrationClass
    {
        public ItemBarcode() { }
        /// <summary>
        /// Ürün
        /// </summary>
        [IgnoreDataMember]
        public Item Item { get; set; }

        public string ItemCode { get; set; }

        /// <summary>
        /// Birim (Adet,Kutu,palet...)
        /// </summary>
        public int UnitType { get; set; }

        /// <summary>
        /// Barkod - Zorunlu
        /// </summary>
        [StringLength(50)]
        //[Required]
        public string Barcode { get; set; }

        /// <summary>
        /// Varsayılan mı ? 
        /// </summary>
        public bool IsDefault { get; set; }


    }
}