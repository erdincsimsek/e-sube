﻿//using ESube.Service.Context;
//using ESube.Service.Models;
//using ESube.Service.Models.Helpers;
//using System;
//using System.Collections.Generic;
//using System.ComponentModel;
//using System.ComponentModel.DataAnnotations;
//using System.ComponentModel.DataAnnotations.Schema;
//using System.Runtime.Serialization;
//using System.ServiceModel;
//namespace ESube.Service.Models
//{

//    /// <summary>
//    /// Sevk edilen sipariş geri dönüş objesi
//    /// </summary>
//    public class ShippedOrderResponse
//    {
//        public ShippedOrderResponse()
//        {
//            Lines = new List<ShippedOrderLine>();
//        }
//        /// <summary>
//        /// Firma kodu
//        /// </summary>
//        [StringLength(25)]
//        public string mainCompanyCode { get; set; }

//        /// <summary>
//        /// Depo kodu
//        /// </summary>
//        [StringLength(25)]
//        public string companyBranchCode { get; set; }

//        /// <summary>
//        /// Sevk İrsaliyesi Numarası
//        /// </summary>
//        [StringLength(50)]
//        public string despatchID { get; set; }
//        /// <summary>
//        /// Sevk İrsaliyesinin Asıl veya Suret Bilgisi Asıl ise false , kopya ise true
//        /// </summary>
//        [StringLength(10)]
//        public string CopyIndicator { get; set; }
//        /// <summary>
//        /// Sevk İrsaliyesinin Evrensel Tekliğini Sağlayan Numara (ETTN)
//        /// </summary>
//        [StringLength(50)]
//        public string UUID { get; set; }
//        /// <summary>
//        ///  İrsaliye Düzenleme Tarihi  YYYY-MM-DD
//        /// </summary>
//        [StringLength(10)]
//        public string IssueDate { get; set; }
//        /// <summary>
//        /// İrsaliye Düzenleme zamanı  HH:MM:SS
//        /// </summary>
//        [StringLength(10)]
//        public string IssueTime { get; set; }
//        /// <summary>
//        /// Sevk İrsaliyesinin Tip Kodu (SEVK/MATBUDAN)
//        /// </summary>
//        [StringLength(10)]
//        public string DespatchAdviceTypeCode { get; set; }
//        /// <summary>
//        /// Sevk İrsaliyesi ile ilgili genel not
//        /// </summary>
//        [StringLength(200)]
//        public string Note { get; set; }
//        /// <summary>
//        /// İrsaliyedeki satır sayısı
//        /// </summary>
//        [StringLength(5)]
//        public int LineCountNumeric { get; set; }

//        // LineCountNumeric Sevk İrsaliyesi Kalem Sayısı
//        /// <summary>
//        /// Sipariş ürün detayı
//        /// </summary>
//        public virtual List<ShippedOrderLine> Lines { get; set; }
//        /// <summary>
//        /// Sipariş Bilgileri
//        /// </summary>
//        public virtual List<ShippedOrderReference> OrderReference { get; set; }
//    }
//}