﻿using ESube.Service.Context;
using ESube.Service.Models.Enums;
using ESube.Service.Models.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.Linq;

namespace ESube.Service.Models
{
    /// <summary>
    /// Parametreler
    /// </summary>
    [Table("IntegrationEnumType")]
    public class IntegrationEnumType
    {
        public IntegrationEnumType() { }
        long _Id;
        [IgnoreDataMember]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id
        {
            get { return _Id; }
            set { _Id = value; }

        }
        /// <summary>
        /// Tip
        /// </summary>
        public string MapType { get; set; }

        /// <summary>
        /// Entegrasyon Kodu
        /// </summary>
       // [StringLength(50)]
        public string IntegrationValue { get; set; }

        /// <summary>
        /// Map Değeri
        /// </summary>
        public string MapValue { get; set; }

    }
}