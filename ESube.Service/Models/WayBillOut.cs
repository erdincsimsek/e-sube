﻿using ESube.Service.Context;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ESube.Service.Models
{
    [Table("vw_WayBillGoodsOut")]
    public class WayBillGoodsOut
    {
        [Key]
        public long Id { get; set; }
        public string CompanyCode { get; set; }
        public string ShipmentCode { get; set; }
        public string WayBillCode { get; set; }
        public string OrderCode { get; set; }
        public string OrderDispatchCurrentCode { get; set; }

        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public string DelivredCount { get; set; }
        public string UnitSet { get; set; }
        public string LotNumber { get; set; }


        public List<WayBillGoodsOut> GetList(string pCompanyCode, string pOrderCode)
        {
            using (var db = new ESubeContext())
            {
                //var list = db.WayBillGoodsOut.Where(x => x.CompanyCode ==  pCompanyCode &&  x.OrderCode == pOrderCode).ToList();

                return null;
            }
        }


    }
}