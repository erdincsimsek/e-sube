﻿using ESube.Service.Context;
using ESube.Service.Models;
using ESube.Service.Models.Enums;
using ESube.Service.Models.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Linq;
using ESube.Service.Models.Enums;
using ESube.Service.Models.Helpers;

namespace ESube.Service.Models
{
    /// <summary>
    /// Sipariş satırları
    /// </summary>
    [Table("OrderLine")]
    public class OrderLineNew : BaseIntegrationClass
    {
        public OrderLineNew()
        {
            Item = new Item();
            RecordType = EnumRecordType.New;
        }
        [IgnoreDataMember]
        public OrderNew Order { get; set; }



        [IgnoreDataMember]
        public string IntegrationCode { get; set; }
   
        /// <summary>
        /// Satır numarası
        /// </summary>
        [StringLength(400)]
        public string LineCode { get; set; }

        /// <summary>
        /// Yük Miktarı
        /// </summary>
        public double? Quantity { get; set; }

        /// <summary>
        /// Ürün Bilgileri
        /// </summary>
        /// 
        public string LotCode { get; set; }

        public virtual Item Item { get; set; }

        public EnumRecordType RecordType { get; set; }

       // [Required]
        public EnumContainerType ContainerType { get; set; }


        public string ProductionDate { get; set; }
        public string ExpireDate { get; set; }
        public string PackageBarcode { get; set; }
        public string PaletteBarcode { get; set; }

        public OrderLineNew GetOrderLineNew(int pOrderLineNewId)
        {
            OrderLineNew _OrderLineNew = new OrderLineNew();

            using (ESubeContext db = new ESubeContext())
            {
                _OrderLineNew = (from x in db.OrderLineNew where x.Id == pOrderLineNewId select x).FirstOrDefault();
            }

            return _OrderLineNew;
        }

        public OrderLineNew GetOrderLine(int pOrderLineId)
        {
            OrderLineNew _OrderLine = new OrderLineNew();

            using (ESubeContext db = new ESubeContext())
            {
                _OrderLine = (from x in db.OrderLineNew where x.Id == pOrderLineId select x).FirstOrDefault();
            }

            return _OrderLine;
        }


        //public OrderLine GetOrderLine(int pOrderLineId)
        //{
        //    OrderLine _OrderLine = new OrderLine();

        //    using (ESubeContext db = new ESubeContext())
        //    {
        //        _OrderLine = (from x in db.OrderLine where x.Id == pOrderLineId select x).FirstOrDefault();
        //    }

        //    return _OrderLine;
        //}



    }
}