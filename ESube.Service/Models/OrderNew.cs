﻿using ESube.Service.Context;
using ESube.Service.Models;
using ESube.Service.Models.Enums;
using ESube.Service.Models.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Linq;
using System.Data.SqlClient;
using ESube.Service.Helpers;
using Newtonsoft.Json;
using ESube.Service.Models.Entegrasyon;

namespace ESube.Service.Models
{
    /// <summary>
    /// Sipariş nesnesi
    /// </summary>
    [Table("Order")]
    public class OrderNew : BaseIntegrationClass
    {
        public OrderNew()
        {
            Lines = new List<OrderLineNew>();
            CompanyList = new List<Company>();
            RecordType = EnumRecordType.New;

        }

        /// <summary>
        /// Firma kodu
        /// </summary>
        [IgnoreDataMember]
        public string MainCompanyCode { get; set; }// ÖR: Hemel in kodu

        /// <summary>
        /// Sipariş tipi
        /// </summary>
        public EnumOrderType OrderType { get; set; }

        [IgnoreDataMember,NotMapped]
        public string OrderTypeDesc { get { return (Enum.GetName(typeof(EnumOrderType), OrderType)); } set { } }

        /// <summary>
        /// Depo kodu
        /// </summary>
        [StringLength(25)]
        public string WarehouseCode { get; set; } // ÖR: A25 nolu depo


        /// <summary>
        /// Sipariş tarihi
        /// </summary>
        [StringLength(20)]
        public string OrderDate { get; set; }

        /// <summary>
        /// Sipariş numarası
        /// </summary>
        [StringLength(50)]
        public string OrderCode { get; set; }


        /// <summary>
        /// Siparişin Id bilgisi
        /// </summary>
        [StringLength(50)]
        public string OrderId { get; set; }


        /// <summary>
        /// Müşteri sipariş kodu
        /// </summary>
        [StringLength(50)]
        public string CustomerOrderCode { get; set; }


        /// <summary>
        /// İlişkili  siparişin numarası 
        /// </summary>
        [StringLength(50)]
        public string RelatedOrderCode{ get; set; }

        /// <summary>
        /// İrsaliye numarası
        /// </summary>
        [StringLength(50)]
        public string WaybillCode { get; set; }

        /// <summary>
        /// Planlanmış teslim tarihi
        /// dd/mm/yyyy hh:mm
        /// </summary>
        //[Required]
        [StringLength(20)]
        public string PlannedDeliveryDate { get; set; }

        /// <summary>
        /// Sipariş önceliği
        /// </summary>
       [StringLength(2)]
        public string OrderPriority { get; set; }

        /// <summary>
        /// Siparişi yükleme tipi
        /// </summary>
        public EnumLoadingType LoadingType { get; set; }

       

        /// <summary>
        /// Sipariş alt tipi
        /// </summary>
        public string OrderSubType { get; set; }
        //[IgnoreDataMember,NotMapped]
        //public string OrderSubTypeDesc { get { return (Enum.GetName(typeof(EnumOrderTypeReason), OrderSubType)); } set { } }

        /// <summary>
        /// Sevk numarası
        /// </summary>
        [StringLength(400)]
        public string ShipmentCode { get; set; }

        /// <summary>
        /// Entegrasyon aktarım tarihi
        /// </summary>
        [IgnoreDataMember]
        public string IntegrationDate { get; set; }

        /// <summary>
        /// Firma bilgileri
        /// </summary>
        public virtual List<Company> CompanyList { get; set; }

        /// <summary>
        /// Tek seferlik kullanılan adres
        /// </summary>
        [StringLength(70)]
        public string TempAddress { get; set; }

        /// <summary>
        /// Sipariş satırları
        /// </summary>
        public virtual List<OrderLineNew> Lines { get; set; }

        /// <summary>
        ///  İşlem türü
        /// </summary>
        public EnumRecordType RecordType { get; set; }

        public string TransactionCode { get; set; }

        /// <summary>
        /// Yük Cins Bilgisi
        /// </summary>
        //[Required]
        public EnumGoodsTypeId GoodsTypeId { get; set; }

        /// <summary>
        /// Yükleme Tarihi
        /// </summary>
        //[Required]
        public string LoadingDate { get; set; }

        /// <summary>
        /// Taşıma Türü Bilgisi
        /// </summary>
        //[Required]
        public EnumTransportTypeCode TransportTypeCode { get; set; }



        public string GetListByOrder(int pOrderId,string pTableName)
        {
            dynamic resultList = new object();

            using (ESubeContext db = new ESubeContext())
            {

                // SP den al
                List<SqlParameter> paramlist = new List<SqlParameter>();
                paramlist.Add(new SqlParameter("pOrderId", pOrderId));
                paramlist.Add(new SqlParameter("pTableName", pTableName));

                if (pTableName =="Company") {
                    resultList = db.Database.SqlQuery<Company>("Sp_Get_Data_By_Table_Name @pOrderId,@pTableName", paramlist.ToArray()).ToList();
                }
                else if (pTableName == "OrderLine")
                {
                    resultList = db.Database.SqlQuery<OrderLineNew>("Sp_Get_Data_By_Table_Name @pOrderId,@pTableName", paramlist.ToArray()).ToList();
                }
                else if (pTableName == "Item")
                {
                    resultList = db.Database.SqlQuery<Item>("Sp_Get_Data_By_Table_Name @pOrderId,@pTableName", paramlist.ToArray()).ToList();
                }
                else if (pTableName == "ItemBarcode")
                {
                    resultList = db.Database.SqlQuery<ItemBarcode>("Sp_Get_Data_By_Table_Name @pOrderId,@pTableName", paramlist.ToArray()).ToList();
                }
                else if (pTableName == "ItemEachCustomer")
                {
                    resultList = db.Database.SqlQuery<ItemEachCustomer>("Sp_Get_Data_By_Table_Name @pOrderId,@pTableName", paramlist.ToArray()).ToList();
                }

                else if (pTableName == "Status")
                {
                   
                    resultList = db.Database.SqlQuery<EntStatus>("Sp_Get_Data_By_Table_Name @pOrderId,@pTableName", paramlist.ToArray()).ToList();   
                }
                else if (pTableName == "Waybill")
                {

                    resultList = db.Database.SqlQuery<EntWaybillStatus>("Sp_Get_Data_By_Table_Name @pOrderId,@pTableName", paramlist.ToArray()).ToList();
                }


                //Ignore Propertylerin Ignore larını temizlemek için
                JsonSerializerSettings settingsXX = new JsonSerializerSettings();
                settingsXX.ContractResolver = new IgnoreJsonAttributesResolver();

                var json = JsonConvert.SerializeObject(resultList, settingsXX);


                return json;
            }
        }

        public List<OrderNew> GetListAll(string pCompanyCode, string pOrderCode, EnumOrderType pOrderType, EnumSyncStatus pOrderStatusCode)
        {
            List<OrderNew> _List = new List<OrderNew>();

            using (ESubeContext db = new ESubeContext())
            {
                _List = (from p in db.OrderNew.Where(x =>
                     (pOrderType == EnumOrderType.None ? 1 == 1 : x.OrderType == pOrderType) &&
                    (string.IsNullOrEmpty(pCompanyCode) ? 1 ==1 : x.MainCompanyCode == pCompanyCode) 
                    && (x.OrderCode.Contains(pOrderCode) || x.ShipmentCode.Contains(pOrderCode)) 
                    && (pOrderStatusCode == EnumSyncStatus.None ? 1 == 1 : x.SyncStatusCode == pOrderStatusCode)
                ) select p).OrderByDescending(x => x.CreateDate).Take(1000).ToList();
            }


            return _List;
        }

        public OrderNew GetOrderWithDetail(string pMainCompanyCode, long pId, string pOrderCode)
        {
            OrderNew _Order = new OrderNew();

            using (ESubeContext db = new ESubeContext())
            {
                _Order = (from x in db.OrderNew where (pId == 0 ? 1 == 1 : x.Id == pId) && (pId == 0 ? x.OrderCode == pOrderCode:1==1 )&& x.MainCompanyCode == pMainCompanyCode select x).FirstOrDefault();

                _Order.Lines = (from y in db.OrderLineNew where y.Order.Id == _Order.Id select y).ToList();

            }

            return _Order;
        }
        public void UpdateTransferStatus(string pMainCompanyCode, int pOrderId, string pOrderCode, string pOrderType)
        {
            using (ESubeContext db = new ESubeContext())
            {

           
            List<SqlParameter> paramlist = new List<SqlParameter>();
            paramlist.Add(new SqlParameter("pMainCompanyCode", pMainCompanyCode));
            paramlist.Add(new SqlParameter("pOrderId", Convert.ToInt32(pOrderId)));
            paramlist.Add(new SqlParameter("pOrderCode", string.IsNullOrEmpty(pOrderCode) == true ? "" : pOrderCode.ToString()));
            paramlist.Add(new SqlParameter("pOrderType", pOrderType.ToString()));

            db.Database.ExecuteSqlCommand("EXEC Sp_Update_Order_TransferStatus_ALL @pMainCompanyCode,@pOrderId,@pOrderCode,@pOrderType", paramlist.ToArray());
            }
        }

        public void UpdateSyncStatus(string pMainCompanyCode, int pOrderId, string pOrderCode, string pTableName, string pUniqueDateStr, EnumSyncStatus pSyncStatus)
        {

            //[Sp_Update_Order_SyncStatus]

            using (ESubeContext db = new ESubeContext())
            {
                List<SqlParameter> paramlist = new List<SqlParameter>();
                paramlist.Add(new SqlParameter("pMainCompanyCode", pMainCompanyCode));
                paramlist.Add(new SqlParameter("pOrderId", Convert.ToInt32(pOrderId)));
                paramlist.Add(new SqlParameter("pOrderCode", string.IsNullOrEmpty(pOrderCode)==true ?"":pOrderCode.ToString()));
                paramlist.Add(new SqlParameter("pTableName", pTableName));
                paramlist.Add(new SqlParameter("pUniqueDateStr", pUniqueDateStr));
                paramlist.Add(new SqlParameter("pSyncStatusCode",Convert.ToInt32(pSyncStatus)));
                paramlist.Add(new SqlParameter("pSyncStatus",(((EnumSyncStatus)pSyncStatus).ToString())));

                db.Database.ExecuteSqlCommand("EXEC Sp_Update_Order_SyncStatus @pMainCompanyCode,@pOrderId,@pOrderCode,@pTableName,@pUniqueDateStr,@pSyncStatusCode,@pSyncStatus", paramlist.ToArray());
            }




            //public List<Order> GetListAll(string pCompanyCode, string pOrderCode, EnumOrderType pOrderType, EnumSyncStatus pOrderStatusCode)
            //{
            //    List<Order> _List = new List<Order>();

            //    using (ESubeContext db = new ESubeContext())
            //    {
            //        _List = (from p in db.Order.Where(x => x.mainCompanyCode == pCompanyCode &&  x.orderCode.Contains(pOrderCode) && x.orderType == pOrderType && (pOrderStatusCode == 0 ? 1 == 1 : x.SyncStatusCode == pOrderStatusCode)) select p).Take(1000).OrderByDescending(x => x.CreateDate).ToList();
            //    }

            //    return _List;
            //}

            //public Order GetOrderWithDetail(string pMainCompanyCode, long pId,string pOrderCode)
            //{
            //    Order _Order = new Order();

            //    using (ESubeContext db = new ESubeContext())
            //    {
            //        _Order = (from x in db.Order where (pId == 0 ? 1 == 1 : x.Id == pId) && x.orderCode == pOrderCode && x.mainCompanyCode == pMainCompanyCode select x).FirstOrDefault();

            //        _Order.Lines= (from y in db.OrderLineNew where y.order.Id == _Order.Id select y).ToList();

            //    }

            //    return _Order;
            //}




            //public void UpdateSyncStatus(string pCompanyCode, int pOrderId, string pOrderCode, EnumSyncStatus pSyncStatus)
            //{
            //    ReturnObject retObj = new ReturnObject();

            //    using (ESubeContext db = new ESubeContext())
            //    {
            //        Order existsOrder = (from p in db.Order where p.mainCompanyCode == pCompanyCode && (pOrderId == 0 ? 1==1 : pOrderId == p.Id ) && p.orderCode == pOrderCode select p).FirstOrDefault();

            //        if (existsOrder != null)
            //        {

            //            existsOrder.Lines = (from p in db.OrderLine where p.order.Id == existsOrder.Id select p).ToList();

            //            existsOrder.UpdateDate = DateTime.Now;

            //            //Order ın sync statüsünü güncelle
            //            existsOrder.SyncStatusCode = pSyncStatus;

            //            //Tender Stopların sync statülerini güncelle 
            //            if (existsOrder.Lines != null)
            //            {
            //                existsOrder.Lines
            //                        .ToList()
            //                        .ForEach(a => {
            //                            a.SyncStatusCode = pSyncStatus;
            //                            a.UpdateDate = DateTime.Now;
            //                        }
            //                        );
            //            };


            //            db.SaveChanges();
            //        }
            //    }

            //}

            //public void InsertOrder(Order pOrder)
            //{
            //    using (ESubeContext db = new ESubeContext())
            //        {
            //            pOrder.integrationDate = DateTime.Now;

            //            //pOrder.CreateDate = DateTime.Now;
            //            //Tender Stopların sync statülerini güncelle 
            //            //if (pOrder.Lines != null)
            //            //{
            //            //    pOrder.Lines
            //            //            .ToList()
            //            //            .ForEach(a => {
            //            //                a.SyncStatusCode = EnumSyncStatus.SyncWaiting;
            //            //                a.SyncStatus = (Enum.GetName(typeof(EnumSyncStatus), SyncStatusCode);
            //            //                a.CreateDate = DateTime.Now;
            //            //            }
            //            //            );
            //            //};

            //            db.Order.Add(pOrder);
            //            db.SaveChanges();
            //        }
            //}

            //public void DeleteOrder(string pOrderCode, string pCompanyBranchCode,string pMainCompanyCode)
            //{

            //    Order _Order = new Order();
            //    _Order.UpdateDate = DateTime.Now;
            //    _Order.SyncStatusCode = EnumSyncStatus.Delete;

            //    _Order.mainCompanyCode = pMainCompanyCode;//Firma
            //    _Order.companyBranchCode = pCompanyBranchCode;//hangi depodaki sipariş...
            //    _Order.orderCode = pOrderCode;

            //    using (ESubeContext db = new ESubeContext())
            //    {

            //        //_Order.orderType = (buraya var olan siparişin tipini çek);

            //        db.Order.Add(_Order);
            //        db.SaveChanges();
            //    }

            //}


            //public EnumOrderType GetOrderType(string pCompanyCode,string pOrderCode)
            //{

            //    using (ESubeContext db = new ESubeContext())
            //    {

            //        EnumOrderType orderType = EnumOrderType.None;

            //        //Sipaş türü bulunamaz ise sıfır döndüğü için, bu siparişe ait kayıt sayısını al dedim...
            //        int existstCount = (from x in db.Order where x.mainCompanyCode == pCompanyCode && x.orderCode == pOrderCode select  x.orderType).Count();


            //        if (existstCount > 0)
            //        {
            //            orderType = (from x in db.Order where x.mainCompanyCode == pCompanyCode && x.orderCode == pOrderCode select x.orderType).FirstOrDefault();
            //        }


            //        return orderType;

            //    }

            //}


            //ReturnObject retObj = new ReturnObject();

            //using (ESubeContext db = new ESubeContext())
            //{
            //    Order existsOrder = (from p in db.Order where p.MainCompanyCode == pCompanyCode && (pOrderId == 0 ? 1 == 1 : pOrderId == p.Id) && p.OrderCode == pOrderCode select p).FirstOrDefault();

            //    if (existsOrder != null)
            //    {

            //        existsOrder.Lines = (from p in db.OrderLine where p.order.Id == existsOrder.Id select p).ToList();

            //        existsOrder.UpdateDate = DateTime.Now;

            //        //Order ın sync statüsünü güncelle
            //        existsOrder.SyncStatusCode = pSyncStatus;

            //        //Tender Stopların sync statülerini güncelle 
            //        if (existsOrder.Lines != null)
            //        {
            //            existsOrder.Lines
            //                    .ToList()
            //                    .ForEach(a => {
            //                        a.SyncStatusCode = pSyncStatus;
            //                        a.UpdateDate = DateTime.Now;
            //                    }
            //                    );


            //            //satırlardaki ürünleri null yaptım çünkü null olmazsa, ürünü arıyor...
            //            foreach (var x in  existsOrder.Lines)
            //            {
            //                x.Item = null;
            //            }


            //            //ürünleri ayrıca güncelliyorum
            //            List<Item>  ItemList  = (from b in db.Item where b.Id == existsOrder.Id select p).ToList();





            //        };




            //        db.SaveChanges();
            //    }
            //}

        }

        public void InsertOrder(OrderNew pOrder)
        {
            using (ESubeContext db = new ESubeContext())
            {
                db.OrderNew.Add(pOrder);
                db.SaveChanges();
            }
        }

        public ReturnObject InsertOrderBefore(OrderNew pOrder)
        {

            ReturnObject _RetObj = new ReturnObject();
            _RetObj.Result = true;
            _RetObj.ResultMessage = "";

            //Ana firma kodunu siparişin içindeki tüm classlara ekliyorum.
            foreach (var x in pOrder.Lines)
            {
                if (x != null)
                {

                    if (x.Item != null)
                    {
                        x.Item.MainCompanyCode = pOrder.MainCompanyCode;
                        if (x.Item.ItemEachCustomerList != null)
                        {
                            foreach (var y in x.Item.ItemEachCustomerList)
                            {
                                if (y != null)
                                {
                                    y.MainCompanyCode = pOrder.MainCompanyCode;
                                }
                            }
                        }


                    }
                }

            }

            //Tüm validation işlemleri...
            _RetObj = pOrder.ValidateOrder(pOrder);
    
            //ÜRÜN ENTEGRASYONU BAZI DURUMLARDA YAPILMAZ (Statülerini aktarılmışa çekiyorum)
            //Hemel de çıkış siparişlerinde
            //Polisan da tüm siparişlerde
            if ((pOrder.MainCompanyCode == "HEMEL" && pOrder.OrderType == EnumOrderType.GoodsOUT)||pOrder.MainCompanyCode =="PLSN01"){
                foreach (var x in pOrder.Lines){
                    if (x!= null){
                        x.Item.SyncStatusCode = EnumSyncStatus.SyncSuccess;
                    }
                }
            }

            //SORUN YOKSA
            if (_RetObj.Result){
                //Gelen Sipariş Koduna Ait Henüz Transfer Edilmemiş Sipariş ve Detay Bilgileri var ise Transferlerini İptal Edilmiş Olarak güncelle !
                pOrder.UpdateSyncStatus(pOrder.MainCompanyCode, 0, pOrder.OrderCode, "", "", EnumSyncStatus.SyncCancelled);
            }

            return _RetObj;

        }

        public void DeleteOrder(string pOrderCode, string pCompanyBranchCode, string pMainCompanyCode)
        {

            OrderNew _Order = new OrderNew();
            _Order.UpdateDate = DateTime.Now;
            _Order.SyncStatusCode = EnumSyncStatus.Delete;

            _Order.MainCompanyCode = pMainCompanyCode;//Firma
            _Order.WarehouseCode = pCompanyBranchCode;//hangi depodaki sipariş...
            _Order.OrderCode = pOrderCode;

            using (ESubeContext db = new ESubeContext())
            {

                //_Order.orderType = (buraya var olan siparişin tipini çek);

                db.OrderNew.Add(_Order);
                db.SaveChanges();
            }

        }


        public EnumOrderType GetOrderType(string pCompanyCode, string pOrderCode)
        {

            using (ESubeContext db = new ESubeContext())
            {

                EnumOrderType orderType = EnumOrderType.None;

                //Sipaş türü bulunamaz ise sıfır döndüğü için, bu siparişe ait kayıt sayısını al dedim...
                int existstCount = (from x in db.OrderNew where x.MainCompanyCode == pCompanyCode && x.OrderCode == pOrderCode select x.OrderType).Count();


                if (existstCount > 0)
                {
                    orderType = (from x in db.OrderNew where x.MainCompanyCode == pCompanyCode && x.OrderCode == pOrderCode select x.OrderType).FirstOrDefault();
                }


                return orderType;

            }

        }

        public ReturnObject ValidateOrder(OrderNew pOrder)
        {

            ReturnObject _RetObj = new ReturnObject();
            _RetObj.Result = true;
            _RetObj.ResultMessage = "";

            //İŞ KURAL KONTROLLERİ
            if (pOrder.MainCompanyCode == "PLSN01")
            {
                if (pOrder.CompanyList != null)
                {
                    Company senderCmp = pOrder.CompanyList.Where(x => x.OrderRoleType == CompanyOrderRoleType.Sender).FirstOrDefault();
                    Company receiverCmpy = pOrder.CompanyList.Where(x => x.OrderRoleType == CompanyOrderRoleType.Receiver).FirstOrDefault();

                    //AR 10 lu siparişlerde siparişi gönderenin vergi numarası asla boş olamaz
                    if (receiverCmpy != null){
                        if (receiverCmpy.CompanyCode.ToLower().Contains("ar10")&&(string.IsNullOrEmpty(senderCmp.TaxNumber))){
                            _RetObj.ResultMessage = "AR10 lu siparişlerde, siparişi verenin vergi numarası boş olamaz";
                            _RetObj.Result = false;
                        }
                    }

                }

            }

            //SORUN YOKSA İŞ KURALLARININ AXATA TARAFINDA OLANLARI DA KONTROL ET
            if (_RetObj.Result){
                using (ESubeContext db = new ESubeContext())
                {
                    //List<SqlParameter> paramlist = new List<SqlParameter>();
                    //paramlist.Add(new SqlParameter("pMainCompanyCode", pOrder.MainCompanyCode));
                    //paramlist.Add(new SqlParameter("pOrderCode", pOrder.OrderCode));

                    var sqlResult = db.Database.SqlQuery<ValidateObject>("EXEC Sp_Validate_Order @pMainCompanyCode={0},@pOrderCode={1},@pOrderType={2}", pOrder.MainCompanyCode, pOrder.OrderCode,pOrder.OrderType).ToList();


                    if (sqlResult != null)
                    {
                        _RetObj.Result = Convert.ToBoolean(sqlResult[0].Result);
                        _RetObj.ResultMessage = sqlResult[0].ResultMessage;
                    }
                    else
                    {
                        _RetObj.Result = false;
                        _RetObj.ResultMessage = "Error occurred";
                    }

                }
            }

            return _RetObj;

        }

        public void MapOldOrderToNewOrder(Order _OldOrder,OrderNew _NewOrder)
        {
            //Siparişi veren ve alan diye firmalar oluşturabilmek için
            Company cSender = new Company();
            Company cReceiver = new Company();

            if (!string.IsNullOrEmpty(_OldOrder.companyName)){
                //Giriş siparişleri
                if (_OldOrder.orderType == EnumOrderType.GoodsIN)
                {
                    //Satın alma siparişi(1 veya 3)
                    if (_OldOrder.orderTypeReason == EnumOrderTypeReason.Purchase || _OldOrder.orderTypeReason == EnumOrderTypeReason.General)
                    {

                        _OldOrder.orderTypeReason = EnumOrderTypeReason.Purchase;


                        cSender.MainCompanyCode = _OldOrder.mainCompanyCode;
                        cSender.CompanyCode = _OldOrder.companyCode;
                        cSender.CompanyType = EnumCompanyType.Supplier;
                        cSender.OrderRoleType = CompanyOrderRoleType.Sender;
                        cSender.Name = _OldOrder.companyName;
                        cSender.TaxNumber = _OldOrder.companyTaxNumber;
                        cSender.TaxDepartmentName = _OldOrder.companyTaxDepartmentName;
                        cSender.CityName = _OldOrder.companyCityName;
                        cSender.TownName = _OldOrder.companyTownName;


                        cSender.AddressText = _OldOrder.companyAddressText;
                        cSender.GLN = _OldOrder.companyGLN;
                        cSender.IsDispatcher = _OldOrder.IsDispatcher == "E" ? true : false;
                        cSender.Phone = _OldOrder.companyPhone;
                        cSender.PostCode = _OldOrder.companyPostCode;
                        cSender.ContactCode = _OldOrder.companyContactCode;
                        cSender.ContactName = _OldOrder.companyContactFirstName + " " + _OldOrder.companyContactLastName;
                        cSender.ContactMail = _OldOrder.companyContactMail;
                        cSender.ContactPhone = _OldOrder.companyContactMobilePhoneNumber + " " + _OldOrder.companyContactPhoneNumber;
                        cSender.UpdateDate = null;



                    }
                    //iade(2)
                    else if (_OldOrder.orderTypeReason == EnumOrderTypeReason.Return)
                    {
                        cSender.MainCompanyCode = _OldOrder.mainCompanyCode;
                        cSender.CompanyCode = "PLS_DNC";
                        cSender.CompanyType = EnumCompanyType.Customer;
                        cSender.OrderRoleType = CompanyOrderRoleType.Receiver;
                        cSender.SyncStatusCode = EnumSyncStatus.SyncSuccess;

                        cReceiver.MainCompanyCode = _OldOrder.mainCompanyCode;
                        cReceiver.CompanyCode = "PLS_DNC";
                        cReceiver.CompanyType = EnumCompanyType.Customer;
                        cReceiver.OrderRoleType = CompanyOrderRoleType.Receiver;
                        cSender.SyncStatusCode = EnumSyncStatus.SyncSuccess;


                    }

                    //Mix Ürün Girişi(8)
                    else if (_OldOrder.orderTypeReason == EnumOrderTypeReason.GoodsInMix)
                    {
                        cSender.MainCompanyCode = _OldOrder.mainCompanyCode;
                        cSender.CompanyCode = _OldOrder.companyBranchCode;
                        cSender.CompanyType = EnumCompanyType.Warehouse;
                        cSender.OrderRoleType = CompanyOrderRoleType.Sender;
                        cSender.SyncStatusCode = EnumSyncStatus.SyncSuccess;

                        cReceiver.MainCompanyCode = _OldOrder.mainCompanyCode;
                        cReceiver.CompanyCode = _OldOrder.companyBranchCode;
                        cReceiver.CompanyType = EnumCompanyType.Warehouse;
                        cReceiver.OrderRoleType = CompanyOrderRoleType.Receiver;
                        cReceiver.SyncStatusCode = EnumSyncStatus.SyncSuccess;

                    }
                    //Depolar arası transfer girişi(12)
                    else if (_OldOrder.orderTypeReason == EnumOrderTypeReason.TransferBetweenDepotsin)
                    {
                        cSender.MainCompanyCode = _OldOrder.mainCompanyCode;
                        cSender.CompanyCode = _OldOrder.companyBranchCode;
                        cSender.CompanyType = EnumCompanyType.Warehouse;
                        cSender.OrderRoleType = CompanyOrderRoleType.Sender;
                        cSender.SyncStatusCode = EnumSyncStatus.SyncSuccess;

                        cReceiver.MainCompanyCode = _OldOrder.mainCompanyCode;
                        cReceiver.CompanyCode = _OldOrder.companyCode;
                        cReceiver.CompanyType = EnumCompanyType.Warehouse;
                        cReceiver.OrderRoleType = CompanyOrderRoleType.Receiver;
                        cReceiver.SyncStatusCode = EnumSyncStatus.SyncSuccess;


                    }

                }
                //Çıkış siparişleri
                else if (_OldOrder.orderType == EnumOrderType.GoodsOUT)
                {
                    //Gelen(3) veya Satış siparişi(10) 
                    if (_OldOrder.orderTypeReason == EnumOrderTypeReason.SalesOrder || _OldOrder.orderTypeReason == EnumOrderTypeReason.General)
                    {
                        _OldOrder.orderTypeReason = EnumOrderTypeReason.SalesOrder;
                        if (_OldOrder.companyCode.ToLower() == "ar10")
                        {

                            cSender.MainCompanyCode = _OldOrder.mainCompanyCode;
                            cSender.CompanyType = EnumCompanyType.Customer;
                            cSender.TaxNumber = _OldOrder.companyTaxNumber;
                            cSender.TaxDepartmentName = _OldOrder.companyTaxDepartmentName;
                            cSender.CityName = _OldOrder.companyCityName;
                            cSender.TownName = _OldOrder.companyTownName;
                            cSender.OrderRoleType = CompanyOrderRoleType.Sender;
                            cSender.SyncStatusCode = EnumSyncStatus.SyncSuccess;



                            cReceiver.MainCompanyCode = _OldOrder.mainCompanyCode;
                            cReceiver.CompanyType = EnumCompanyType.Customer;
                            cReceiver.CompanyCode = _OldOrder.companyCode;
                            cReceiver.Name = _OldOrder.companyName;
                            cReceiver.AddressText = _OldOrder.companyAddressText;
                            cReceiver.CityCode = _OldOrder.companyCityCode;
                            cReceiver.CityName = _OldOrder.companyCityName;
                            cReceiver.TownName = _OldOrder.companyTownName;
                            cReceiver.AddressText = _OldOrder.companyAddressText;
                            cReceiver.OrderRoleType = CompanyOrderRoleType.Receiver;
                            cReceiver.SyncStatusCode = EnumSyncStatus.SyncSuccess;
                        }
                        else
                        {
                            //siparişi veren de alanda aynı firmadır
                            cSender.MainCompanyCode = _OldOrder.mainCompanyCode;
                            cSender.CompanyCode = _OldOrder.companyCode;
                            cSender.Name = _OldOrder.companyName;
                            cSender.CompanyType = EnumCompanyType.Customer;
                            cSender.TaxNumber = _OldOrder.companyTaxNumber;
                            cSender.TaxDepartmentName = _OldOrder.companyTaxDepartmentName;
                            cSender.CityName = _OldOrder.companyCityName;
                            cSender.TownName = _OldOrder.companyTownName;
                            cSender.TownCode = _OldOrder.companyTownCode;
                            cSender.GLN = _OldOrder.companyGLN;
                            cSender.AddressText = _OldOrder.companyAddressText;
                            cSender.OrderRoleType = CompanyOrderRoleType.Sender;



                            cReceiver.MainCompanyCode = _OldOrder.mainCompanyCode;
                            cReceiver.CompanyCode = _OldOrder.companyCode;
                            cReceiver.Name = _OldOrder.companyName;
                            cReceiver.CompanyType = EnumCompanyType.Customer;
                            cReceiver.TaxNumber = _OldOrder.companyTaxNumber;
                            cReceiver.TaxDepartmentName = _OldOrder.companyTaxDepartmentName;
                            cReceiver.GLN = _OldOrder.companyGLN;
                            cReceiver.CityName = _OldOrder.companyCityName;
                            cReceiver.TownName = _OldOrder.companyTownName;
                            cReceiver.TownCode = _OldOrder.companyTownCode;
                            cReceiver.AddressText = _OldOrder.companyAddressText;
                            cReceiver.OrderRoleType = CompanyOrderRoleType.Receiver;



                        }
                    }
                    // Mix Ürün Çıkışı 9 
                    else if (_OldOrder.orderTypeReason == EnumOrderTypeReason.GoodsOutMix)
                    {
                        cSender.MainCompanyCode = _OldOrder.mainCompanyCode;
                        cSender.CompanyCode = _OldOrder.companyBranchCode;
                        cSender.CompanyType = EnumCompanyType.Warehouse;
                        cSender.OrderRoleType = CompanyOrderRoleType.Sender;
                        cSender.SyncStatusCode = EnumSyncStatus.SyncSuccess;

                        cReceiver.MainCompanyCode = _OldOrder.mainCompanyCode;
                        cReceiver.CompanyCode = _OldOrder.companyBranchCode;
                        cReceiver.CompanyType = EnumCompanyType.Warehouse;
                        cReceiver.OrderRoleType = CompanyOrderRoleType.Receiver;
                        cReceiver.SyncStatusCode = EnumSyncStatus.SyncSuccess;

                    }
                }
            }

            //Sipariş
            _NewOrder.MainCompanyCode = _OldOrder.mainCompanyCode;
            _NewOrder.OrderCode = _OldOrder.orderCode;
            _NewOrder.OrderDate = _OldOrder.orderDate;
            _NewOrder.PlannedDeliveryDate = _OldOrder.plannedDate;
            _NewOrder.OrderType = _OldOrder.orderType;
            _NewOrder.LoadingType = _OldOrder.loadingType;
            _NewOrder.ShipmentCode = _OldOrder.shipmentCode;
            _NewOrder.SyncStatusCode = EnumSyncStatus.SyncWaiting;
            _NewOrder.WarehouseCode = _OldOrder.companyBranchCode;
            //_NewOrder.RelatedOrderCode = _OldOrder.orderReferenceCode;
            _NewOrder.WaybillCode = _OldOrder.documentCode;
            _NewOrder.OrderPriority = _OldOrder.orderPriority;
            _NewOrder.OrderSubType = _OldOrder.orderTypeReason.ToString();


            //Siparişe firmaları ekleme

            if (!string.IsNullOrEmpty(cSender.MainCompanyCode))
            {
                _NewOrder.CompanyList.Add(cSender);
            }
            if (!string.IsNullOrEmpty(cReceiver.MainCompanyCode))
            {
                _NewOrder.CompanyList.Add(cReceiver);
            }

            List<OrderLineNew> _OrderLineNewList = new List<OrderLineNew>();
            int counter = 0;
            foreach (var z in _OldOrder.Lines)
            {
                counter++;

                //Satır
                OrderLineNew _OrderLineNew = new OrderLineNew();
                _OrderLineNew.Order = _NewOrder;

                _OrderLineNew.LineCode = counter.ToString();
                _OrderLineNew.Quantity = z.quantity;
                _OrderLineNew.LotCode = z.lotCode;
                _OrderLineNew.ContainerType = z.containerType;
                _OrderLineNew.ExpireDate = z.expireDate;

                //Ürün Entegrasyonu yok !
                Item _Item = new Item();
                _Item.MainCompanyCode = _OldOrder.mainCompanyCode;
                _Item.ItemCode = z.itemCode;
                _Item.ItemDescription = z.itemDescription;
                //_Item.Ldm = z.ldm;
                //_Item.Gtin = z.gtin;
                //_Item.ExpireDateRequired = z.expireDateRequired == "E" ? true : false;
                //_Item.LotRequired = false;
                //_Item.ShelfLife = z.shelfLife;
                //_Item.AdrTypeLabel = z.adrTypeLabel;
                //_Item.AdrUnNumber = z.adrUnNumber;
                //_Item.AdrItemName = z.adrItemName;
                //_Item.AdrMainRiskClass = z.adrMainRiskClass;
                //_Item.AdrAdditionalRisk = z.adrAdditionalRisk;
                //_Item.AdrPackageGroup = z.adrPackageGroup;
                //_Item.AdrTunnelRestrictionCode = z.adrTunnelRestrictionCode;
                //_Item.IsNotified = false;
                //_Item.Volume = z.volume;
                //_Item.GrossWeight = z.grossWeight;
                //_Item.NetWeight = z.netWeight;
                //_Item.UnitType = z.unitType;
                _Item.SyncStatusCode = EnumSyncStatus.SyncSuccess;

                _Item.ItemEachCustomerList = null;


                _OrderLineNew.Item = _Item;
                _NewOrder.Lines.Add(_OrderLineNew);
            }

         }

        public OrderNew GetIntOrder(string pMainCompanyCode,string pOrderCode)
        {
            OrderNew _Order = new OrderNew();
            List<ItemCustom> _ItemList = new List<ItemCustom>();
            OrderLineNew _OrderLine = new OrderLineNew();
            Item _Item = new Item();

            using (ESubeContext db = new ESubeContext())
            {
                List<SqlParameter> paramlistOrder = new List<SqlParameter>();
                List<SqlParameter> paramlistOrderLine = new List<SqlParameter>();

                //Sipariş header bilgileri
                paramlistOrder.Add(new SqlParameter("pMainCompanyCode", string.IsNullOrEmpty(pOrderCode) == true ? "" : pMainCompanyCode.ToString()));
                paramlistOrder.Add(new SqlParameter("pOrderCode", string.IsNullOrEmpty(pOrderCode) == true ? "" : pOrderCode.ToString()));
                _Order = db.Database.SqlQuery<OrderNew>("Sp_Getir_Get_Int_Order @pMainCompanyCode,@pOrderCode", paramlistOrder.ToArray()).FirstOrDefault();

                //Sipariş satır bilgileri
                if (_Order != null)
                {
                    paramlistOrderLine.Add(new SqlParameter("pMainCompanyCode", string.IsNullOrEmpty(pOrderCode) == true ? "" : pMainCompanyCode.ToString()));
                    paramlistOrderLine.Add(new SqlParameter("pOrderCode", string.IsNullOrEmpty(pOrderCode) == true ? "" : _Order.OrderCode.ToString()));
                    _ItemList = db.Database.SqlQuery<ItemCustom>("Sp_Getir_Get_Int_Order_Detail @pMainCompanyCode,@pOrderCode", paramlistOrderLine.ToArray()).ToList();


                    for (int i = 0; i < _ItemList.Count; i++)
                    {

                            _OrderLine = new OrderLineNew();
                            _OrderLine.Quantity = _ItemList[i].Quantity;

                            _Item = new Item();
                            _Item.ItemCode = _ItemList[i].ItemCode;
                            _Item.ItemDescription = _ItemList[i].ItemDescription;

                            _OrderLine.Item = _Item;
                            _Order.Lines.Add(_OrderLine);
                    }
                }


            }

            return _Order;

        }

        public ReturnObject TransferOrder(string pCurrentOrderCode, string pNewOrderCode, string pNewOrderId, string pItemCodeListStr)
        {

            ReturnObject _RetObj = new ReturnObject();
            _RetObj.Result = true;
            _RetObj.ResultMessage = "";

            using (ESubeContext db = new ESubeContext())
            {
                //List<SqlParameter> paramlist = new List<SqlParameter>();
                //paramlist.Add(new SqlParameter("pMainCompanyCode", pOrder.MainCompanyCode));
                //paramlist.Add(new SqlParameter("pOrderCode", pOrder.OrderCode));

                var sqlResult = db.Database.SqlQuery<ReturnObject>("EXEC Sp_Getir_Transfer_Order @pCurrentOrderCode={0},@pItemCodeListStr={1},@pNewOrderId={2},@pNewOrderCode={3}", pCurrentOrderCode, pItemCodeListStr, pNewOrderId, pNewOrderCode).ToList();


                if (sqlResult != null)
                {
                    _RetObj.Result = Convert.ToBoolean(sqlResult[0].Result);
                    _RetObj.ResultMessage = sqlResult[0].ResultMessage;
                }
                else
                {
                    _RetObj.Result = false;
                    _RetObj.ResultMessage = "Error occurred";
                }

            }

            return _RetObj;

        }


    }
}




