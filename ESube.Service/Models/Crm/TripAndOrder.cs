﻿using ESube.Service.Context;
using ESube.Service.Models;
using ESube.Service.Models.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Linq;
using ESube.Service.Models.Views;
using ESube.Service.Models.Enums;
using System.Data.SqlClient;


namespace ESube.Service.Models.Crm
{
    public class TripAndOrder
    {
        public string OrderCode { get; set; }
        public string OrderStatus { get; set; }
        public DateTime OrderDatetime { get; set; }
        public string OrderType { get; set; }
        public string InvoiceType { get; set; }
        public string TransitWarehouse { get; set; }
        public string DocNo { get; set; }
        public string RefNo { get; set; }
        public string SenderCode { get; set; }
        public string SenderAddress { get; set; }
        public string SenderCity { get; set; }
        public string SenderTown { get; set; }
        public string CustCode { get; set; }
        public string CustAddress { get; set; }
        public string CustCity { get; set; }
        public string CustTown { get; set; }
        public string PartyTaxCode { get; set; }
        public string PartyTaxOffice { get; set; }
        public string PartyDescription { get; set; }
        public decimal SumWeight { get; set; }
        public decimal SumQuantity { get; set; }
        public string TripCode { get; set; }
        public DateTime TripDateTime { get; set; }
        public string TripInvoiceType { get; set; }
        public string ToAddress { get; set; }
        public string Platenumber { get; set; }
        public string VehicleType { get; set; }
        public string DriverName { get; set; }
        public string TransportSubCont { get; set; }
        public string  TripType { get; set; }


        public List<TripAndOrder> GetList(string pOrderCode="",  string pTripCode="")
        {
            List<TripAndOrder> _Return = new List<TripAndOrder>();

            using (ESubeContext db = new ESubeContext())
            {
                // SP den al
                List<SqlParameter> paramlist = new List<SqlParameter>();
                paramlist.Add(new SqlParameter("pOrderCode",string.IsNullOrEmpty(pOrderCode) ?"" : pOrderCode));
                paramlist.Add(new SqlParameter("pTripCode", string.IsNullOrEmpty(pTripCode) ? "" : pTripCode));
               _Return = db.Database.SqlQuery<TripAndOrder>("Sp_Crm_Get_Order_And_Trip_Info @pOrderCode,@pTripCode", paramlist.ToArray()).ToList();

            }

            return _Return;
        }

    }
}