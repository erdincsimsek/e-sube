﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ESube.Service.Context;
using System.Data.Entity;
using ESube.Service.Models.Helpers;
using ESube.Service.Helpers;

namespace ESube.Service.Models
{
    [Table("Report")]
    public class Report : BaseEntityClass
    {
        [Required]
        public string MainCompanyCode { get; set; }
        [Required]
        public string Name{ get; set; }
        public string Description{ get; set; }
        [Required]
        public string URL { get; set; }


        public List<Report> GetList(string pMainCompanyCode,string pName,bool? pIsActive)
        {
            List<Report> _List = new List<Report>();

            using (ESubeContext db = new ESubeContext())
            {
                _List = (from x in db.Report where  x.IsDeleted == false && 
                         (string.IsNullOrEmpty(pMainCompanyCode) ? 1==1 : pMainCompanyCode == x.MainCompanyCode) &&
                         (string.IsNullOrEmpty(pName) ? 1 == 1 : x.Name.Contains(pName)) &&
                         (pIsActive == null ? 1 == 1 : x.IsActive == pIsActive)

                         select x).ToList();
            }

            return _List;
        }

        public ReturnObject InsertOrUpdate(Report pReport)
        {

            ReturnObject ret = new ReturnObject();
            ret.Result = false;
            ret.ResultMessage = "";

            Report existReport = new Report();



            try
            {
                using (ESubeContext db = new ESubeContext())
                {

                    if (pReport.Id == null || pReport.Id == 0)
                    {
                        db.Report.Add(pReport);
                    }
                    else if(pReport.Id > 0)
                    {
                        existReport = (from x in db.Report where x.Id == pReport.Id select x).FirstOrDefault();
                        existReport.MainCompanyCode = pReport.MainCompanyCode;
                        existReport.Name = pReport.Name;
                        existReport.Description = pReport.Description;
                        existReport.URL = pReport.URL;
                        existReport.IsDeleted = pReport.IsDeleted;
                        existReport.IsActive = pReport.IsActive;
                    }
                    db.SaveChanges();

                }
                ret.Result = true;
            }
            catch (Exception e)
            {
                ret.ResultMessage = e.Message;
            }
          

            return ret;
        }

    }
}