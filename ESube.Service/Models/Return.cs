﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ESube.Service.Models
{
    public class ReturnClass
    {
        public bool Success { get; set; }

        public int MessageCode { get; set; }
        public string MessageTitle { get; set; }
        public string MessageDetail { get; set; }

    }
}
