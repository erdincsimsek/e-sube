﻿//using ESube.Service.Context;
//using ESube.Service.Models;
//using ESube.Service.Models.Helpers;
//using System;
//using System.Collections.Generic;
//using System.ComponentModel;
//using System.ComponentModel.DataAnnotations;
//using System.ComponentModel.DataAnnotations.Schema;
//using System.Runtime.Serialization;
//using System.ServiceModel;

//namespace ESube.Service.Models
//{
//    /// <summary>
//    /// sipariş detatyları
//    /// </summary>
//    public class ShippedOrderLine
//    {
//        public ShippedOrderLine() { }

//        [IgnoreDataMember]
//        public ShippedOrderResponse order { get; set; }

//        /// <summary>
//        /// İrsaliye Numarası
//        /// </summary>
//        [StringLength(20)]
//        public string dispatchNumber { get; set; }

//        /// <summary>
//        /// İrsaliye basım tarihi(YYYY-MM-DD)
//        /// </summary>
//        [StringLength(10)]
//        public string issueDate { get; set; }

//        /// <summary>
//        /// İrsaliye basım zamanı (HH:MM:SS)
//        /// </summary>
//        [StringLength(10)]
//        public string issueTime { get; set; }

//        /// <summary>
//        /// İrsaliye tipi
//        /// </summary>
//        [StringLength(50)]
//        public string dispatchType { get; set; }

//        /// <summary>
//        /// İrsaliye dosya adı
//        /// </summary>
//        [StringLength(200)]
//        public string dispatchImageName { get; set; }

//        /// <summary>
//        /// Palet barkodu
//        /// </summary>
//        [StringLength(50)]
//        public string palletBarcode { get; set; }

//        /// <summary>
//        /// Satır sırası
//        /// </summary>
//        [StringLength(10)]
//        public string lineCode { get; set; }

//        /// <summary>
//        /// Ürün Kodu
//        /// </summary>
//        [StringLength(50)]
//        public string itemCode { get; set; }

//        /// <summary>
//        /// Ürün adı
//        /// </summary>
//        [StringLength(200)]
//        public string itemName { get; set; }

//        /// <summary>
//        /// Lot numarası
//        /// </summary>
//        [StringLength(20)]
//        public string lotCode { get; set; }

//        /// <summary>
//        /// Lot numarası (YYYY-MM-DD)
//        /// </summary>
//        [StringLength(10)]
//        public string expiryDate { get; set; }

//        /// <summary>
//        /// Miktar
//        /// </summary>
//        [StringLength(200)]
//        public string quantity { get; set; }

//        /// <summary>
//        /// Çıkış birimi (Adet,Koli,Palet vb)
//        /// </summary>
//        [StringLength(200)]
//        public string packageType { get; set; }

//        /// <summary>
//        /// Çıkış birimi (Adet,Koli,Palet vb)
//        /// </summary>
//        [StringLength(10)]
//        public string grossWeight { get; set; }
//    }

//    [ServiceContract]
//    public interface IOrder
//    {
//        [OperationContract]
//        string sCreateOrder(Order order);
//        //[OperationContract]
//        //string deleteOrder(string mainCompanyCode, string companyBranchCode, string orderCode);
//        //[OperationContract]
//        //string createItem(Item item);
//        //[OperationContract]
//        //string sendStockSnapshot(StockObject stock);

//    }
//}