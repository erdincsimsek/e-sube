﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ESube.Service.Models.Enums
{
    //_tender.StatusId = (int)Enum.GetValues(typeof(EnumTenderStatu)).OfType<EnumTenderStatu>().Where(x => x.ToString().ToLower().StartsWith("requested")).FirstOrDefault();

    public enum EnumTenderStatus : int
    {
        Requested = 0,
        Answered = 100,
        Accepted = 200,

        Rejected = 1100,
        Error = 1100
    }

    public enum EnumTenderAnswerStatus : int
    {
        Requested = 0,
        Answered = 100,
        Accepted = 200,

        Rejected = 1100,
        Error = 1100
    }

    public enum EnumSyncStatus : int
    {
        None = 0,
        SyncWaiting = 100,
        SyncSuccess = 200,
        SyncCancelled = 900,

        Delete = 400
    }

    public enum EnumLogStatus : int
    {
        Success = 200,
        Error = 1000
    }

    public enum EnumLogSource : int
    {
        WebService = 1,
        DbTransfer = 2
    }

    public enum EnumStockSource : int
    {
        Customer = 1,
        Dincer = 2
    }



    /// <summary>
    /// Sipariş Tipi
    /// </summary>
    public enum EnumOrderType : int
    {
        /// <summary>
        /// Hiçbiri
        /// </summary>
        None = -10,

        /// <summary>
        /// Giriş Siparişi
        /// </summary>
        GoodsIN = 1,
        /// <summary>
        /// Çıkış Siparişi
        /// </summary>
        GoodsOUT = -1, // depo çıkış

        /// <summary>
        /// Taşıma
        /// </summary>
        Transport = 2 // taşıma - NAKLİYE
    }

    /// <summary>
    /// Sipariş alt tipi
    /// </summary>
    public enum EnumOrderSubType : int
    {
        /// <summary>
        /// Hiçbiri
        /// </summary>
        None = 0,

        /// <summary>
        /// Satın Alma Siparişi
        /// </summary>
        Purchase = 1,

        /// <summary>
        /// İade
        /// </summary>
        Return = 2,

        /// <summary>
        /// Genel
        /// </summary>
        General = 3,

        /// <summary>
        /// Numune
        /// </summary>
        Sampling = 4,

        /// <summary>
        /// İmha
        /// </summary>
        /// 
        Destruction = 5,

        /// <summary>
        /// Depolararası transfer
        /// </summary>
        TransferBetweenDepots = 6,

        /// <summary>
        /// Hasarlı
        /// </summary>
        Damaged = 7,

        /// <summary>
        /// Mix Ürün Girişi
        /// </summary>
        GoodsInMix = 8,

        /// <summary>
        /// Mix Ürün Çıkışı
        /// </summary>
        GoodsOutMix = 9,

        /// <summary>
        /// Satış Siparişi
        /// </summary>
        SalesOrder = 10,

        /// <summary>
        /// Promosyon
        /// </summary>
        Promotion = 11,//promosyon

        /// <summary>
        /// Depolar arası transfer girişi
        /// </summary>
        TransferBetweenDepotsin = 12,

        /// <summary>
        /// Depolar arası transfer çıkışı
        /// </summary>
        TransferBetweenDepotsOut = 13,

        /// <summary>
        /// Sarf malzeme çıkış siparişi
        /// </summary>
        GoodsOutConsumable = 14,

        /// <summary>
        /// Sarf malzeme giriş siparişi
        /// </summary>
        GoodsInConsumable = 15,


        /// <summary>
        /// Komple yük
        /// </summary>
        FullTruck = 16,

        /// <summary>
        /// Parsiyel
        /// </summary>
        Partial = 17

        

    }


    /// <summary>
    /// Sipariş alt tipi
    /// </summary>
    public enum EnumOrderTypeReason : int
    {
        /// <summary>
        /// Hiçbiri
        /// </summary>
        None = 0,

        /// <summary>
        /// Satın Alma Siparişi
        /// </summary>
        Purchase = 1,

        /// <summary>
        /// İade
        /// </summary>
        Return = 2,

        /// <summary>
        /// Genel
        /// </summary>
        General = 3,

        /// <summary>
        /// Numune
        /// </summary>
        Sampling = 4,

        /// <summary>
        /// İmha
        /// </summary>
        /// 
        Destruction = 5,

        /// <summary>
        /// Depolararası transfer
        /// </summary>
        TransferBetweenDepots = 6,

        /// <summary>
        /// Hasarlı
        /// </summary>
        Damaged = 7,

        /// <summary>
        /// Mix Ürün Girişi
        /// </summary>
        GoodsInMix = 8,

        /// <summary>
        /// Müx Ürün Çıkışı
        /// </summary>
        GoodsOutMix = 9,

        /// <summary>
        /// Satış Siparişi
        /// </summary>
        SalesOrder = 10,

        /// <summary>
        /// Promosyon
        /// </summary>
        Promotion = 11,//promosyon

        /// <summary>
        /// Depolar arası transfer girişi
        /// </summary>
        TransferBetweenDepotsin = 12,

        /// <summary>
        /// Depolar arası transfer çıkışı
        /// </summary>
        TransferBetweenDepotsOut = 13,

        /// <summary>
        /// Sarf malzeme çıkış siparişi
        /// </summary>
        GoodsOutConsumable = 14,

        /// <summary>
        /// Sarf malzeme giriş siparişi
        /// </summary>
        GoodsInConsumable = 15,


        /// <summary>
        /// Komple yük
        /// </summary>
        FullTruck = 16,

        /// <summary>
        /// Parsiyel
        /// </summary>
        Partial = 17

        

    }

    /// <summary>
    /// Ürün Taşıma Şekli
    /// </summary>
    public enum EnumContainerType : int
    {

        /// <summary>
        /// Hiçbiri
        /// </summary>
        None = 0,

        /// <summary>
        /// Palet
        /// </summary>
        Pallet = 1,

        /// <summary>
        /// Paket
        /// </summary>
        Package = 2,

        /// <summary>
        /// Adet
        /// </summary>
        Piece = 3,

        /// <summary>
        /// Kutu
        /// </summary>
        Box = 4,

        ///<summary>
        ///Tank
        ///</summary>
        Tank = 5,

       /// <summary>
       /// Dökme 
       /// </summary>
        Bulk = 6,
    }

    /// <summary>
    /// Sipariş yükleme tipi
    /// </summary>
    public enum EnumLoadingType : int
    {

        /// <summary>
        /// Hiçbiri
        /// </summary>
        None = 0,

        /// <summary>
        /// Paletli 
        /// </summary>
        Palette = 1,

        /// <summary>
        /// Dökme 
        /// </summary>
        Bulk = 2,

        /// <summary>
        /// Hacimli
        /// </summary>
        Volume = 3,

        /// <summary>
        /// Full truck ( GEÇERSİZ DİR )
        /// </summary>
        FullTruck = 4
    }

    /// <summary>
    /// Ürün(Yük) birimleri
    /// </summary>
    public enum EnumUnitType
    {
        /// <summary>
        /// Hiçbiri
        /// </summary>
        None = -1,

        /// <summary>
        /// Adet
        /// </summary>
        Piece = 0,
        /// <summary>
        /// Litre
        /// </summary>
        Liter = 1,
        /// <summary>
        /// Kilogram
        /// </summary>
        Kilogram = 2,
        /// <summary>
        /// Gram
        /// </summary>
        Gram = 3,
        /// <summary>
        /// Metrekare
        /// </summary>
        Meter = 4,
        /// <summary>
        /// Metrekare
        /// </summary>
        Meter2 = 5,
        /// <summary>
        /// Metreküp
        /// </summary>
        Meter3 = 6,
        /// <summary>
        /// Paket
        /// </summary>
        Package = 7,
        /// <summary>
        /// Kutu
        /// </summary>
        Box = 8,
        /// <summary>
        /// Palet
        /// </summary>
        Pallet = 9

    }

    /// <summary>
    /// Statüler
    /// </summary>
    public enum EnumStatus : int
    {
        /// <summary>
        /// Girildi
        /// </summary>
        initial = 100,


        /// <summary>
        /// İşlemde
        /// </summary>
        inprogress = 200,


        /// <summary>
        /// Başarılı
        /// </summary>
        success = 300,


        /// <summary>
        /// Hatalı
        /// </summary>
        error = 900
    }
    //public enum EnumAdrType : int
    //{
    //    Class1 = 1, // patlayıcı maddeler ve nesneler
    //    Class14 = 2, // patlayıcı maddeler ve nesneler
    //    Class15 = 3,// patlayıcı maddeler ve nesneler
    //    Class16 = 4,// patlayıcı maddeler ve nesneler
    //    Class2 = 5, // gazlar
    //    Class21 = 6, // yaznıcı gazlar
    //    Class22 = 7, // yanmayan ve zehirli olmayan gazlar
    //    Class23 = 8, // zehirli gazlar
    //    Class3 = 9, // yanıcı sıvı maddeler
    //    Class41 = 10, // alevlenir katılar, katı patlayıcılar
    //    Class42 = 11, // kendiliğinden yanmaya yatkın maddeler
    //    Class43 = 12, // su ile temas ettiğinde alevlenir gazlar açığa çıkartan maddeler
    //    Class51 = 13, // yükseltgen maddeler
    //    Class52 = 14, // organik peroksitler
    //    Class61 = 15, // zehirli maddeler
    //    Class62 = 16, // Radyoaktif maddeler
    //    Class7A = 17, // Radyoaktif maddeler
    //    Class7B = 18, // Radyoaktif maddeler
    //    Class7C = 19, // Radyoaktif maddeler 
    //    Class7D = 20, // Radyoaktif maddeler
    //    Class7E = 21, // parçalanabilen maddeler
    //    Class8 = 21, // aşındırıcı maddeler
    //    Class9 = 23 // Muhtelif tehlşikeli maddeler ve nesneler
    //}

    /// <summary>
    /// Entegrasyon cevap kodları
    /// </summary>
    public enum EnumResponseCode : int
    {
        /// <summary>
        /// Başarılı
        /// </summary>
        success = 0, // palet
        /// <summary>
        /// Hatalı
        /// </summary>
        error = 700,
        /// <summary>
        /// Ürün tanımlama hatası
        /// </summary>
        createItemError = 1,
        /// <summary>
        /// Firma tanımlama hatası
        /// </summary>
        createCompanyError = 2,
        /// <summary>
        /// Çıkış siparişi tanımlama hatası
        /// </summary>
        createGoodsOutError = 3,
        /// <summary>
        /// Giriş siparişi tanımlama hatası
        /// </summary>
        createGoodsInError = 4,
        /// <summary>
        /// Giriş siparişi silme hatası
        /// </summary>
        deleteGetirGoodsInError = 5,
        /// <summary>
        /// Çıkış sipairişi silme hatası
        /// </summary>
        deleteGetirGoodsOutError = 6
        //errorBeforeInsertedOrder =201, // paket
        //errorBeforeInsertedLine = 201,
        //errorOrderNotFound = 201,
        //errorOrderDeleted = 201,
        //errorDocumentNotFound=206,
    }

    public enum EnumDocumentType : int
    {
        None = 0,
        waybill = 1,
        report = 2
    }

    public enum EnumDocumentStatus : int
    {
        Waiting = 0,
        Accepted = 100,
        Rejected = 200
    }

    public enum EnumOrderStatus : int
    {
        None = 0,
        Created = 100,
        InProgress = 110,
        Closed = 120,
        InComplateClosed = 130,

        Groupped = 200,
        Picking = 210,
        Picked = 220,
        Packing = 230,
        Approved = 240,
        WayBillPrinted = 250,
        Loaded = 260
    }

    public enum EnumRecordType : int
    {
        New = 0,
        Cancel = 1,
        Blocked = 2
    }

    public enum EnumCompanyType : int
    {


        /// <summary>
        /// Müşteri
        /// </summary>
        Customer = 1,

        /// <summary>
        /// Tedarikçi
        /// </summary>
        Supplier = 2,

        /// <summary>
        /// Diğer
        /// </summary>
        Other = 3,

        /// <summary>
        /// Depo
        /// </summary>
        Warehouse = 4

    }



    /// <summary>
    /// Firmanın Siparişteki Rolü ( siparişi veren, siparişi teslim alan... )
    /// </summary>
    public enum CompanyOrderRoleType : int
    {

        /// <summary>
        /// Hiçbiri
        /// </summary>
        None = 0,

        /// <summary>
        /// Gönderen
        /// </summary>
        Sender = 1,

        /// <summary>
        /// Alıcı
        /// </summary>
        Receiver = 2
    }

    /// <summary>
    /// Yük Cins Değerleri
    /// </summary>
    public enum EnumGoodsTypeId : int
    {
        /// <summary>
        /// Hammadde / Maden ürünleri / İnşaat malzemesi
        /// </summary>
        RawMaterial = 205,

        ///<summary>
        ///Ev/Büro Eşyası
        /// </summary>
        Furniture = 204,

        ///<summary>
        ///Kargo
        /// </summary>
        Cargo = 207,

        ///<summary>
        ///İçecekler / Kuru Gıda / Hububat
        /// </summary>
        DrinkAndDryFood = 206,

        ///<summary>
        ///Beyaz eşya / Elektronik ürünler
        /// </summary>
        WhiteGoodsAndElectronics = 201,

        ///<summary>
        ///Ağaç ürünleri
        /// </summary>
        WoodProducts = 200,

        ///<summary>
        ///Canlı hayvan
        /// </summary>
        Animal = 203,

        ///<summary>
        ///Bozulabilir gıda
        /// </summary>
        PerishableFood = 202,

        ///<summary>
        ///Otomotiv / İş makineleri
        /// </summary>
        Automotive = 208,

        ///<summary>
        ///Tekstil
        /// </summary>
        Textiles = 209,

        ///<summary>
        ///Diğer
        /// </summary>
        Other = 210,

        ///<summary>
        ///Tehlikeli Madde 
        ///* Taşıma Türü Kodu 1 ise yukCinsId = 999 gönderilmelidir.
        ///</summary>
        DangerousSubstance=999
    }

    public enum EnumTransportTypeCode : int
    {
        ///<summary>
        ///Tehlikeli madde
        ///</summary>
        DangerousSubstance = 1,
        ///<summary>
        ///Diğer
        ///</summary>
        Other = 2
    }


}