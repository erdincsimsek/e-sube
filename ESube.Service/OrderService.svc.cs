﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using ESube.Service.Models;
using ESube.Service.Context;
using ESube.Service.Filters;
using System.Text;
using System.Web;
using ESube.Service.Controllers;
using ESube.Service.Models.Enums;
using Newtonsoft.Json;
using ESube.Service.Models.WebApi;
using System.Text.RegularExpressions;
using System.Net.Http;
using System.Globalization;
using ESube.Service.Models.Helpers;
using static ESube.Service.Models.DailyStock;

namespace ESube.Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "OrderService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select OrderService.svc or OrderService.svc.cs at the Solution Explorer and start debugging.
    public class OrderService : IOrder
    {

        Log _Log = new Log();
        ApiResponse _ApiResponse = new ApiResponse();
        OrderNew _OrderNew = new OrderNew();
        ReturnObject _RetObj = new ReturnObject();


        // private ESubeContext db = new ESubeContext();
        /// <summary>
        /// Müşteri siparişleri (Giriş-Çıkış) Dincer sistemine alınır
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        public string sCreateOrder(Order order)
        {
            string returnMessage = "";
            OrderNew _OrderNew =new  OrderNew();
            clientResponse clientResponse = new clientResponse();
            string resultText = "";

            try
            {
                 //INSERT LOG
                 _Log = _Log.InsertWebApiLog(null, order.mainCompanyCode, order, HttpContext.Current);

                //ESKİ SİPARİŞ YAPISINI, YENİ SİPARİŞ CLASS INA GİYDİR.
                _OrderNew.MapOldOrderToNewOrder(order, _OrderNew);

                //SİPARİŞİ KAYDETMEDEN YAPILMASI GEREKEN İŞLEMLER...
                _RetObj = _OrderNew.InsertOrderBefore(_OrderNew);

                //SORUN YOKSA
                if (_RetObj.Result){
                    _OrderNew.InsertOrder(_OrderNew);
                    resultText  = string.Format("{0}-Order Inserted-{1}", EnumResponseCode.success, _OrderNew.OrderCode);
                }
                else
                {
                    resultText = string.Format("{0}-order not inserted-{1}-{2}", EnumResponseCode.error, _RetObj.ResultMessage, _OrderNew.OrderCode);
                }


                //using (ESubeContext db = new ESubeContext())
                //{

                //    try
                //    {
                //        order.createDate = DateTime.Now;
                //        order.integrationDate = DateTime.Now;
                //        order.status = "100";

                //        //db.Order.Add(order);

                //        db.SaveChanges();
                //        clientResponse.code = EnumResponseCode.success;
                //        clientResponse.message = string.Format("Order Inserted - {0}", order.orderCode);
                //        return string.Format("{0}-{1}", clientResponse.code, clientResponse.message);
                //    }
                //    catch (Exception ex)
                //    {
                //        clientResponse.code = EnumResponseCode.error;
                //        clientResponse.message = string.Format("order not inserted - {0}-{1}", order.orderCode, ex.InnerException.Message);
                //        clientResponse.errorMessage = ex.InnerException != null && ex.InnerException.InnerException != null ? ex.InnerException.InnerException.Message : ex.InnerException.Message;
                //        return string.Format("{0}-{1}", clientResponse.code, clientResponse.errorMessage);
                //    }
                //}
                //}
                //return returnMessage;

        
            }
            catch (Exception e)
            {

                string errorMsg = e.InnerException != null && e.InnerException.InnerException != null ? e.InnerException.InnerException.Message : e.InnerException.Message;

                resultText = string.Format("{0}-order not inserted-{1}", EnumResponseCode.error, errorMsg);

                _ApiResponse.StatusCode = EnumApiHttpStatus.MethodFailure;
                _ApiResponse.Message = "Error Code : " + _Log.Id.ToString();

                _Log.StatusCode = EnumLogStatus.Error;
                _Log.StatusMessage = errorMsg;
            }

            //LOG Update   
            if (_Log != null)
            {
                _Log.ResponseBody = JsonConvert.SerializeObject(_ApiResponse).ToString();
                _Log.UpdateWebApiLog(_Log);
            }

 
            return resultText;

            //return "Token is not valid";
        }


        /// <summary>
        /// Sipariş silme
        /// </summary>
        /// <param name="mainCompanyCode"></param>
        /// /// <param name="companyBranchCode"></param>
        /// /// <param name="orderCode"></param>
        /// <returns></returns>
        public string deleteOrder(string mainCompanyCode, string companyBranchCode, string orderCode)
        {
            bool ISAuthorization = true;

            //if (ISAuthorization)
            //{
            //    using (ESubeContext db = new ESubeContext())
            //    {
            //        clientResponse clientResponse = new clientResponse();
            //        var checkOrder = new Order();
            //        try
            //        {
            //            checkOrder.mainCompanyCode = mainCompanyCode;
            //            checkOrder.companyBranchCode = companyBranchCode;
            //            checkOrder.orderCode = orderCode;
            //            checkOrder.status = "delete";

            //            db.Order.Add(checkOrder);
            //            db.SaveChanges();
            //            clientResponse.code = EnumResponseCode.success;
            //            clientResponse.message = string.Format("order deleted - {0}", checkOrder.orderCode);
            //            return string.Format("{0}-{1}", clientResponse.code, clientResponse.message);
            //        }
            //        catch (Exception e)
            //        {
            //            clientResponse.code = EnumResponseCode.error;
            //            clientResponse.message = string.Format("order not delete - {0}", checkOrder.orderCode);
            //            clientResponse.errorMessage = e.InnerException.Message;
            //            return string.Format("{0}-{1}", clientResponse.code, clientResponse.errorMessage);
            //        }
            //    }
            //}

            return "Token is not valid";
        }



        /// <summary>
        /// Ürün tanımı
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public string createItem(Item item)
        {
            bool ISAuthorization = true;
            //if (ISAuthorization)
            //{
            //    using (ESubeContext db = new ESubeContext())
            //    {
            //        clientItemResponse clientResponse = new clientItemResponse();
            //        clientResponse.message = "200 item Created succeed";
            //        clientResponse.itemCode = item.itemCode;
            //        item.createDate = DateTime.Now;
            //        item.status = "100";
            //        try
            //        {
            //            db.InventoryItem.Add(item);
            //            db.SaveChanges();
            //            clientResponse.code = EnumResponseCode.success;
            //            clientResponse.message = string.Format("item inserted : {0} ", item.itemCode);
            //            return string.Format("{0}-{1}", clientResponse.code, clientResponse.message);

            //        }
            //        catch (Exception e)
            //        {
            //            clientResponse.code = EnumResponseCode.error;
            //            clientResponse.message = string.Format("item inserted error  : {0}", item.itemCode);
            //            clientResponse.errorMessage = e.InnerException.Message;
            //            return string.Format("{0}-{1}", clientResponse.code, clientResponse.errorMessage);
            //        }
            //    }
            //}

            return "Token is not valid";
        }




        /// <summary>
        /// Müşteri kendi stok verilerini Dincer tarfaına gönderir.
        /// </summary>
        /// <param name="stock"></param>
        /// <returns></returns>
        public string sendStockSnapshot(StockObject stock)
        {


            //string jsonObject = Regex.Unescape(JsonConvert.SerializeObject(stock, Newtonsoft.Json.Formatting.Indented));
            //var x =  new StringContent(jsonObject, Encoding.UTF8, "application/json");

            _Log = _Log.InsertWebApiLog(null, "PLSN01", stock, HttpContext.Current);


            bool ISAuthorization = true;
            if (ISAuthorization)
            {
                using (ESubeContext db = new ESubeContext())
                {
                    clientResponse clientResponse = new clientResponse();
                    try
                    {
                        //var checkMainCompany = db.RegisterCompany.Where(x => x.Name == stock.MainCompanyCode).FirstOrDefault();

                        //if (checkMainCompany == null)
                        //{
                        //    clientResponse.code = EnumResponseCode.error;
                        //    clientResponse.message = "stock inserted error  : ";
                        //    clientResponse.errorMessage = "Firma bilgisi bulanamdı.";
                        //    return string.Format("{0}-{1}", clientResponse.code, clientResponse.errorMessage);
                        //}

                        if (string.IsNullOrEmpty(stock.companyBranchCode))
                        {
                            clientResponse.code = EnumResponseCode.error;
                            clientResponse.message = "stock inserted error  : ";
                            clientResponse.errorMessage = "Depo bilgisi boş geçilemez.";
                            return string.Format("{0}-{1}", clientResponse.code, clientResponse.errorMessage);
                        }

                        if (string.IsNullOrEmpty(stock.stockDate))
                        {
                            clientResponse.code = EnumResponseCode.error;
                            clientResponse.message = "stock inserted error  : ";
                            clientResponse.errorMessage = "Stok tarihi bilgisi boş geçilemez.";
                            return string.Format("{0}-{1}", clientResponse.code, clientResponse.errorMessage);
                        }

                        //Stok tarihini sql formatına çevir
                        stock.stockDateSQL= Convert.ToDateTime(DateTime.ParseExact(stock.stockDate, "yyyyMMdd", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd"));


                        //Aynı gün için daha önce stok bilgisi var ise var olanları iptale çek     
                        _OrderNew.UpdateSyncStatus("PLSN01", 0, "", "stocksnapshot", stock.stockDate, EnumSyncStatus.SyncCancelled);

                        foreach (StockObjectItem item in stock.ListItems)
                        {
                            DailyStock obj = new DailyStock();
                            obj.mainCompanyCode = "PLSN01"/*stock.MainCompanyCode*/;
                            obj.warehouseCode = stock.companyBranchCode;
                            //obj.registerCompany = 5;
                            obj.stockDate = stock.stockDate;
                            obj.stockDateSQL = Convert.ToDateTime(DateTime.ParseExact(stock.stockDate, "yyyyMMdd", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd"));
                            obj.stockDate = stock.stockDate;
                            obj.itemCode = item.ItemCode;
                            obj.lotCode = item.LotCode;
                            obj.quantity = item.Quantity;
                            obj.expireDate = item.ExpiryDate;
                            obj.CreateDate = DateTime.Now;
                            obj.palleteCapacity = 0;
                            obj.packageCapacity = 0;
                            obj.SyncStatusCode = EnumSyncStatus.SyncSuccess;
                            db.DailyStock.Add(obj);

                        }
                        db.SaveChanges();


                        clientResponse.code = EnumResponseCode.success;
                        clientResponse.message = string.Format("stock inserted ");
                        return string.Format("{0}-{1}", clientResponse.code, clientResponse.message);

                    }
                    catch (Exception e)
                    {

                        _ApiResponse.StatusCode = EnumApiHttpStatus.MethodFailure;
                        _ApiResponse.Message = "Error Code : " + _Log.Id.ToString();

                        _Log.StatusCode = EnumLogStatus.Error;
                        _Log.StatusMessage = _ApiResponse.Message;

                        //LOG Update   
                        if (_Log != null)
                        {
                            _Log.ResponseBody = JsonConvert.SerializeObject(_ApiResponse).ToString();
                            _Log.UpdateWebApiLog(_Log);
                        }


                        clientResponse.code = EnumResponseCode.error;
                        clientResponse.message = "stock inserted error  : ";
                        clientResponse.errorMessage = e.InnerException.Message;
                        return clientResponse.errorMessage;
                    }

                    //LOG Update   
                    if (_Log != null)
                    {
                        _Log.ResponseBody = JsonConvert.SerializeObject(_ApiResponse).ToString();
                        _Log.UpdateWebApiLog(_Log);
                    }
                }
            }

            return "Token is not valid";
        }
    }
}
