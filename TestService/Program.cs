using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestService.ESube;

namespace TestService
{
    class Program
    { 
        static void Main(string[] args)
        {
            var integrationService = new IntegrationService();
            var integration = new Integration();
            var integrationLine = new IntegrationLine();
            var integrationLines = new List<IntegrationLine>(); 

            integration.companyAddressText = "Polisan Bostanci";
            integration.CompanyBranchCode = "DIN001";
            integration.companyCityCode = "34";
            integration.companyCityName = "�STANBUL";
            integration.CompanyCode = "cmpy2001";
            integration.companyContactCode = "CNCT011";
            integration.companyContactFirstName = "SELDA PLS";

            integrationLine.adrType = ESube.EnumAdrType.Class1;
            integrationLine.barcode = "32145";
            integrationLine.containerType = ESube.EnumContainerType.Packege;
            integrationLine.expiraDate = "30/01/2020";
            integrationLine.freeText = "HELLO WORD";
            integrationLine.grossWeight = 321;

            integrationLines.Add(integrationLine);
            integration.Lines = integrationLines.ToArray<IntegrationLine>();
            integrationService.createIntegration(integration);
            Console.WriteLine("Hello World!");
            Console.ReadKey();
            
        }
    }
}
