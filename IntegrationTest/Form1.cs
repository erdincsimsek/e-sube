using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using IntegrationTest.ServiceReference1;
using System.Net; 
using System.ServiceModel.Channels;
namespace IntegrationTest
{ 
    public partial class Form1 : Form
    {
        private NetworkCredential Credentials { get; set; }
        private string token = "GcbskWDI2rmOXklcUuNlKUeg8gYYA0h9/7y8tjSY/q8VMeOaA3QL8dkrOL4S9MuE59YWQMtbyOiz5TjVLy4lLg==";
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var service = new OrderClient();
            var order = new Order();
            var line = new OrderLine();
            var lines = new List<OrderLine>();
            string serviceReturn = "";
            using (new System.ServiceModel.OperationContextScope((System.ServiceModel.IClientChannel)service.InnerChannel))
            {
                try
                {
                    System.ServiceModel.Web.WebOperationContext.Current.OutgoingRequest.Headers.Add("Token", token);
                    serviceReturn = service.createOrder(dumyOrder());
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            using (new System.ServiceModel.OperationContextScope((System.ServiceModel.IClientChannel)service.InnerChannel))
            {
                try
                {
                    System.ServiceModel.Web.WebOperationContext.Current.OutgoingRequest.Headers.Add("Token", token);
                    serviceReturn = service.createOrder(dumyOrder());
                }
                catch (Exception ex)
                { 
                    throw ex;
                } 
            } 
            txtName.Text = serviceReturn;
            txtName.Refresh();
        }

        private Order dumyOrder()
        {
            var order = new Order();
            var orderLines = new List<OrderLine>();
            order.mainCompanyCode = "PLSN01";
            order.companyBranchCode = "001";
            order.companyCityCode = "16";
            order.companyCityName = "Bursa";
            order.companyContactCode = "C001";
            order.companyContactFirstName = "Dincer";
            order.companyContactLastName = "Lojistik";
            order.companyContactMail = "info@dincerlojistik.com";
            order.companyContactMobilePhoneNumber = "0536540652";
            order.companyDistrictName = "Kazanl�";
            order.companyGLN = "47656037288";
            order.companyName = "PBT Ta��ma";
            order.companyPostCode = "deneme";
            order.companyTownName = "BIM";
            order.companyTaxNumber = "4569874521";
            order.companyTaxDepartmentName = "ANKARA";
            order.documentCode = "IR999999";
            order.loadingType = "FullTruck";
            order.orderCode = "W2545236";
            order.orderDate = "24.08.2018";
            order.orderPriority = "1";
            order.orderType = "GoodsIN";
            order.orderTypeReason = "Purchase";
            order.plannedDate = "26.08.2018";
            orderLines.Add(dumyOrderLine());
            order.Lines = orderLines.ToArray<OrderLine>();
            return order;
        }

        private OrderLine dumyOrderLine()
        {
            var orderLine = new OrderLine();

            orderLine.adrType = EnumAdrType.Class15;
            orderLine.barcode = "63563256666";
            orderLine.containerType = EnumContainerType.Pallet;
            orderLine.expiraDate = "30.12.2020";
            orderLine.grossWeight = 13.0;
            orderLine.gtin = "8691234000007";
            orderLine.itemCode = "H01-9999";
            orderLine.itemDescription = "Peta SIL. Beyaz BO999 0.5 LT";
            orderLine.ldm = 0.0;
            orderLine.lotCode = "20180545";
            orderLine.netWeight = 1330.0;
            orderLine.productionDate = "14.02.2018";
            orderLine.quantity = 26.0;
            orderLine.volume = 12.0;

            orderLine.freeText = "bu alan bo� gecilebilir.";
          
            return orderLine;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
